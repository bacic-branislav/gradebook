$(document).ready(function(){
    

    // Admin panel

    // Prikaz tabela klikom na korisnike
    $('.studentsTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaRoditelji, .tabelaUcitelji, .tabelaProfesori, .tabelaAdmini, .tabelaDirektori').css({'display':'none'});
        $('.detaljiRoditelja, .detaljiUcitelja, .detaljiProfesora, .detaljiAdmina, .detaljiDirektora').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaUcenici, .detaljiUcenika').slideDown();

    })
    // Roditelji
    $('.parentsTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaUcenici, .tabelaUcitelji, .tabelaProfesori, .tabelaAdmini, .tabelaDirektori').css({'display':'none'});
        $('.detaljiUcenika, .detaljiUcitelja, .detaljiProfesora, .detaljiAdmina, .detaljiDirektora').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaRoditelji, .detaljiRoditelja').slideDown();
    })
    // Ucitelji
    $('.teachersTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaUcenici, .tabelaRoditelji, .tabelaProfesori, .tabelaAdmini, .tabelaDirektori').css({'display':'none'});
        $('.detaljiUcenika, .detaljiRoditelja, .detaljiProfesora, .detaljiAdmina, .detaljiDirektora').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaUcitelji, .detaljiUcitelja').slideDown();
    })
    // Profesori
    $('.profsTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaUcenici, .tabelaRoditelji, .tabelaUcitelji, .tabelaAdmini, .tabelaDirektori').css({'display':'none'});
        $('.detaljiUcenika, .detaljiRoditelja, .detaljiUcitelja, .detaljiAdmina, .detaljiDirektora').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaProfesori, .detaljiProfesora').slideDown();
    })
    // Admini
    $('.adminTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaUcenici, .tabelaRoditelji, .tabelaUcitelji, .tabelaProfesori, .tabelaDirektori').css({'display':'none'});
        $('.detaljiUcenika, .detaljiRoditelja, .detaljiUcitelja, .detaljiProfesora, .detaljiDirektora').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaAdmini, .detaljiAdmina').slideDown();
    })
    // Direktori
    $('.directorTab').click(function(){
        // Prvo ostale zatvaram
        $('.tabelaUcenici, .tabelaRoditelji, .tabelaUcitelji, .tabelaProfesori, .tabelaAdmini').css({'display':'none'});
        $('.detaljiUcenika, .detaljiRoditelja, .detaljiUcitelja, .detaljiProfesora, .detaljiAdmina').css({'display':'none'});
        $('.editodg').html('');
        // Tek onda prikazujem
        $('.tabelaDirektori, .detaljiDirektora').slideDown();
    })


    

    // Otkazi detalje za ucenika
    $('.detaljiUcenikaOtkazi').click(function(){
        $('#ucenik_ime').val('');
        $('#ucenik_prezime').val('');
    })
    // Otkazi detalje za roditelja
    $('.detaljiRoditeljaOtkazi').click(function(){
        $('#roditelj_ime').val('');
        $('#roditelj_prezime').val('');
        $('#roditelj_jmbg').val('');
    })
    // Otkazi detalje za ucitelja
    $('.detaljiUciteljaOtkazi').click(function(){
        $('#ucitelj_ime').val('');
        $('#ucitelj_prezime').val('');
    })
    // Otkazi detalje za profesora
    $('.detaljiProfesoraOtkazi').click(function(){
        $('#profesor_ime').val('');
        $('#profesor_prezime').val('');
        $('#profesor_predmet').val('');
    })
    // Otkazi detalje za admina
    $('.detaljiAdminaOtkazi').click(function(){
        $('#admin_ime').val('');
        $('#admin_prezime').val('');
    })
    // Otkazi detalje za direktora
    $('.detaljiDirektoraOtkazi').click(function(){
        $('#direktor_ime').val('');
        $('#direktor_prezime').val('');
    })
    



    /*----------- OPRAVDANJA ---------------*/

    // Animacija
    $('.istorija').click(function(){
        $('.svaOpravdanja').slideToggle();
    })

})


/*----------- ADMIN UNOS NOVOG KORISNIKA ------------*/

// Odabir statusa korsika
function korisnikRola(izabraniStatus) {
    
    switch(izabraniStatus) {
        case '0':
            //Nije nista izabrano i sve je zatvoreno
            $('.unesiUcitelja, .unesiRoditelja, .unesiProfesora, .unesiAdmina, .unesiDirektora').slideUp();
            $('.odgovor, .odgovorUcitelj, .odgovorProfesor, .odgovorAdmin, .odgovorDirektor').html('');
            break;

        case 'roditelj':
            // zatvaranje ostalih prozora
            $('.unesiUcitelja, .unesiProfesora, .unesiAdmina, .unesiDirektora').css({'display':'none'});
            $('.odgovor').html('');
            // prikazivanje trazenog
            $('.unesiRoditelja').slideDown();
            $('#profesor').prop( "disabled", true );
            $('#ucitelj').prop( "disabled", true );          
            break;

        case 'ucitelj':
            // zatvaranje ostalih prozora
            $('.unesiRoditelja, .unesiProfesora, .unesiAdmina, .unesiDirektora').css({'display':'none'});
            $('.odgovorUcitelj').html('');
            // prikazivanje trazenog
            $('.unesiUcitelja').slideDown();
            break;

        case 'profesor':
            // zatvaranje ostalih prozora
            $('.unesiRoditelja, .unesiUcitelja, .unesiAdmina, .unesiDirektora').css({'display':'none'});
            $('.odgovorProfesor').html('');
            // prikazivanje trazenog
            $('.unesiProfesora').slideDown();
            break;
        
        case 'admin':
            // zatvaranje ostalih prozora
            $('.unesiRoditelja, .unesiUcitelja, .unesiProfesora, .unesiDirektora').css({'display':'none'});
            $('.odgovorAdmin').html('');
            // prikazivanje trazenog
            $('.unesiAdmina').slideDown();
            break;

        case 'direktor':
            // zatvaranje ostalih prozora
            $('.unesiRoditelja, .unesiUcitelja, .unesiProfesora, .unesiAdmina').css({'display':'none'});
            $('.odgovorDirektor').html('');
            // prikazivanje trazenog
            $('.unesiDirektora').slideDown();
            break;    
    }
}

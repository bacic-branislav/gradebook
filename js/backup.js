$(document).ready(function(){
    // Run
    //prikazi_sve();

    function prikazi_ucenike() {
        // prikazi sve ucenike
        $.get('admin/index?ucenici', function(response){
            var response = JSON.parse(response);
            console.log(response);
            var br = 1;
            var izlaz = '';
            $('.brojUcenika').html(response.length);
    
            for(let i=0; i<response.length; i++) {
                
                var ime     = "'"+ response[i].ucenik_ime +"'";    
                var prezime = "'"+ response[i].ucenik_prezime +"'";
    
                izlaz += '<tr>'+
                            '<td>'+ response[i].ucenik_id +'</td>'+
                            '<td>'+ response[i].ucenik_ime + ' ' + response[i].ucenik_prezime +'</td>'+
                            '<td>'+ response[i].roditelj_ime +' '+ response[i].roditelj_prezime +'</td>'+
                            '<td>'+ response[i].razred +'</td>'+
                            '<td>'+ response[i].odeljenje +'</td>'+
                            '<td class="adminOpcije">'+
                                '<i class="fas fa-trash-restore" onclick="obrisiUcenika('+ response[i].ucenik_id +','+ response[i].roditelj_id +')" title="Obriši" ></i>'+
                                '<i class="fas fa-user-edit" onclick="izmeniUcenika('+ response[i].ucenik_id +','+ ime +','+ prezime +')" title="Izmeni" ></i>'+
                            '</td>'+
                         '</tr>';
            }
            
            // response.forEach(element => {
            //     var ime     = "'"+ element.ucenik_ime +"'";    
            //     var prezime = "'"+ element.ucenik_prezime +"'";
            //     izlaz += '<tr>';
            //     izlaz +=    '<td>'+ br +'</td>';
            //     izlaz +=    '<td>'+ element.ucenik_ime +' '+ element.ucenik_prezime +'</td>';
            //     izlaz +=    '<td>'+ element.roditelj_ime +' '+ element.roditelj_prezime +'</td>';
            //     izlaz +=    '<td>'+ element.razred +'</td>';
            //     izlaz +=    '<td>'+ element.odeljenje +'</td>';
            //     izlaz +=    '<td class="adminOpcije"> <i class="fas fa-trash-restore" onclick="obrisiUcenika('+ element.ucenik_id +','+ element.roditelj_id +')" title="Obriši"></i> <i class="fas fa-user-edit" onclick="izmeniUcenika('+ element.ucenik_id +','+ ime +','+ prezime +')" title="Izmeni"></i> </td>';
            //     izlaz += '</tr>'; br++;
            // });
    
            $('.tabelaUceniciResponse').html(izlaz);
        })
    }


    // Prikazivanje svih ucenika 
    function prikazi_sve(){
        $.ajax({
            type  : 'ajax',
            /*url   : '<?php echo site_url('product/product_data')?>',*/
            url : 'profesor/prikazi',
            async : true,
            dataType : 'json',
            success : function(data){
                var html = '';
                for(let i=0; i<data.length; i++) {
                    html += '<tr>'+
                                '<td>'+ data[i].ucenik_id +'</td>' +
                                '<td>'+ data[i].ucenik_ime +' '+ data[i].ucenik_prezime +'</td>' +
                                '<td>'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                '</td>'+
                                '<td>'+
                                    '<input type="number" class="form-control" id="profZakljuceno" min="1" max="5">'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-outline-success">Snimi ocenu</button>'+
                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }
        });
    } // end of prikazi_sve()



    // Ukoliko je kliknuto na polje
    //$('#primeniPromenu').click(function() { 

    function primeniPromenu (){

        var razred = $('#izabranRazred').val();
        var odeljenje = $('#izabranoOdeljenje').val();
        console.log(razred + odeljenje);
        
        $.post('profesor/izabran_razred', {razred:razred, odeljenje:odeljenje}, function(data){
            var html = '';
                for(let i=0; i<data.length; i++) {
                    html += '<tr>'+
                                '<td>'+ data[i].ucenik_id +'</td>' +
                                '<td>'+ data[i].ucenik_ime +' '+ data[i].ucenik_prezime +'</td>' +
                                '<td>'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                    '<input type="number" class="form-control" min="1" max="5">'+
                                '</td>'+
                                '<td>'+
                                    '<input type="number" class="form-control" id="profZakljuceno" min="1" max="5">'+
                                '</td>'+
                                '<td>'+
                                    '<button type="button" class="btn btn-outline-success">Snimi ocenu</button>'+
                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
        }) // ond of $.post
    }) // end of primeni promenu




})// end of document.ready





/*   $('#primeniPromenu').click(function(){
        var razred = $('#izabranRazred').val();
        var odeljenje = $('#izabranoOdeljenje').val();
        console.log(razred + odeljenje);
        
        $.post('profesor/razred', {razred:razred, odeljenje:odeljenje}, function(response){
            $('#odgovor').html(response);
        })

       // $.post('profesor/razred', {razred:razred, odeljenje:odeljenje})
    })
 */   


/*
    $('#primeniPromenu').click(function(){
        var razred = $('#izabranRazred').val();
        var odeljenje = $('#izabranoOdeljenje').val();
        console.log(razred + odeljenje);
        
        $.post('profesor', {razred:razred, odeljenje:odeljenje}, function(response){
            $('#odgovor').html(response);
        })
    })
*/ 



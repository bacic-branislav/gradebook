// DEFAULT putanja
var base_url = 'http://localhost/gradebook/';


//  Brisanje predmeta 
function obrisiPredmet(id, razred) {
    if(!confirm("Da li ste sigurni?"))
        return false;
    var predmet = id;
    var razred  = razred;
    // ajax
    $.post('predmeti', { predmet_id:predmet, razred:razred }, function(){
        location.reload();;
    })
}

// Brisanje opravdanja
function obrisiOpravdanje(id) {
    if(!confirm("Da li ste sigurni?"))
        return false;
    let brisanje_id = id;
    //ajax
    $.post('opravdanja', {brisanje_id:brisanje_id}, function(){
        location.reload();
    })
}

// Prikaz vec procitanog opravdanja
function prikaziOpravdanje(id) {
    let prikazi_id = id;
    $.post('opravdanja', {prikazi_id:prikazi_id}, function(response){
        let odg = JSON.parse(response);
        let roditelj = odg.roditelj_ime + ' ' + odg.roditelj_prezime;
        let ucenik   = odg.ucenik_ime + ' ' + odg.ucenik_prezime;
        let sadrzaj  = odg.sadrzaj;
        let slika    = odg.slika;
        let vreme    = odg.vreme;

        $('#odRoditelja').val(roditelj);
        $('#zaUcenika').val(ucenik);
        $('#sadrzaj').val(sadrzaj);
        $('#slika').attr("src","http://localhost/gradebook/uploads/" + slika);
        $('#vreme').html(vreme);
    })
}

/*--------------------- RAZREDI I ODELJENJA --------------------*/

// Uklanjanje staresine iz odeljenja
function ukloniStaresinu(id, razred, odeljenje) {
    let staresina = id;
    let ukloni_razred = razred;
    let ukloni_odeljenje = odeljenje;
    $.post('razredi', {ukloniStaresinu:staresina, razred:ukloni_razred, odeljenje:ukloni_odeljenje}, function(response){
        location.reload();
    })
}

// Uklanjanje ucenika iz odeljenja
function ukloniUcenika(id) {
    let ucenik_id = id;
    $.post('razredi', {ukloniUcenika:ucenik_id}, function(response){
        location.reload();
    })
}

// Dodavanje staresine u odeljenje
function dodajStaresinu(razred, odeljenje) {
    // Animacije
    if(razred <= 4){
        $('.dodajUcitelja').slideDown();
    }else{
        $('.dodajProfesora').slideDown();
    } 

    // Dodavanje ucitelja
    $('.addUcitelj').click(function(){
        var id_ucitelja = $('#izaberiUcitelja').val();
        $.post('razredi', {dodajUcitelja:id_ucitelja, razred:razred, odeljenje:odeljenje}, function(response){
            location.reload();
        })
    })
    // Dodavanje profesora
    $('.addProfesor').click(function(){
        var id_profesora = $('#izaberiProfesora').val();
        $.post('razredi', {dodajProfesora:id_profesora, razred:razred, odeljenje:odeljenje}, function(response){
            location.reload();
        })
    })
}

// Dodavanje ucenika u odeljeje
function dodajUcenika(razred, odeljenje) {
    // Animacija
    $('.dodajUcenika').slideDown();

    $('.addUcenik').click(function(){    
        var id_ucenika = $('#izaberiUcenika').val();
        $.post('razredi', {dodajUcenika:id_ucenika, razred:razred, odeljenje:odeljenje}, function(response){
            location.reload();
        })
    })
}


/*---------------------- PRIKAZ SVIH KORISNIKA zana pocetnoj strani admina ---------------*/

// Prikazi sve ucenike
function prikazi_ucenike() {
    $.get(base_url +'admin/index?ucenici', function(response){
        var response = JSON.parse(response);
        $('.broj_ucenika').html(response.length);
        var broj  = 1;
        var izlaz = '';
        
        response.forEach(element => {
            var ime     = "'"+ element.ucenik_ime +"'";    
            var prezime = "'"+ element.ucenik_prezime +"'";
            izlaz += 
            '<tr>'+
                '<td>'+ broj +'</td>'+
                '<td>'+ element.ucenik_ime +' '+ element.ucenik_prezime +'</td>'+
                '<td>'+ element.roditelj_ime +' '+ element.roditelj_prezime +'</td>'+
                '<td>'+ ((element.razred    == null) ? "<span style='color:tomato'>nema</span>" : element.razred) +'</td>'+
                '<td>'+ ((element.odeljenje == null) ? "<span style='color:tomato'>nema</span>" : element.odeljenje) +'</td>'+
                '<td class="adminOpcije">'+
                    '<i class="fas fa-trash-restore" onclick="obrisi_ucenika('+ element.ucenik_id + ',' + element.roditelj_id +')"       title="Obriši"></i>'+
                    '<i class="fas fa-user-edit"     onclick="popuni_formu_ucenika('+ element.ucenik_id + ',' + ime + ',' + prezime +')" title="Izmeni"></i>'+
                '</td>';
            '</tr>'; broj++;
        });
        $('.tabelaUceniciResponse').html(izlaz);
    })
}
// Prikazi sve roditelje
function prikazi_roditelje() {
    $.get(base_url + 'admin/index?roditelji', function(response){
        var response = JSON.parse(response);
        $('.brojRoditelja').html(response.length);
        var broj  = 1;
        var izlaz = '';

        response.forEach(element => {
            var ime     = "'"+ element.first_name +"'";    
            var prezime = "'"+ element.last_name  +"'";
            izlaz +=
            '<tr>'+
                '<td>'+ broj +'</td>'+
                '<td>'+ element.first_name + ' ' + element.last_name + '</td>'+
                '<td>'+ element.jmbg +'</td>'+
                '<td class="adminOpcije">'+
                    '<i class="fas fa-trash-restore" onclick="obrisi_ucenika('+ element.ucenik_id + ',' + element.roditelj_id +')"  title="Obriši"></i>'+
                    '<i class="fas fa-user-edit"     onclick="popuni_formu_roditelja('+ element.id + ',' + ime + ',' + prezime + ',' + element.jmbg +')" title="Izmeni"></i>'+
                '</td>'+
            '<tr>'; broj++;
        });
        $('.tabelaRoditeljiResponse').html(izlaz);
    })
}
// Prikazi sve ucitelje
function prikazi_ucitelje() {
    $.get(base_url +'admin/index?ucitelji', function(response){
        var response = JSON.parse(response);
        $('.brojUcitelja').html(response.length);
        var broj  = 1;
        var izlaz = '';

        response.forEach(element => {
            var ime     = "'"+ element.first_name +"'";    
            var prezime = "'"+ element.last_name  +"'";
            izlaz +=
            '<tr>'+
                '<td>'+ broj +'</td>'+
                '<td>'+ element.first_name + ' ' + element.last_name + '</td>'+
                '<td>'+ element.razred_id +'</td>'+
                '<td>'+ element.odeljenje_id +'</td>'+
                '<td class="adminOpcije">'+
                    '<i class="fas fa-trash-restore" onclick="obrisi_ucitelja('+ element.id + ',' + element.user_id +')"  title="Obriši"></i>'+
                    '<i class="fas fa-user-edit"     onclick="popuni_formu_ucitelja('+ element.id + ',' + ime + ',' + prezime + ')" title="Izmeni"></i>'+
                '</td>'+
            '<tr>'; broj++;
        });
        $('.tabelaUciteljiResponse').html(izlaz);
    })
}
// Prikazi sve profesore
function prikazi_profesore() {
    $.get(base_url +'admin/index?profesori', function(response){
        var response = JSON.parse(response);
        $('.brojProfesora').html(response.length);
        var broj  = 1;
        var izlaz = '';

        response.forEach(element => {
            var ime     = "'"+ element.first_name +"'";    
            var prezime = "'"+ element.last_name  +"'";
            var predmet = "'"+ element.predmet    +"'";
            izlaz +=
            '<tr>'+
                '<td>'+ broj +'</td>'+
                '<td>'+ element.first_name + ' ' + element.last_name + '</td>'+
                '<td>'+ element.razred_id +'</td>'+
                '<td>'+ element.odeljenje_id +'</td>'+
                '<td>'+ element.predmet +'</td>'+
                '<td class="adminOpcije">'+
                    '<i class="fas fa-trash-restore" onclick="obrisi_profesora('+ element.user_id +')"  title="Obriši"></i>'+
                    '<i class="fas fa-user-edit"     onclick="popuni_formu_profesora('+ element.user_id + ',' + ime + ',' + prezime + ',' + element.predmet_id + ',' + predmet +')" title="Izmeni"></i>'+
                '</td>'+
            '<tr>'; broj++;
        });
        $('.tabelaProfesoriResponse').html(izlaz);
    })
}
// Prikazi sve administratore
function prikazi_administratore() {
     $.get(base_url +'admin/index?admini', function(response){
        var response = JSON.parse(response);
        $('.brojAdmina').html(response.length);
        var broj  = 1;
        var izlaz = '';

        response.forEach(element => {
            var ime     = "'"+ element.first_name +"'";    
            var prezime = "'"+ element.last_name  +"'";
            izlaz +=
            '<tr>'+
                '<td>'+ broj +'</td>'+
                '<td>'+ element.first_name + ' ' + element.last_name + '</td>'+
                '<td class="adminOpcije">'+
                    '<i class="fas fa-trash-restore" onclick="obrisi_admina('+ element.user_id +')"  title="Obriši"></i>'+
                    '<i class="fas fa-user-edit"     onclick="popuni_formu_admina('+ element.user_id + ',' + ime + ',' + prezime +')" title="Izmeni"></i>'+
                '</td>'+
            '<tr>'; broj++;
        });
        $('.tabelaAdministratoriResponse').html(izlaz);
    })
}
// Prikazi sve direktore
function prikazi_direktore() {
    $.get(base_url +'admin/index?direktori', function(response){
       var response = JSON.parse(response);
       $('.brojDirektora').html(response.length);
       var broj  = 1;
       var izlaz = '';

       response.forEach(element => {
           var ime     = "'"+ element.first_name +"'";    
           var prezime = "'"+ element.last_name  +"'";
           izlaz +=
           '<tr>'+
               '<td>'+ broj +'</td>'+
               '<td>'+ element.first_name + ' ' + element.last_name + '</td>'+
               '<td class="adminOpcije">'+
                   '<i class="fas fa-trash-restore" onclick="obrisi_direktora('+ element.user_id +')"  title="Obriši"></i>'+
                   '<i class="fas fa-user-edit"     onclick="popuni_formu_direktora('+ element.user_id + ',' + ime + ',' + prezime +')" title="Izmeni"></i>'+
               '</td>'+
           '<tr>'; broj++;
       });
       $('.tabelaDirektoriResponse').html(izlaz);
   })
}





/*----------------------------- DELETE KORISNIKA ---------------------------*/

// Delete ucenika i roditelja
function obrisi_ucenika(ucenik_id, roditelj_id){
    if(!confirm('Da li ste sigurni?')) {
        return false;
    }else {
        $.post('admin/index',{obrisiUcenika:ucenik_id, obrisiRoditelja:roditelj_id}, function(){
            prikazi_ucenike();
            prikazi_roditelje()
        })
    }
}
// Delete ucitelja
function obrisi_ucitelja(ucitelj_id, user_id){
    if(!confirm('Da li ste sigurni?')) {
       return false; 
    }else {
        $.post('admin/index',{obrisiUcitelja:ucitelj_id, user:user_id}, function(){
            prikazi_ucitelje();
        })
    }
}
// Delete profesora
function obrisi_profesora(user_id){
    if(!confirm('Da li ste sigurni?')) {
        return false;
    }else {
        $.post('admin/index', {obrisiProfesora:user_id}, function(){
            prikazi_profesore();
        })
    }
}
// Delete admina
function obrisi_admina(user_id){
    if(!confirm('Da li ste sigurni?')) {
        return false;
    }else {
        $.post('admin/index', {obrisiAdmina:user_id}, function(){
            prikazi_administratore();
        })
    }
}
// Delete direktora
function obrisi_direktora(user_id){
    if(!confirm('Da li ste sigurni?')) {
        return false;
    }else {
        $.post('admin/index', {obrisiDirektora:user_id}, function(response){
            prikazi_direktore();
        })
    }
}




/*------------------ EDIT POSTOJECIH KORISNIKA ---------------*/

/*---------------------- Popuni formular ----------------------*/
// Popuni formu za ucenika
function popuni_formu_ucenika(id, ime, prezime){
    $('#ucenik_id').val(id);
    $('#ucenik_ime').val(ime);
    $('#ucenik_prezime').val(prezime);
}

// Popuni formu za roditelja
function popuni_formu_roditelja(id, ime, prezime, jmbg){
    $('#roditelj_id').val(id);
    $('#roditelj_ime').val(ime);
    $('#roditelj_prezime').val(prezime);
    $('#roditelj_jmbg').val(jmbg);
}

// Popuni formu ucitelja
function popuni_formu_ucitelja(id, ime, prezime){
    $('#ucitelj_id').val(id);
    $('#ucitelj_ime').val(ime);
    $('#ucitelj_prezime').val(prezime);
}
// Popuni formu profesora
function popuni_formu_profesora(id, ime, prezime, predmet_id, predmet){
    $('#profesor_id').val(id);
    $('#profesor_ime').val(ime);
    $('#profesor_prezime').val(prezime);
    $('#profesor_predmet').val(predmet);
    $("#profesor_predmet option[value="+ predmet_id +"]").attr('selected', 'selected');
}
// Popuni formu administratora
function popuni_formu_admina(id, ime, prezime){
    $('#admin_id').val(id);
    $('#admin_ime').val(ime);
    $('#admin_prezime').val(prezime);
}
// Popuni formu direktora
function popuni_formu_direktora(id, ime, prezime){
    $('#direktor_id').val(id);
    $('#direktor_ime').val(ime);
    $('#direktor_prezime').val(prezime);
}

/*---------------------- Sacuvaj nove izmene -------------------*/
// Editovanje ucenika
function sacuvaj_ucenika(){
    // slanje novih podataka u bazu
    var id      = $('#ucenik_id').val();
    var ime     = $('#ucenik_ime').val();
    var prezime = $('#ucenik_prezime').val();

    $.post('admin/index', {ucenik_id:id, ucenik_ime:ime, ucenik_prezime:prezime}, function(response){
        $(".editodg").html(response);
        $('#ucenik_id, #ucenik_ime, #ucenik_prezime').val('');
        prikazi_ucenike();
    })
}
// Editovanje roditelja
function sacuvaj_roditelja(){
    // slanje novih podataka u bazu
    var id      = $('#roditelj_id').val();
    var ime     = $('#roditelj_ime').val();
    var prezime = $('#roditelj_prezime').val();
    var jmbg    = $('#roditelj_jmbg').val();

    $.post('admin/index', {roditelj_id:id, roditelj_ime:ime, roditelj_prezime:prezime, roditelj_jmbg:jmbg}, function(response){
        $(".editodg").html(response);
        $('#roditelj_id, #roditelj_ime, #roditelj_prezime, #roditelj_jmbg').val('');
        prikazi_roditelje();
    })
}
// Editovanje roditelja
function sacuvaj_ucitelja(){
    // popunjavanje forme
    var id      = $('#ucitelj_id').val();
    var ime     = $('#ucitelj_ime').val();
    var prezime = $('#ucitelj_prezime').val();

    $.post('admin/index', {ucitelj_id:id, ucitelj_ime:ime, ucitelj_prezime:prezime}, function(response){
        $('.editodg').html(response);
        $('#ucitelj_id, #ucitelj_ime, #ucitelj_prezime').val('');
        prikazi_ucitelje();
    })
}
// Editovanje profesora
function sacuvaj_profesora(){
    // slanje novih podataka u bazu
    var id      = $('#profesor_id').val();
    var ime     = $('#profesor_ime').val();
    var prezime = $('#profesor_prezime').val();
    var predmet = $('#profesor_predmet').val();
    
    $.post('admin/index', {profesor_id:id, profesor_ime:ime, profesor_prezime:prezime, predmet:predmet}, function(response){
        $('.editodg').html(response);
        $('#profesor_id, #profesor_ime, #profesor_prezime').val('');
        prikazi_profesore();
    })
    
}
// Editovanje admina
function sacuvaj_admina(){
    // slanje novih podataka u bazu
    var id      = $('#admin_id').val();
    var ime     = $('#admin_ime').val();
    var prezime = $('#admin_prezime').val();   
        
    $.post('admin/index', {admin_id:id, admin_ime:ime, admin_prezime:prezime}, function(response){
        $('.editodg').html(response);
        $('#admin_id, #admin_ime, #admin_prezime').val('');
        prikazi_administratore();
    })
    
}
// Editovanje direktora
function sacuvaj_direktora(){
    // popunjavanje forme
    var id      = $('#direktor_id').val();
    var ime     = $('#direktor_ime').val();
    var prezime = $('#direktor_prezime').val();

    $.post('admin/index', {direktor_id:id, direktor_ime:ime, direktor_prezime:prezime}, function(response){
        $('.editodg').html(response);
        $('#direktor_id, #direktor_ime, #direktor_prezime').val('');
        prikazi_direktore();
    })
}
    


/*--------------------------- Document ready --------------------*/

$(document).ready(function(){

    var current_url = window.location.href;
    var admin_url = 'http://localhost/gradebook/admin';

    // Run 
    if(current_url == admin_url){
        prikazi_ucenike();
        prikazi_roditelje();
        prikazi_ucitelje();
        prikazi_profesore();
        prikazi_administratore();
        prikazi_direktore();
    }
    
    
    //------------------- UNOS NOVIH KORISNIKA --------------------*/

    // Unos roditelja - ucenika
    $('#unesiRoditelja').click(function(){
        var role         = $('#role').val();
        var first_name   = $('#f_name').val();
        var last_name    = $('#l_name').val();
        var jmbg         = $('#jmbg').val();
        var child_f_name = $('#child_f_name').val();
        var child_l_name = $('#child_l_name').val();
        var razred       = $('#razred').val();
        var odeljenje    = $('#odeljenje').val();
        var ucitelj      = $('#ucitelj').val();
        var profesor     = $('#profesor').val();

        // Slanje ajax zateva
        $.post('register', {role:role, f_name:first_name, l_name:last_name, jmbg:jmbg, child_f_name:child_f_name, child_l_name:child_l_name, razred:razred, odeljenje:odeljenje, ucitelj:ucitelj, profesor:profesor}, function(response){
            $('.odgovor').html(response);
            $('#f_name, #l_name, #jmbg, #child_f_name, #child_l_name, #razred, #odeljenje, #ucitelj, #profesor').val('');
            
        })// end of ajax
    })

    // Unos ucitelja 
    $('#unesiUcitelja').click(function() {
        var rola      = $('#role').val();
        var ime       = $('#ucitelj_ime').val();
        var prezime   = $('#ucitelj_prezime').val();
        var jmbg      = $('#ucitelj_jmbg').val();

        $.post('register', {role:rola, ucitelj_ime:ime, ucitelj_prezime:prezime, ucitelj_jmbg:jmbg}, function(response){
            $('.odgovorUcitelj').html(response);
            $('#ucitelj_ime, #ucitelj_prezime, #ucitelj_jmbg').val('');
        })// end of ajax
    })

    // Unos profesora
    $('#unesiProfesora').click(function(){
        var rola      = $('#role').val();
        var ime       = $('#profesor_ime').val();
        var prezime   = $('#profesor_prezime').val();
        var jmbg      = $('#profesor_jmbg').val();
        var predmet   = $('#profesor_predmet').val();

        $.post('register', {role:rola, profesor_ime:ime, profesor_prezime:prezime, profesor_jmbg:jmbg, profesor_predmet:predmet}, function(response){
            $('.odgovorProfesor').html(response);
            $('#profesor_ime, #profesor_prezime, #profesor_jmbg, #profesor_predmet').val('');
        })// end of ajax
    })

    // Unos admina
    $('#unesiAdmina').click(function(){
        var rola    = $('#role').val();
        var ime     = $('#admin_ime').val();
        var prezime = $('#admin_prezime').val();
        var jmbg    = $('#admin_jmbg').val();

        $.post('register', {role:rola, admin_ime:ime, admin_prezime:prezime, admin_jmbg:jmbg}, function(response){
            $('.odgovorAdmin').html(response);
            $('#admin_ime, #admin_prezime, #admin_jmbg').val('');
        })// end of ajax
    })

    // Unos direktora
    $('#unesiDirektora').click(function(){
        var rola    = $('#role').val();
        var ime     = $('#direktor_ime').val();
        var prezime = $('#direktor_prezime').val();
        var jmbg    = $('#direktor_jmbg').val();

        $.post('register', {role:rola, direktor_ime:ime, direktor_prezime:prezime, direktor_jmbg:jmbg}, function(response){
            $('.odgovorDirektor').html(response);
            $('#direktor_ime, #direktor_prezime, #direktor_jmbg').val();
        })// end of ajax
    })




    /*------------------------- OPRAVDANJA -------------------*/

    // Kada roditelj salje opravdanja
    $('#opravdanje_posalji').click(function() {
        $.ajax({
            url:  'opravdanja',
            type: 'POST', 
            data:  new FormData (document.getElementById('opravdanje_form')),
            contentType: false,
            cache:       false,
            processData: false, 
            success: function(response){
                $(".opravdanjaOdg").html(response);
            }
        })
    })

    // Kada ucitelj prima nova opravdanja
    $('#opravdanjaPrikazi').click(function() {
        var id_opravdanja = $('#svaOpravdanja').val();
        //ajax
        $.post('opravdanja', {id:id_opravdanja}, function(response) {
            var response = JSON.parse(response);
            var roditelj = response.roditelj_ime + ' ' + response.roditelj_prezime;
            var ucenik   = response.ucenik_ime + ' ' + response.ucenik_prezime;
            var sadrzaj  = response.sadrzaj;
            var slika    = response.slika;
            var vreme    = response.vreme;

            $('#odRoditelja').val(roditelj);
            $('#zaUcenika').val(ucenik);
            $('#sadrzaj').val(sadrzaj);
            $('#slika').attr("src","http://localhost/gradebook/uploads/" + slika);
            $('#vreme').html(vreme);
        })  
    }) 

    // Postavljanje opravdanja kao PROCITANO
    $('#procitano').click(function() {
        var procitano_id = $('#svaOpravdanja').val();
        // ajax
        $.post('opravdanja', {procitano:procitano_id}, function(response){
            $('.opravdanjeOdg').html(response);
            location.reload();
        })
    })


    
    //---------------- RAZREDI --------------//
    
    
    

})// end of document.ready

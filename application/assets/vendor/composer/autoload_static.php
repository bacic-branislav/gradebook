<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit28d9ea43c0c484b215d1899b348b3e66
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
        'C' => 
        array (
            'Chatkit\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
        'Chatkit\\' => 
        array (
            0 => __DIR__ . '/..' . '/pusher/pusher-chatkit-server/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit28d9ea43c0c484b215d1899b348b3e66::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit28d9ea43c0c484b215d1899b348b3e66::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

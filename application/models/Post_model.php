<?php

/**
 * Process goes like this
 * from views/posts/index.php 
 * button "Procitaj vise" sends url gradebook/posts/(slug) slug iz taken from db and post from which we press a button
 * then a router routes to posts/view/(that very slug) and sends it to controller
 * controller calls view method, instructed by router
 * view method takes in slug parameter view($slug)
 * sets $data['whatever'] = $this->Post_model->get_posts($slug)
 * in get_posts($slug = false) we actually check to see if param is sent
 * if not, we call CI's get() method which we use to list all the posts
 * if yes, we call CI's get_where() method which lists single post from which we pressed a button
 */

class Post_model extends CI_Model {

    //to load our database we use contructor and CI integrated methods load and database
    public function __construct() {
        $this->load->database();
    }
    //in get_posts method we take in parameter $slug which is url version of title
    public function get_posts($slug = false, $limit = false, $offset = false) {
        //checking to see if the limit is set
        if($limit) {
            $this->db->limit($limit, $offset);
        }
        //we check if slug is false. if it is we query the database throug CI's active record model
        //SELECT * FROM `posts`
        if($slug === false) {
            $this->db->order_by('id', 'desc'); // reverse order of posts
            $query = $this->db->get("view_massimo_posts");
            return $query->result_array();
        }
        //if and when the slug is passed in, query the database like this
        //SELECT * from POSTS where slug = $slug
        $query = $this->db->get_where("view_massimo_posts", array("slug" => $slug));
        return $query->row_array();
    }
    public function set_post() {
        //getting the form value with $this->input->post('name of input') and wrapping it in url_title() to create slug
        $slug = url_title($this->input->post('title'));
        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'body' => $this->input->post('body'),
            'user_id' => $this->session->userdata['user_id']
        );
        return $this->db->insert('posts', $data);
    }
    public function delete_post($id) {
        $this->db->where('id', $id);
        $this->db->delete('posts');
        return true;
    }
    public function update_post() {
        $slug = url_title($this->input->post('title'));
        // check to see if the post was edited before and preventing adding fixed notification if it was
        $edited = "";
        if(preg_match("~\bEditovano obavestenje\b~", $this->input->post('body'))) {
            $edited = "";
        } else {
            $edited = " | Editovano obavestenje";
        }
        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'body' => $this->input->post('body') . $edited // adding fixed notification that the post is edited
        );
        $this->db->where('id', $this->input->post('id'));
        return $this->db->update('posts', $data);
    }
}
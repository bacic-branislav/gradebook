
<?php 
    class Direktor_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }

        public function getPredmeti($razred) {
            $this->db->select('id, predmet');
            $this->db->from('view_massimo_predmeti_po_razredima');
            $this->db->where('razred', $razred);
            $query = $this->db->get();
            return $query->result();
        }

        // Za statistiku na novou skole
        public function getAllPredmeti() {
            $this->db->distinct('predmet, id');
            $this->db->from('predmet');
            $query = $this->db->get();
            return $query->result();
		}
		// za statistiku odeljenja po predmetima 
        public function zakljucne_ocene($razred,$odeljenje) {
			$this->db->from('view_ocena-odeljenje-razred');
			$this->db->where('razred_id',$razred);
			 $this->db->where('odeljenje_id', $odeljenje);
			$ocene = $this->db->get();
            $ocene = $ocene->result();
            //var_dump($ocene);
			for ($i=0;$i<count($ocene);$i++){
			$ocene[$i]->ocena=(json_decode($ocene[$i]->ocena));
			}
			//var_dump($ocene[0]->predmet);
			if (count($ocene)==0){
				$ocene[0]->ocena->Zakljucna[0]=10;
				// $ocene[$i]->predmet= "nema ocena";
				$ocene[$i]->predmet= "Ruza Marinovic";
			}
			for ($i=0;$i<count($ocene);$i++){
				
				$zakljucne[$ocene[$i]->predmet]['ocena'][]=$ocene[$i]->ocena->Zakljucna[0];
				$zakljucne[$ocene[$i]->predmet]['predmet']=$ocene[$i]->predmet;
				
			}
			//echo($zakljucne["Informatika"]['ocena'][0]);
			// racunanje proseka 
			$ocene=array();
			$s=0;
			$i=0;
			$p=0;
			$j=0;
			foreach($zakljucne as $zakljucena){
				foreach($zakljucena['ocena']as $ocena){
                    //echo('---'.$ocena);
					if(is_numeric($ocena)){
						$s=$s+$ocena;
						$i++;
					}
					else {
						$s=1;
						$i=1;
						break;
					}
					
				}
				if($i==0){
					$i=1;
				}
				$p=$s/$i;
				$ocene[$j]['ocena']=$p;
				$ocene[$j]['predmet']=$zakljucena['predmet'];
				$j++;
				
				
				
					//var_dump($s);
			}
			// pisanje u json fajl
			$JSON = json_encode($ocene);
			$predmeti_ocene =  fopen("predmeti_ocene.json", "w");
			fwrite($predmeti_ocene, $JSON);
			// var_dump($JSON);
			
		}
		// prosjek po deljenima za svaki razred
		public function uspeh_odeljenja($razred) {
			$this->db->from('view_ocena-odeljenje-razred');
			$this->db->where('razred_id',$razred);
			$ocene = $this->db->get();
            $ocene = $ocene->result();
			//var_dump($ocene);
			for ($i=0;$i<count($ocene);$i++){
			$ocene[$i]->ocena=(json_decode($ocene[$i]->ocena));
			}
			if (count($ocene)==0){
				$ocene[0]->ocena->Zakljucna[0]=10;
				// $ocene[$i]->odeljenje_id= "nema ocena";
				$ocene[$i]->odeljenje_id= "Ruza Marina Davalovic";
			}
			//var_dump($ocene);
			
			// var_dump($ocene[0]->odeljenje_id);
			for ($i=0;$i<count($ocene);$i++){
				
				$zakljucne[$ocene[$i]->odeljenje_id]['ocena'][]=$ocene[$i]->ocena->Zakljucna[0];
				$zakljucne[$ocene[$i]->odeljenje_id]['odeljenje']=$ocene[$i]->odeljenje_id;
				
			}
			// var_dump($zakljucne);
			$ocene=array();
			$s=0;
			$i=0;
			$p=0;
			$j=0;
			foreach($zakljucne as $zakljucena){
				foreach($zakljucena['ocena']as $ocena){
					//echo('---'.$ocena);
					if(is_numeric($ocena)){
						$s=$s+$ocena;
						$i++;
					}
				}
				if($i==0){
					$i=1;
				}
				$p=$s/$i;
				$ocene[$j]['uspeh']=$p;
				$ocene[$j]['odeljenje']=$zakljucena['odeljenje'];
				$j++;
				
				
				
					//var_dump($ocene);
			}
			// formiranje json fajla
			$JSON = json_encode($ocene);
			$predmeti_ocene =  fopen("odeljenja_razredi.json", "w");
			fwrite($predmeti_ocene, $JSON);
			// var_dump($JSON);
		}

    

    }
<?php

    class Ucitelj_model extends CI_Model {

        
        private $user_id;  // Dobija vrednost iz sesije
        private $ucitelj_id;
        private $odelenje_id;
        private $ucenici_id = [];

        public function __construct() {
            $this->load->database();
            $this->IzmeniOcene();
        }

        // get online users (dodao sam i ovde ovu metodu, u slucaju da hocemo da chat-ujemo iskljucivo sa online userima)
        public function getOnlineUsers() {
            //$this->db->from('user');
            $query = $this->db->query('SELECT username, role FROM user WHERE loged_in ="1" ORDER BY role DESC');
            return $query->result();
        }

        public function chatRoditelji($sesija) {
            //$this->db->from('view_massimo_razredi-ucitelj');
            $this->ucitelj_id = $sesija['user_id'];
            $this->db->select('roditelj_ime, roditelj_prezime');
            $this->db->from('view_massimo_razredi-ucitelj');
            $this->db->where('user_id', $this->ucitelj_id);
            $query = $this->db->get();
            return $query->result();

            // $query = $this->db->query("SELECT roditelj_ime, roditelj_prezime FROM `view_massimo_razredi-ucitelj` WHERE user_id = $this->ucitelj_id");
            // return $query->result();
        }
                 
        public function teacher_data($sesija) {

            $this->user_id = $sesija['user_id'];
            $resultUcitelj = $this->db->query("select * from ucitelj where user_id = $this->user_id")->result();
            foreach($resultUcitelj as $ucitelj) {
                $this->ucitelj_id   = $ucitelj->id;
                $this->odelenje_id  = $ucitelj->odeljenje_id;
                $this->razred_id    = $ucitelj->razred_id;
            }
            return $resultUcitelj;
            
        }

        public function SpisakUcenika(){
            $SpisakUcenika = $this->db->query("select * from ucenik where odeljenje_id = '$this->odelenje_id' and razred_id = '$this->razred_id' order by last_name asc")->result();
            foreach($SpisakUcenika as $ucenik){
                array_push($this->ucenici_id, $ucenik->id);
            }
            
            return $SpisakUcenika;
        }

        public function getPredmeti(){
            if( $this->odelenje_id == null){ // Ako ucitelj nema dodeljen razred
                echo "Niste rasporedjeni kao učitelj.<br> Kontaktirajte Administratora radi dodele razreda.<br>";
                echo "<a href='admin/logout'>Odjavi se!</a>";
                exit();
            }
            $predmeti = $this->db->query("SELECT * FROM `predmeti_po_razredima` join predmet on predmeti_po_razredima.predmet_id = predmet.id where predmeti_po_razredima.razred_id = $this->odelenje_id ORDER BY predmet.id ASC")->result();
            return $predmeti;
        }

        public function getOcene(){
           $uslov = "";
           if($this->ucenici_id == null){
               echo "Nemate učenike u odeljenju.<br> Kontaktirajte Administratora radi dodele razreda.<br>";
               echo "<a href='admin/logout'>Odjavi se!</a>";
               exit();
           }
           foreach($this->ucenici_id as $uc_id){
               $uslov = $uslov.$uc_id.",";   //$uc_id je Ucenik_id iz $this->Ucenici_id arraya koji dobija podatke iz funkcije SpisakUcenika()       
           }
           $uslov = rtrim($uslov,",");//ovde je mogao i implode
            $result = $this->db->query("select * from ocene where ucenik_id in ($uslov) order by ucenik_id")->result();
            return $result;
        }

        public function IzmeniOcene(){
            if(isset($_POST['snimi_ocenu'])){
                $ucenik_id = $_POST['ucenik'];
                $rezultat_izmene = [];
                foreach($_POST as $value=>$data){
                    if($value == "ucenik" or $value == "snimi_ocenu"){
                      //  echo "ovaj preskacemo <br>";
                    } else {
                    $splitter   = explode("|",$value);
                    $predmet    = $splitter[0];
                    $polje      = $splitter[1];
                    $rezultati[$predmet][$polje] = $data;
                    }
                    
                }
             foreach($rezultati as $predmet=>$ocene){
                $Ocena_nekodirana = [
                    "Ocene"=>[$ocene[0],$ocene[1],$ocene[2],$ocene[3]],
                    "Zakljucna"=>[$ocene[4]]
                ];
                $Ocena_kodirana = json_encode($Ocena_nekodirana);
                $izmena_ocene   = $this->db->query("UPDATE ocene
                SET ocena = '$Ocena_kodirana' WHERE ucenik_id = $ucenik_id AND predmet_id = $predmet;");
               }
            }
        }

        public function otvorena_vrata(){
            // Dobijanje ucitelj_id
            $this->db->from('ucitelj');
            $this->db->where('user_id', $this->user_id);
            $id = $this->db->get();
            $id = $id->result();
            $id =  ($id[0]->id); 

			$this->db->from('view_otvorena_vrata');
			$this->db->where('ucitelj_id',$id);
			$query = $this->db->get();
            return $query->result();
            
        }
        public function showAcceptedAppointments()
        {
            $query = $this->db->query('select roditelj.first_name as ime, roditelj.last_name as prezime, zahtevi.id, zahtevi.dan as dan, zahtevi.time as vreme, zahtevi.approved as odobrenje
                                        from zahtevi 
                                        join roditelj 
                                        on zahtevi.roditelj_id = roditelj.id
                                        where zahtevi.ucitelj_id = "'.$this->ucitelj_id.'"
                                        and approved = "prihvaceno"
                                        order by id DESC');
            return $query->result();
        }
        
        public function showAppointments()
        {
    
            $this->db->from('zahtevi');
            $query = $this->db->query('select roditelj.first_name as ime, roditelj.last_name as prezime, zahtevi.id, zahtevi.dan as dan, zahtevi.time as vreme, zahtevi.approved as odobrenje
                                        from zahtevi 
                                        join roditelj 
                                        on zahtevi.roditelj_id = roditelj.id
                                        where zahtevi.ucitelj_id = "'.$this->ucitelj_id.'"');
            return $query->result();
    
        }
    
        public function acceptAppointment($id)
        {
            $query = $this->db->query("UPDATE zahtevi 
                                SET approved = 'prihvaceno'
                                WHERE ucitelj_id = '$this->ucitelj_id'
                                AND id = '$id'");
            return $query;
        }
    
        public function refuseAppointment($id)
        {
            $query = $this->db->query("DELETE FROM zahtevi 
                                WHERE ucitelj_id = '$this->ucitelj_id'
                                AND id = '$id'");
            return $query;
        }

        public function otvorena(){
            if(isset($_POST['dan_otvorena_vrata'])){
                $this->db->from('ucitelj');
                $this->db->where('user_id', $this->user_id);
                $id = $this->db->get();
                $id = $id->result();
                $id =  ($id[0]->id); 
                $dan=($_POST['dan_otvorena_vrata']);
                $this->db->set('otvorena_vrata', $dan);
                $this->db->where('id', $id);
                $this->db->update('ucitelj');
                header("location: ".current_url());
            }
        }

        public function prihvati(){
            if(isset($_POST['prihvati'])and isset($_POST['status_id'])) {
                for($i=0; $i<count($_POST['prihvati']); $i++){
                    $status=($_POST['prihvati'][$i]);
                    $id=($_POST['status_id'][$i]);
                    
                    $this->db->set('status', $status);
                    $this->db->where('id', $id);
                    $this->db->update('otvorena_vrata');
                    header("location: ".current_url());
                
                }
            } 
        }

        // OPRAVDANJA
        public function getOpravdanja() {
            // Dobavljanje svih opravdanja za jednog ucitelja
            $this->db->from('view_massimo_opravdanja_ucitelji');
            $this->db->where('ucitelj_id', $this->ucitelj_id);
            $this->db->where('procitano', '0');
            $query = $this->db->get();
            return $query->result();   
        }

        // Prikaz svih procitanih opravdanja
        public function svaOpravdanja() {
            $this->db->from('view_massimo_opravdanja_ucitelji');
            $this->db->where('ucitelj_id', $this->ucitelj_id);
            $this->db->where('procitano', '1');
            $query = $this->db->get();
            return $query->result();   
        }

        // Dobijanje sadrzaja samo za jedno opravdanje
        public function getSadrzajOpravdanja($id) {
            $this->db->from('view_massimo_opravdanja_ucitelji');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $query = $query->result();
            foreach ($query as $opravdanje) {
                return $opravdanje;
            }
        }
        
        // Postavljanje opravdanja u procitano
        public function procitanoOpravdanje($id) {
            $this->db->set('procitano', '1');
            $this->db->where('id', $id);
            $this->db->update('opravdanja');
        }

        // Brisanje opravdanja
        public function obrisiOpravdanje($id) {
            $this->db->where('id', $id);
            $this->db->delete('opravdanja');
        }
        

        /** Bonus Deo -> Upisivanje Casa **/
        public function UpisCasa(){
            if(isset($_POST['upis_casova']) or isset($_POST['upis_submit'])){
                $splitter = isset($_POST['upis_casova'])?$_POST['upis_casova']:"";
                $splitter = explode(",",$splitter);
                $dan  = $splitter[0];
                $cas  = $splitter[1];
                $Unos = "Upis".$cas;
                $vec_upisano = " | " ;
                if(isset($_POST['upis_submit'])){
                    foreach($_POST as $key=>$vrednosti){
                        if($key == 'upis_submit' or $key=='upis_casova'){}
                        else {
                        $podatci[$key] = $vrednosti;
                        }
                    }
                    $podatci = implode(" | ",$podatci);
                    $Unos = "Upis".$cas;
                    $this->db->set($Unos, $podatci);
                    $this->db->where('razred', $this->razred_id);
                    $this->db->where('odeljenje', $this->odelenje_id);
                    $this->db->where('day', $dan);
                    $this->db->update('upis_izostanci');
                }
                $result = $this->db->query("select $Unos from upis_izostanci where day = '$dan' and razred = '$this->razred_id' and odeljenje = '$this->odelenje_id';")->result();
                foreach($result as $rez){
                  $uneseno = explode(" | ", $rez->$Unos);
                   $vec_upisano = $uneseno[0];
                   if(empty($vec_upisano)){
                       $vec_upisano = " ";
                   }
                }
            return $vec_upisano;
            }
        }
        /** Bonus deo => Upis izostanaka **/
        public function Absentees(){
            if(isset($_POST['ukloniUpisanog'])){
                $dummyday = "0,0";
                $dan_cas = isset($_POST['izostanci2'])?$_POST['izostanci2']:$dummyday;
                $dan_cas = explode(",",$dan_cas);
                $dan    = $dan_cas[0];
                $cas    = $dan_cas[1];
                $izostanak = "Izo".$cas;
                // $this->db->select('*');
                // $this->db->from('upis_izostanci');
                // $this->db->where('razred', $this->razred_id);
                // $this->db->where('odeljenje', $this->odelenje_id);
                // $this->db->where('day', $dan);
                // $query = $this->db->get();
                // $svi_upisani = $query->result();
                $svi_upisani = $this->db->query("select * from upis_izostanci where razred = $this->razred_id and odeljenje = $this->odelenje_id and day = $dan")->result();

                foreach($svi_upisani as $upisan){
                    $dekodiram = json_decode($upisan->$izostanak);
                }
                if(empty($_POST['remove_absentees'])){}
                    else {
                foreach($_POST['remove_absentees'] as $remove){
                    print_r($remove);
                    if(in_array($remove,$dekodiram)){
                       $brisac = array_search($remove,$dekodiram);
                       unset($dekodiram[$brisac]);
                    }
                }
            }   print_r($dekodiram);
                $enkodirano = json_encode($dekodiram);
                echo "<br>";
                print_r($enkodirano);
                $this->db->query("update upis_izostanci set $izostanak = '$enkodirano' where razred = $this->razred_id and odeljenje = $this->odelenje_id and day = $dan");
                // $this->db->set($izostanak, $enkodirano);
                // $this->db->where('razred', $this->razred_id);
                // $this->db->where('odeljenje', $this->odelenje_id);
                // $this->db->where('day', $dan);
                // $this->db->update('upis_izostanci');
            }
            else if(isset($_POST['izostanci']) or isset($_POST['odabraliSmo'])){
                $dummyday = "0,0";
                $dan_cas = isset($_POST['izostanci'])?$_POST['izostanci']:$dummyday;
                $dan_cas = explode(",",$dan_cas);
                $dan    = $dan_cas[0];
                $cas    = $dan_cas[1];
                $izostanak = "Izo".$cas;
                if(isset($_POST['odabraliSmo'])){
                    $data = isset($_POST['absentees'])?$_POST['absentees']:0;
                    $unos = json_encode($data);
                    if($_POST['izostanci'] == "0,0"){
                        //echo "greska";
                    } else {
                    $this->db->set($izostanak, $unos);
                    $this->db->where('razred', $this->razred_id);
                    $this->db->where('odeljenje', $this->odelenje_id);
                    $this->db->where('day', $dan);
                    $this->db->update('upis_izostanci');
                    }
                }
                $izostanci = isset($_POST['izostanci'])?($_POST['izostanci']):"0,0";
                $izostanci = explode(",",$izostanci);
                $dan    = $izostanci[0];
                $cas    = $izostanci[1];
                $izostanak = "Izo".$cas;
                $this->db->select('*');
                $this->db->from('upis_izostanci');
                $this->db->where('razred', $this->razred_id);
                $this->db->where('odeljenje', $this->odelenje_id);
                $this->db->where('day', $dan);
                $query = $this->db->get();
                $vec_upisani = $query->result();
                foreach($vec_upisani as $upisano){
                    if($izostanak == "Izo0"){}
                    else {
                    $vec_upisani = json_decode($upisano->$izostanak);
                    }
                }
                return $vec_upisani;
            }
        }
    }
        //Deo rasporeda za profesora ce ici tako sto cemo unositi prvo odelenju nakon cega ce se za isto updejtovati profesoru raspored (koji mora da ima zaseban entry) pa ce se tako i vrsiti provera. Isto tako moze da se radi i obrnuto
        //Mozda da postoji forma levo za raspored, desno za profesore pa kad se klikne na odredjeno polje on prodje kroz loop, izbaci sve profesore sa strane koji u tom casu imaju prazno polje

    /* Banetov deo koda, ostavljam za sad posto mi se svidja ARP da implementiram kasnije 
    
    private $ucitelj_id = 6;
        private $razred =  1;
        private $odeljenje = 'A';
        
        public function __construct() {
            $this->load->database();
        }

        public function podaci_ulogovanog_ucitelja() {
            $this->db->from('view_massimo_odeljenje-ucitelj');
            $this->db->where('id', $this->ucitelj_id); 
            $query = $this->db->get();
            return $query->result();
        }
        
        public function spisak_ucenika() {
            $this->db->from('view_massimo_odeljenje-ucenici');
            $this->db->where('razred', $this->razred);    
            $this->db->where('odeljenje', $this->odeljenje);    
            $this->db->order_by('ucenik_prezime', 'ASC');
            $query = $this->db->get();
            return $query->result();
        }

       public function predmeti_po_razredu() {
           $this->db->from('view_massimo_predmeti_po_razredima');
           $this->db->where('razred', $this->razred); 
           $query = $this->db->get();
           return $query->result();
       }

        public function razredni_staresina() {
            $this->db->from('view_massimo_odeljenje-profesor');
            $this->db->where('razred', $this->razred);    
            $this->db->where('odeljenje', $this->odeljenje); 
            $query = $this->db->get();
            return $query->result(); 
        }
        */
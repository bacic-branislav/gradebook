<?php

    class Profesor_model extends CI_Model {

        private $user_id;
        private $profesor_id; 
        private $predmet_id;
        private $odelenje_id;
        private $razred_id;
        private $ucenici_id = [];

        public function __construct() {
            $this->load->database();
            $this->IzmeniOcene();
        }

        public function chatRoditelji($sesija) {
            //$this->db->from('view_massimo_razredi-ucitelj');
            $this->profesor_id = $sesija['user_id'];
            $this->db->select('roditelj_ime, roditelj_prezime');
            $this->db->from('view_massimo_razredi-profesor');
            $this->db->where('user_id', $this->profesor_id);
            $query = $this->db->get();
            return $query->result();

            // stari - klasican query
            // $query = $this->db->query("SELECT roditelj_ime, roditelj_prezime FROM `view_massimo_razredi-profesor` WHERE user_id = $this->profesor_id");
            // return $query->result();
        }

        // Podaci o ulogovanom profesoru
        public function teacher_data($sesija) {
            
            $this->user_id = $sesija['user_id'];
            
            $this->db->from('profesor');
            $this->db->where('user_id', $this->user_id);
            $profResults = $this->db->get();
            $profResults = $profResults->result();
            foreach($profResults as $profesor){
                $this->profesor_id = $profesor->id;
                $this->odelenje_id = $profesor->odeljenje_id;
                $this->razred_id = $profesor->razred_id;
                $this->predmet_id = $profesor->predmet_id;
            }
            return $profResults;
        }

        // Prikazuje sve profesore - ocekuje parametar razred i odeljenje!
        public function razredni($razred, $odeljenje) {
            $this->db->from('view_massimo_odeljenje-profesor');
            $this->db->where('razred', $razred);    
            $this->db->where('odeljenje', $odeljenje); 
            $query = $this->db->get();
            return $query->result();
        }

        public function SpisakUcenika($razred, $odeljenje) {
            // $this->db->from('ucenik');
            $this->db->where('odeljenje_id', $odeljenje);
            $this->db->where('razred_id', $razred);
            $this->db->order_by('last_name', 'asc');
            $query = $this->db->get('ucenik');
            $query = $query->result();
            foreach($query as $ucenik){
                array_push($this->ucenici_id, $ucenik->id);
            }
            return $query;
        }

        public function getPredmet(){
            $this->db->from('predmet');
            $this->db->where('id', $this->predmet_id);
            $query = $this->db->get();
            return $query->result();
        }

        public function getOcene(){
            $uslov = "";
            if($this->ucenici_id == null){ 
                echo "Nema učenika u ovom odeljenju.<br> Kontaktirajte Administratora radi dodele.<br>";
                echo "<a href='profesor'>Nazad</a>";
                exit();
            }
            foreach($this->ucenici_id as $uc_id){
                $uslov = $uslov.$uc_id.",";   //$uc_id je Ucenik_id iz $this->Ucenici_id arraya koji dobija podatkje iz funkcije SpisakUcenika()       
            }
            $uslov = rtrim($uslov,",");//ovde je mogao i implode
            $result = $this->db->query("select * from ocene where ucenik_id in ($uslov) and predmet_id = '$this->predmet_id' order by ucenik_id")->result();
            return $result;
        }

        public function IzmeniOcene(){
            if(isset($_POST['snimi_ocenu'])){
               //print_r($_POST);
                $ucenik_id = $_POST['ucenik'];
                $rezultat_izmene = [];
                foreach($_POST as $value=>$data){
                    if($value == "ucenik" or $value == "snimi_ocenu" or $value == "izabranoOdeljenje" or $value == "izabranRazred"){
                      //  echo "ovaj preskacemo <br>";
                    } else {
                    $splitter   = explode("|",$value);
                    $predmet    = $splitter[0];
                    $polje      = $splitter[1];
                    $rezultati[$predmet][$polje] = $data;
                    }
                }
             foreach($rezultati as $predmet=>$ocene){
                $Ocena_nekodirana = [
                    "Ocene"=>[$ocene[0],$ocene[1],$ocene[2],$ocene[3]],
                    "Zakljucna"=>[$ocene[4]]
                ];
                $Ocena_kodirana = json_encode($Ocena_nekodirana);
                $izmena_ocene   = $this->db->query("UPDATE ocene
                SET ocena = '$Ocena_kodirana' WHERE ucenik_id = $ucenik_id AND predmet_id = $predmet;");
               }
            }
        }

        /*------------- OTVORENA VRATA -------------*/
        public function showAcceptedAppointments()
        {
            $query = $this->db->query('select roditelj.first_name as ime, roditelj.last_name as prezime, zahtevi.id, zahtevi.dan as dan, zahtevi.time as vreme, zahtevi.approved as odobrenje
                                        from zahtevi 
                                        join roditelj 
                                        on zahtevi.roditelj_id = roditelj.id
                                        where zahtevi.profesor_id = "'.$this->profesor_id.'"
                                        and approved = "prihvaceno"
                                        order by id DESC');
            return $query->result();
        }

        public function showAppointments()
        {

            $this->db->from('zahtevi');
            $query = $this->db->query('select roditelj.first_name as ime, roditelj.last_name as prezime, zahtevi.id, zahtevi.dan as dan, zahtevi.time as vreme, zahtevi.approved as odobrenje
                                        from zahtevi 
                                        join roditelj 
                                        on zahtevi.roditelj_id = roditelj.id
                                        where zahtevi.profesor_id = "'.$this->profesor_id.'"');
            return $query->result();

        }
         public function refuseAppointment($id)
        {
            $query = $this->db->query("DELETE FROM zahtevi 
                                WHERE profesor_id = '$this->profesor_id'
                                AND id = '$id'");
            return $query;
        }
        public function acceptAppointment($id)
        {
            $query = $this->db->query("UPDATE zahtevi 
                                SET approved = 'prihvaceno'
                                WHERE profesor_id = '$this->profesor_id'
                                AND id = '$id'");
            return $query;
        }

        public function otvorena_vrata(){
			// Dobijanje profesor_id
            
            $id=$this->profesor_id;
            
            
			$this->db->from('view_otvorena_vrata');
			$this->db->where('profesor_id',$id);
			$query = $this->db->get();
			return $query->result();
            }
           public function otvorena(){
                if(isset($_POST['dan_otvorena_vrata'])){
                    $dan=($_POST['dan_otvorena_vrata']);
                    
                    $id=$this->profesor_id; 
                    //var_dump($_POST['dan_otvorena_vrata']);
                    $this->db->set('otvorena_vrata', $dan);
                    $this->db->where('id', $id);
                    $this->db->update('profesor');
                } 
            }
            public function prihvati(){
                if(isset($_POST['prihvati'])and isset($_POST['status_id'])) {
                    for($i=0; $i<count($_POST['prihvati']); $i++){
                        $status=($_POST['prihvati'][$i]);
                        $id=($_POST['status_id'][$i]);
                        
                        $this->db->set('status', $status);
                        $this->db->where('id', $id);
                        $this->db->update('otvorena_vrata');
                        header("location: ".current_url());
                    
                    } 
                }
            }/*------------- end of OTVORENA VRATA -------------*/
            


        /*--------------  OPRAVDANJA ---------------*/

        public function getOpravdanja() {
            // Dobavljanje svih opravdanja za jednog ucitelja
            $this->db->from('view_massimo_opravdanja_profesor');
            $this->db->where('profesor_id', $this->profesor_id);
            $this->db->where('procitano', '0');
            $query = $this->db->get();
            return $query->result();   
        }

        // Prikaz svih procitanih opravdanja
        public function svaOpravdanja() {
            $this->db->from('view_massimo_opravdanja_profesor');
            $this->db->where('profesor_id', $this->profesor_id);
            $this->db->where('procitano', '1');
            $query = $this->db->get();
            return $query->result();   
        }

        // Dobijanje sadrzaja samo za jedno opravdanje
        public function getSadrzajOpravdanja($id) {
            $this->db->from('view_massimo_opravdanja_profesor');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $query = $query->result();
            foreach ($query as $opravdanje) {
                return $opravdanje;
            }
        }
        
        // Postavljanje opravdanja u procitano
        public function procitanoOpravdanje($id) {
            $this->db->set('procitano', '1');
            $this->db->where('id', $id);
            $this->db->update('opravdanja');
        }

        // Brisanje opravdanja
        public function obrisiOpravdanje($id) {
            $this->db->where('id', $id);
            $this->db->delete('opravdanja');
            
        }/*-------------- end of OPRAVDANJA ---------------*/

        //Ovo je za bonus deo
        /** Mini funkcija koju koristim dole kako bi splitovao Dan i Cas koji su potrebni za sve **/
        public function AbsenteeSplitter($post_name){
            $dummyday = "0,0";
            $dan_cas = isset($_POST["$post_name"])?$_POST["$post_name"]:$dummyday;
            $dan_cas = explode(",",$dan_cas);
            return $dan_cas;
        }

        /** Bonus Deo -> Upisivanje Casa **/
        public function UpisCasa(){
            if(isset($_POST['upis_casova']) or isset($_POST['upis_submit'])){
                $dan_cas = $this->AbsenteeSplitter("upis_casova");
                $dan  = $dan_cas[0];
                $cas  = $dan_cas[1];
                $Unos = "Upis".$cas;
                $vec_upisano = " | " ;
                $Cas = "Cas".$cas;
                $prof_raspored = $this->db->query("select $Cas from prof_raspored where day = '$dan' and prof_id = $this->profesor_id")->result();
                foreach($prof_raspored as $pro_raspored){
                    $splitterz = explode(".", $pro_raspored->$Cas);
                    if(in_array(0,$splitterz)){
                        echo "<script>
                                alert('nemate upisan cas ovde!');
                            </script>";
                        $prazan_cas = true;
                        header("location: ".current_url());
                    }
                        else {
                            $razred     = $splitterz[0];
                            $odeljenje  = $splitterz[1];
                            $prazan_cas = false;
                        }
                }
                if(isset($_POST['upis_submit'])){
                    foreach($_POST as $key=>$vrednosti){
                        if($key == 'upis_submit' or $key=='upis_casova'){}
                        else {
                        $podatci[$key] = $vrednosti;
                        }
                    }
                    $podatci = implode(" | ",$podatci);
                    $Unos = "Upis".$cas;
                    if(!($prazan_cas)){
                        $this->db->set($Unos, $podatci);
                        $this->db->where('razred', $razred);
                        $this->db->where('odeljenje', $odeljenje);
                        $this->db->where('day', $dan);
                        $this->db->update('upis_izostanci');
                    }
                }
                if(!($prazan_cas)){
                    $result = $this->db->query("select $Unos from upis_izostanci where day = '$dan' and razred = '$razred' and odeljenje = '$odeljenje';")->result();
                        foreach($result as $rez){
                        $uneseno = explode(" | ", $rez->$Unos);
                        $vec_upisano = $uneseno[0];
                        if(empty($vec_upisano)){
                            $vec_upisano = " ";
                        }
                    }
                } else {
                    $vec_upisano = "";
                }
            return $vec_upisano;
            }
        }
        /** Bonus deo => Upis izostanaka **/
        public function Absentees(){
            if(isset($_POST['ukloniUpisanog'])){
                $dan_cas = $this->AbsenteeSplitter("izostanci2");
                $dan  = $dan_cas[0];
                $cas  = $dan_cas[1];
                $izostanak = "Izo".$cas;
                // $this->db->select('*');
                // $this->db->from('upis_izostanci');
                // $this->db->where('razred', $this->razred_id);
                // $this->db->where('odeljenje', $this->odelenje_id);
                // $this->db->where('day', $dan);
                // $query = $this->db->get();
                // $svi_upisani = $query->result();
                $svi_upisani = $this->db->query("select * from upis_izostanci where razred = $this->razred_id and odeljenje = $this->odelenje_id and day = $dan")->result();

                foreach($svi_upisani as $upisan){
                    $dekodiram = json_decode($upisan->$izostanak);
                }
                if(empty($_POST['remove_absentees'])){}
                    else {
                foreach($_POST['remove_absentees'] as $remove){
                    print_r($remove);
                    if(in_array($remove,$dekodiram)){
                       $brisac = array_search($remove,$dekodiram);
                       unset($dekodiram[$brisac]);
                    }
                }
            }   print_r($dekodiram);
                $enkodirano = json_encode($dekodiram);
                echo "<br>";
                print_r($enkodirano);
                $this->db->query("update upis_izostanci set $izostanak = '$enkodirano' where razred = $this->razred_id and odeljenje = $this->odelenje_id and day = $dan");
                // $this->db->set($izostanak, $enkodirano);
                // $this->db->where('razred', $this->razred_id);
                // $this->db->where('odeljenje', $this->odelenje_id);
                // $this->db->where('day', $dan);
                // $this->db->update('upis_izostanci');
            }
            else if(isset($_POST['izostanci']) or isset($_POST['odabraliSmo'])){
                $dan_cas = $this->AbsenteeSplitter("izostanci");
                $dan  = $dan_cas[0];
                $cas  = $dan_cas[1];
                $izostanak = "Izo".$cas;
                if(isset($_POST['odabraliSmo'])){
                    $data = isset($_POST['absentees'])?$_POST['absentees']:0;
                    $unos = json_encode($data);
                    if($_POST['izostanci'] != "0,0"){
                        $this->db->set($izostanak, $unos);
                        $this->db->where('razred', $this->razred_id);
                        $this->db->where('odeljenje', $this->odelenje_id);
                        $this->db->where('day', $dan);
                        $this->db->update('upis_izostanci');
                    }
                }
                $dan_cas = $this->AbsenteeSplitter("izostanci");
                $dan  = $dan_cas[0];
                $cas  = $dan_cas[1];
                $izostanak = "Izo".$cas;
                $this->db->from('upis_izostanci');
                $this->db->where('razred', $this->razred_id);
                $this->db->where('odeljenje', $this->odelenje_id);
                $this->db->where('day', $dan);
                $query = $this->db->get();
                $vec_upisani = $query->result();
                foreach($vec_upisani as $upisano){
                    if($izostanak != "Izo0"){
                    $vec_upisani = json_decode($upisano->$izostanak);
                    }
                }
                return $vec_upisani;
            }
        }
        public function StudentsForAbsenteesm(){
            if(isset($_POST['izostanci']) or isset($_POST['odabraliSmo']) or isset($_POST['ukloniUpisanog'])){
                $dan_cas = $this->AbsenteeSplitter("izostanci");
                $dan  = $dan_cas[0];
                $cas  = $dan_cas[1];
                $Cas    = "Cas".$cas;
                $this->db->select($Cas);
                $this->db->from('prof_raspored');
                $this->db->where('prof_id', $this->profesor_id);
                $this->db->where('day', $dan);
                $result = $this->db->get();
                $result = $result->result();
                
                foreach($result as $razred_od){
                    $razred_od = explode(".",$razred_od->$Cas);
                    $razred    = $razred_od[0];
                    $odeljenje = !empty($razred_od[1])?$razred_od[1]:0;
                    
                }

                if($razred != 0 and $odeljenje != 0){
                    $this->db->from('ucenik');
                    $this->db->where('razred_id', $razred);
                    $this->db->where('odeljenje_id', $odeljenje);
                    $ucenici = $this->db->get();
                    $ucenici = $ucenici->result();
                }

            if(!empty($ucenici)){
               return $ucenici;
            }
        }
    }
}
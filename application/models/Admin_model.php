<?php

class Admin_model extends CI_Model {

    private $razred;
    private $odeljenje;
    private $ucitelj_id;
    private $profesor_id;
    private $last_user_id;
    private $last_ucenik_id; 
    private $last_ucitelj_id;
    private $last_profesor_id;
    private $last_predmet_id;
        
    public function __construct() {
        $this->load->database();
    }

    // Get all students
    public function getAllStudents() {
        $this->db->from('view_ucenik-roditelj');
        $this->db->order_by("razred", "asc");
        $this->db->order_by("odeljenje", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    // Get all parents
    public function getAllParents() {
        $this->db->from('roditelj');
        $query = $this->db->get();
        return $query->result();
    }

    //getting all the teachers
    public function getAllTeachers() {
        $this->db->from('ucitelj');
        $this->db->order_by("razred_id", "asc");
        $this->db->order_by("odeljenje_id", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    // getting all the profesors
    public function getAllProfesors() {   
        $this->db->from('profesor');
        $this->db->join('predmet', 'profesor.predmet_id = predmet.id');
        $this->db->order_by("razred_id", "asc");
        $this->db->order_by("odeljenje_id", "asc");
        $query = $this->db->get();
        return $query->result();
    }

     // getting all the admin
    public function getAllAdmins() {
        $this->db->from('admin');
        $query = $this->db->get();
        return $query->result();
    }

    // getting all the directors
    public function getAllDirectors() {
        $this->db->from('direktor');
        $query = $this->db->get();
        return $query->result();
    }

    // geting on online users
    public function getOnlineUsers() {
        $this->db->select('username, role');
        $this->db->from('user');
        $this->db->where('loged_in', '1');
        $this->db->order_by('role', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    // svi predmeti potrebni za insert/edit profesora
    public function get_predmeti() {
        // $query = $this->db->query('SELECT DISTINCT * FROM `view_massimo_predmeti_po_razredima` where razred >= 5 GROUP BY predmet order by id asc');
        //return $query->result();

        $this->db->from('view_massimo_predmeti_po_razredima');
        $this->db->distinct();
        $this->db->group_by('predmet');
        $this->db->where('razred >=', '5');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    /*--------------------- UPDATE POSTOJECIH KORISIKA --------------------*/
    // Upadate ucenika
    public function updateUcenik($id, $ime, $prezime){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->where('id', $id);
        $this->db->update('ucenik');
    }

    // Upadate uceitelja
    public function updateRoditelj($id, $ime, $prezime, $jmbg){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->set('jmbg', $jmbg);
        $this->db->where('id', $id);
        $this->db->update('roditelj');
    }

    // Upadate roditelja
    public function updateUcitelj($id, $ime, $prezime){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->where('id', $id);
        $this->db->update('ucitelj');
    }

    // Upadate profesora
    public function updateProfesor($id, $ime, $prezime, $predmet){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->set('predmet_id', $predmet);
        $this->db->where('user_id', $id);
        $this->db->update('profesor');
    }

    // Upadate admin
    public function updateAdmin($id, $ime, $prezime){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->where('id', $id);
        $this->db->update('admin');
    }

    // Upadate direktora
    public function updateDirektor($id, $ime, $prezime){
        $this->db->set('first_name', $ime);
        $this->db->set('last_name', $prezime);
        $this->db->where('id', $id);
        $this->db->update('direktor');
    }
    
    /*--------------------------- DELETE USERS ----------------------------*/

    // DELETE ucenik i roditelj
    public function deleteUcenikRoditelj($ucenik_id, $roditelj_id) {
        // Moram da dobijem user_id od roditelja
        $this->db->from('roditelj');
        $this->db->where('id', $roditelj_id);
        $query = $this->db->get();
        $query = $query->result();
        $user_id = $query[0]->user_id;

        // Delete Roditelja
        $this->db->where('id', $roditelj_id);
        $this->db->delete('roditelj');
        // Delete Usera
        $this->db->where('id', $user_id);
        $this->db->delete('user');
        // Delete ucenika
        $this->db->where('id', $ucenik_id);
        $this->db->delete('ucenik');
        // Delete ocene
        $this->db->where('ucenik_id', $ucenik_id);
        $this->db->delete('ocene');
    }

    // DELETE ucitelja
    public function deleteUcitelj($ucitelj_id, $user_id) {
        // uklanjanje staresine - ako je bio
        $this->db->set('ucitelj_id', null);
        $this->db->set('profesor_id', null);
        $this->db->where('ucitelj_id', $ucitelj_id);
        $this->db->update('staresina');

        // Delete ucitelj
        $this->db->where('id', $ucitelj_id);
        $this->db->delete('ucitelj');
        // Delete user
        $this->db->where('id', $user_id);
        $this->db->delete('user');
    }

    // Delete profesor
    public function obrisiProfesor($user_id) {
        // Dobijanje id profesora
        $this->db->from('profesor');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $query = $query->result();
        $profesor_id = $query[0]->id;

        // uklanjanje staresine - ako je bio
        $this->db->set('ucitelj_id', null);
        $this->db->set('profesor_id', null);
        $this->db->where('profesor_id', $profesor_id);
        $this->db->update('staresina');

        // Delete profesor
        $this->db->where('id', $profesor_id);
        $this->db->delete('profesor');
        // Delete user
        $this->db->where('id', $user_id);
        $this->db->delete('user');
    }

    // Delete admin
    public function obrisiAdmin($user_id) {
        // Delete admin
        $this->db->where('user_id', $user_id);
        $this->db->delete('admin');
        // Delete user
        $this->db->where('id', $user_id);
        $this->db->delete('user');
    }

    // Delete direktor
    public function obrisiDirektor($user_id) {
        // Delete admin
        $this->db->where('user_id', $user_id);
        $this->db->delete('direktor');
        // Delete user
        $this->db->where('id', $user_id);
        $this->db->delete('user');
    }

    /*--------------------- PRIKAZ RAZREDA I ODELJENJA --------------------*/
    
    // Prikaz svih razreda i odeljenja
    public function getallClasses() {
        
        $ucitelji = $this->db->query("SELECT * FROM `view_massimo_odeljenje-ucitelj`")->result();
        $ucenici = $this->db->query("SELECT * FROM `ucenik` where razred_id < 5")->result();
        $razredi = [];
        foreach($ucitelji as $ucitelj){
            $razred = $ucitelj->razred;
            $odeljenje = $ucitelj->odeljenje;
            $staresina = $ucitelj->first_name." ".$ucitelj->last_name;
            $ceoRazred = [];
            foreach($ucenici as $ucenik){
                $ucenik_razred = $ucenik->razred_id;
                $ucenik_odeljenje = $ucenik->odeljenje_id;
                if($ucenik_razred == $razred and $ucenik_odeljenje == $odeljenje){
                    array_push($ceoRazred,$ucenik);
                }
            }
            array_push($razredi, $ceoRazred);
        }
        return $razredi;
    }


     /*-------------- UNOS RODITELJA - UCENIKA ----------------------*/

    public function proveriStaresinu($razred, $odeljenje) {
        // Proveram ko je staresina izabranom odeljenju i uzimam njegov ID, da bi mogao uneti kod ucenika u tabelu
        $this->db->select('ucitelj_id, profesor_id');
        $this->db->from('staresina');
        $this->db->where('razred_id', $razred);
        $this->db->where('odeljenje_id', $odeljenje);
        $query = $this->db->get();
        $query = $query->result();
        $this->ucitelj_id  = $query[0]->ucitelj_id;
        $this->profesor_id = $query[0]->profesor_id;
        return $query;
    }

    public function register_ucenik($ucenik_ime, $ucenik_prezime, $razred, $odeljenje) {
         $data_ucenik = array(
            'first_name'   => $ucenik_ime,
            'last_name'    => $ucenik_prezime,
            'razred_id'    => $razred,
            'odeljenje_id' => $odeljenje,
            'ucitelj_id'   => $this->ucitelj_id,
            'profesor_id'  => $this->profesor_id
        );
        // Provere 
        if(empty($ucenik_ime) || empty($ucenik_prezime) || empty($razred) || empty($odeljenje)) {
            exit();
        }
        // Upisivanje ucenika u bazu
        $this->db->insert('ucenik', $data_ucenik);
        $this->last_ucenik_id = $this->db->insert_id();
    }

    // Nakon sto je ubacen ucenik u bazu, treba dodeliti ocene za sve predmete tog razreda
    public function unesi_ocene($razred) {
        $this->db->from('predmeti_po_razredima');
        $this->db->where('razred_id', $razred);
        $predmeti = $this->db->get();
        $predmeti = $predmeti->result();


        // Ubacivanje ocena
        foreach($predmeti as $predmet) {
            $data_ocene = array( 
                'ucenik_id'  => $this->last_ucenik_id,
                'predmet_id' => $predmet->predmet_id,
                'ocena'      => '{"Ocene": ["", "", "", ""], "Zakljucna": [""]}'
            );
            $this->db->insert('ocene', $data_ocene);
        } 
    }

    public function register_user($hash_password, $username, $role) {
        $data_user = array(
            'username' => $username,
            'password' => $hash_password,
            'role'     => $role
        );
        $this->db->insert('user', $data_user);
        $this->last_user_id = $this->db->insert_id(); // Saljem poslednje ubacen ID u property
        return $this->last_user_id;
    }

    public function register_roditelj($jmbg, $first_name, $last_name) {
        $data_roditelj = array(
            'first_name' => $first_name,
            'last_name'  => $last_name,
            'jmbg'       => $jmbg,
            'user_id'    => $this->last_user_id,
            'ucenik_id'  => $this->last_ucenik_id
        );
        $this->db->insert('roditelj', $data_roditelj);
    }

    /*-------------- UNOS UCITELJA ----------------------*/

    public function register_ucitelj($first_name, $last_name/*, $razred, $odeljenje*/) {
        $data_ucitelj = array(
            'first_name'   => $first_name,
            'last_name'    => $last_name,
            'user_id'      => $this->last_user_id/*,
            'odeljenje_id' => $razred,
            'razred_id'    => $odeljenje*/
        );
        $this->db->insert('ucitelj', $data_ucitelj);
        $this->last_ucitelj_id = $this->db->insert_id(); // Uzimanje ID ucitelja  
    }

    /*-------------- UNOS PROFESORA ----------------------*/

    public function register_profesor($ime, $prezime, /*$razred, $odeljenje,*/ $predmet) {
        $data_profesor = array(
            'first_name'   => $ime,
            'last_name'    => $prezime,
            'user_id'      => $this->last_user_id,
            'predmet_id'   => $predmet
        );
        $this->db->insert('profesor', $data_profesor);
        $this->last_profesor_id = $this->db->insert_id(); // Uzimanje ID profesora

        // Unos potreban za raspored
        $prof_id = $this->last_profesor_id;
            for($dan=1;$dan<6;$dan++){
                $sql1 = "INSERT INTO `prof_raspored` (`id`,`day`,`Cas1`, `Cas2`, `Cas3`, `Cas4`, `Cas5`, `Cas6`, `Cas7`, `prof_id`) VALUES (NULL,'$dan','0', '0', '0', '0', '0', '0', '0', '$prof_id');";
                $result1 = $this->db->query($sql1);
            }
    }

    /*------------ UNOS ADMINISTRATORA ------------------*/

    public function register_admin($ime, $prezime) {
        $data_admin = array(
            'first_name' => $ime,
            'last_name'  => $prezime,
            'user_id'    => $this->last_user_id
        );
        $this->db->insert('admin', $data_admin);
        echo $ime.$prezime;
    }

    /*------------ UNOS DIREKTORA ----------------------*/

    public function register_direktor($ime, $prezime) {
        $data_direktor = array(
            'first_name' => $ime,
            'last_name'  => $prezime,
            'user_id'    => $this->last_user_id
        );
        $this->db->insert('direktor', $data_direktor);
    }





    /*-------------------- RAZREDI -------------------------*/

    // Dobijanje svih trenutnih odeljenja u bazi za select menu
    public function getOdeljenja() {
        $this->db->from('odeljenje');
        $query = $this->db->get();
        $query = $query->result();
        return $query;
    }

    // NERASPOREDJENI ucitelji
    public function nerasporedjeniUcitelji() {
        $this->db->from('ucitelj');
        $this->db->where('razred_id', null);
        $this->db->where('odeljenje_id', null);
        $query = $this->db->get();
        $query = $query->result();
        return $query;
    }

    // NERASPOREDJENI profesori
    public function nerasporedjeniProfesori() {
        $this->db->from('profesor');
        $this->db->where('razred_id', null);
        $this->db->where('odeljenje_id', null);
        $query = $this->db->get();
        $query = $query->result();
        return $query;
    }

    // NERASPOREDJENI ucenici
    public function nerasporedjeniUcenici() {
        $this->db->from('ucenik');
        $this->db->where('razred_id', null);
        $this->db->where('odeljenje_id', null);
        $query = $this->db->get();
        $query = $query->result();
        return $query;
    }

    // UKLONI staresinu iz odeljenja
    public function ukloniStaresinu($id, $razred, $odeljenje) {
        if($razred <= 4){
            // Tabela ucitelj
            $this->db->set('razred_id', null);
            $this->db->set('odeljenje_id', null);
            $this->db->where('id', $id);
            $this->db->update('ucitelj');
            // Tabela staresina
            $this->db->set('ucitelj_id', null);
            $this->db->where('razred_id', $razred);
            $this->db->where('odeljenje_id', $odeljenje);
            $this->db->update('staresina');

        }elseif($razred >=5){
            $this->db->set('razred_id', null);
            $this->db->set('odeljenje_id', null);
            $this->db->where('id', $id);
            $this->db->update('profesor');
            // Tabela staresina
            $this->db->set('profesor_id', null);
            $this->db->where('razred_id', $razred);
            $this->db->where('odeljenje_id', $odeljenje);
            $this->db->update('staresina');
        }
    }

    // UKLONI ucenika iz odeljenja
    public function ukloniUcenika($id, $razred) {
        /*-- uklanjanje iz chatkit-a --*/
        // uzimanje user-id roditelja
        $this->db->select('user_id');
        $this->db->from('roditelj');
        $this->db->where('ucenik_id', $id);
        $query = $this->db->get();
        $query = $query->result();
        $user_id = $query[0]->user_id;

        // uzimanje id ucitelja/profesora
        if($razred <= 4){
            $this->db->from('ucitelj');
            
        }

        // ucenik
        $this->db->set('razred_id', null);
        $this->db->set('odeljenje_id', null);
        $this->db->set('profesor_id', null);
        $this->db->set('ucitelj_id', null);
        $this->db->where('id', $id);
        $this->db->update('ucenik');

        

    }

    // DODELI ucitelja u odeljenje
    public function dodeliUcitelja($id, $razred, $odeljenje) {
        // Tabela ucitelj
        $this->db->set('razred_id', $razred);
        $this->db->set('odeljenje_id', $odeljenje);
        $this->db->where('id', $id);
        $this->db->update('ucitelj');
        // Tabela staresina
        $this->db->set('ucitelj_id', $id);
        $this->db->where('razred_id', $razred);
        $this->db->where('odeljenje_id', $odeljenje);
        $this->db->update('staresina');
    }

    // DODELI profesora u odeljenje
    public function dodeliProfesora($id, $razred, $odeljenje) {
        // Tabela profesor
        $this->db->set('razred_id', $razred);
        $this->db->set('odeljenje_id', $odeljenje);
        $this->db->where('id', $id);
        $this->db->update('profesor');
        // Tabela staresina
        $this->db->set('profesor_id', $id);
        $this->db->where('razred_id', $razred);
        $this->db->where('odeljenje_id', $odeljenje);
        $this->db->update('staresina');
    }

    // DODELI ucenika u odeljneje
    public function dodeliUcenika($id, $razred, $odeljenje) {
        // prvo dobijam ucitelja/profesora koji predaje tom odeljenju
        if($razred <= 4){
            $this->db->from('view_massimo_odeljenje-ucitelj');
            $this->db->where('razred', $razred);
            $this->db->where('odeljenje', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            $ucitelj_id  = $query[0]->id;
            $profesor_id = null;
        }else {
            $this->db->from('view_massimo_odeljenje-profesor');
            $this->db->where('razred', $razred);
            $this->db->where('odeljenje', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            $profesor_id = $query[0]->id;
            $ucitelj_id  = null;
        }
        // Zatim unosim ucenika
        $this->db->set('profesor_id', $profesor_id);
        $this->db->set('ucitelj_id', $ucitelj_id);
        $this->db->set('razred_id', $razred);
        $this->db->set('odeljenje_id', $odeljenje);
        $this->db->where('id', $id);
        $this->db->update('ucenik');
    }

    // DODELI ocene uceniku za novi razred
    public function dodeliOcene($id, $razred){
        // Prvo brisemo sve stare ocene iz prethodnog razreda
        $this->db->where('ucenik_id', $id);
        $this->db->delete('ocene');
        // Uzimamo listu predmeta koji se predaju u tom razredu
        $this->db->select('predmet_id');
        $this->db->from('predmeti_po_razredima');
        $this->db->where('razred_id', $razred);
        $predmeti = $this->db->get();
        $predmeti = $predmeti->result();
        // Dodavanje ocena u te predmete
        foreach($predmeti as $predmet) {
            $data_predmet = array( 
                'ucenik_id'  => $id,
                'predmet_id' => $predmet->predmet_id,
                'ocena'      => '{"Ocene": ["", "", "", ""], "Zakljucna": [""]}'
            );
            $this->db->insert('ocene', $data_predmet);
        }
    }



    // PRIKAZ ucenika po razredima
    public function getRazredi($razred, $odeljenje) {

        // Prikaz svih razreda i svih odeljenja
        if($razred=='sve' && $odeljenje=='sve') {
            // Prikaz za sve razrede i sva odeljenja
            $response = '';
            for($razred=1; $razred<=8; $razred++) {
                // Dobijanje koliko ima odeljenja
                $this->db->from('odeljenje');
                $odeljenja = $this->db->get();
                $odeljenja = $odeljenja->result();

                // Pravljenje tabele za svako odeljenje
                foreach($odeljenja as $odeljenje) {
                    $this->db->from('view_massimo_odeljenje-ucenici');
                    $this->db->where('razred', $razred);
                    $query = $this->db->get();
                    $query = $query->result();

                    $response.=
                    "<div class='razred odeljenja'>
                        <table class='table table-borderless table-hover'>
                            <thead>
                            <tr class='head'>
                                <th colspan='2'>Razred $razred / Odeljenje $odeljenje->odeljenje</th>
                            </tr>
                            </thead>";
                            // Dobijanje staresine za to odeljenje
                            if($razred <= 4){
                                $this->db->from('view_massimo_odeljenje-ucitelj');
                                $this->db->where('razred', $razred);
                                $this->db->where('odeljenje', $odeljenje->odeljenje);
                                $staresine = $this->db->get();
                                $staresine = $staresine->result();

                            }else if($razred >=5) {
                                $this->db->from('view_massimo_odeljenje-profesor');
                                $this->db->where('razred', $razred);
                                $this->db->where('odeljenje', $odeljenje->odeljenje);
                                $staresine = $this->db->get();
                                $staresine = $staresine->result();
                            }
                            if(sizeof($staresine)==0){
                    $response.= "<tr>
                                    <td>Starešina:</td>
                                    <td><span class='text-danger'>Nije dodeljen</span></td>
                                </tr>";}
                            foreach($staresine as $staresina) {
                    $response.= "<tr>
                                    <td>";
                                    if($razred <=4) {
                    $response.=         "Učitelj:";
                                    }else if($razred >=5) {
                    $response.=         "Profesor:";}
                    $response.=    "</td>
                                    <td>$staresina->first_name $staresina->last_name</td>
                                </tr>";}
                            // Dobijanje ucenika za to odeljenje
                            $this->db->from('view_massimo_odeljenje-ucenici');
                            $this->db->where('razred', $razred);
                            $this->db->where('odeljenje', $odeljenje->odeljenje);
                            $ucenici = $this->db->get();
                            $ucenici = $ucenici->num_rows();
                    $response.="<tr>
                                    <td>Učenici:</td>";
                                    if($ucenici == 0){
                    $response.=    "<td><span class='text-danger'>Nema učenika</span></td>";      
                                    }else{ 
                    $response.=    "<td>$ucenici</td>";}
                    $response.="</tr>
                        </table>
                    </div>";
                }
            }
            return $response;
            

        // Prikaz svih odeljenja jednog razreda
        }else if($odeljenje=='sve') {
            // Dobijanje koliko ima odeljenja
            $this->db->from('odeljenje');
            $odeljenja = $this->db->get();
            $odeljenja = $odeljenja->result();
            $response = '';
            // Pravljenje tabele za svako odeljenje
            foreach($odeljenja as $odeljenje) {
                $this->db->from('view_massimo_odeljenje-ucenici');
                $this->db->where('razred', $razred);
                $query = $this->db->get();
                $query = $query->result();

                $response.=
                    "<div class='razred odeljenja'>
                        <table class='table table-dark table-hover'>
                            <thead>
                            <tr class='head'>
                                <th colspan='3'>Razred $razred / Odeljenje $odeljenje->odeljenje</th>
                            </tr>
                            </thead>";
                            // Dobijanje staresine za to odeljenje
                            if($razred <= 4){
                                $this->db->from('view_massimo_odeljenje-ucitelj');
                                $this->db->where('razred', $razred);
                                $this->db->where('odeljenje', $odeljenje->odeljenje);
                                $staresine = $this->db->get();
                                $staresine = $staresine->result();
                            }else if($razred >=5) {
                                $this->db->from('view_massimo_odeljenje-profesor');
                                $this->db->where('razred', $razred);
                                $this->db->where('odeljenje', $odeljenje->odeljenje);
                                $staresine = $this->db->get();
                                $staresine = $staresine->result();
                            }
                            if(sizeof($staresine)==0){
                $response.=    "<tr class='staresina'>
                                    <td>Starešina:</td>
                                    <td><p class='text-danger'>Nije dodeljen</p></td>
                                    <td><i class='fas fa-user-plus iconDodaj' onclick='dodajStaresinu($razred, $odeljenje->odeljenje)' title='Dodaj starešinu'></i></td>
                                </tr>";}
                            foreach($staresine as $staresina) {
                $response.=     "<tr class='staresina'>
                                    <td>";
                                    if($razred <=4) {
                $response.=             "Učitelj:";
                                    }else if($razred >=5) {
                $response.=             "Profesor:";}
                $response.=        "</td>
                                    <td>$staresina->first_name $staresina->last_name</td>
                                    <td><i class='fas fa-user-minus iconObrisi' onclick='ukloniStaresinu($staresina->id, $staresina->razred, $staresina->odeljenje)' title='Ukloni starešinu'></i></td>
                                </tr>";}
                            // Dobijanje ucenika za to odeljenje
                            $this->db->from('view_massimo_odeljenje-ucenici');
                            $this->db->where('razred', $razred);
                            $this->db->where('odeljenje', $odeljenje->odeljenje);
                            $ucenici = $this->db->get();
                            $ucenici = $ucenici->result();
                $response.=    "<tr>
                                    <td>Učenik:</td>
                                    <td></td>";
                                    if(sizeof($staresine)==0){
                $response.=         "<td><i class='fas fa-user-lock' title='Nema starešine'></i></td>";
                                    }else{
                $response.=         "<td><i class='fas fa-user-plus iconDodaj' onclick='dodajUcenika($staresina->razred,                                                          $staresina->odeljenje)' title='Dodaj učenika'></i></td>";
                                    }
                $response.=    "</tr>";          
                            if(sizeof($ucenici)==0) {
                $response.=     "<tr>
                                    <td>0</td>
                                    <td><span class='text-danger'>Nema učenika</span></td>
                                    <td></td>
                                </tr>";}
                            $i=1; 
                            foreach($ucenici as $ucenik) {
                $response.=     "<tr>
                                    <td>$i</td>
                                    <td>$ucenik->ucenik_ime $ucenik->ucenik_prezime</td>
                                    <td><i class='fas fa-user-minus iconObrisi' onclick='ukloniUcenika($ucenik->ucenik_id, $ucenik->razred)' title='Ukloni učenika'></i></td>
                                </tr>"; $i++;}
                $response.= "</table>
                    </div>";
            }
            return $response;
         
        // Prikaz samo za jedno odeljenje    
        }else {
            $this->db->from('view_ucenik-roditelj');
            $this->db->where('razred', $razred);
            $this->db->where('odeljenje', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            return $query;
        }
        
    }

    // Prikaz razrednog staresina za izabrani razred
    public function getStaresina($razred, $odeljenje) {
        // Ucitelj
        if($razred <= 4) {
            $this->db->from('view_massimo_odeljenje-ucitelj');
            $this->db->where('razred', $razred);
            $this->db->where('odeljenje', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            return $query;
        // Profesor    
        }else if($razred >= 5) {
            $this->db->from('view_massimo_odeljenje-profesor');
            $this->db->where('razred', $razred);
            $this->db->where('odeljenje', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            return $query;
        }
    }

    // Prikaz razrednog staresina za izabrani razred
    public function getStaresinaID($razred, $odeljenje) {
        // Ucitelj
        if($razred <= 4) {
            $this->db->select('user_id');
            $this->db->from('ucitelj');
            $this->db->where('razred_id', $razred);
            $this->db->where('odeljenje_id', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            return $query;
        // Profesor    
        }else if($razred >= 5) {
            $this->db->select('user_id');
            $this->db->from('profesor');
            $this->db->where('razred_id', $razred);
            $this->db->where('odeljenje_id', $odeljenje);
            $query = $this->db->get();
            $query = $query->result();
            return $query;
        }
    }






    /*-------------------- PREDMETI -------------------------*/
    
    // Prikazi sve predmete po razredima
    public function predmetiPoRazredima() {  
        $odgovor = "";
        for($i=1; $i<=8; $i++) { // Za 8 razreda ponavljam svaki put upit
            $this->db->from('view_massimo_predmeti_po_razredima');
            $this->db->where('razred', $i);
            $query = $this->db->get();
            $query = $query->result();

            $odgovor.= "<div class='razred'>
                            <table class='table table-dark table-hover'>
                                <tr>
                                    <th class='headline' colspan='3'>Razred - $i</th>
                                </tr>";
                                if(sizeof($query)==0) {
            $odgovor.=          "<tr>
                                    <td>0</td>
                                    <td colspan='2'>Nema dodeljenog predmeta</td>
                                </tr>"; 
                                } else { $br=1;
                                foreach($query as $naziv){ 
            $odgovor.=          "<tr>
                                    <td>$br</td>
                                    <td>$naziv->predmet</td>
                                    <td><i class='fas fa-trash-restore iconObrisi' onclick='obrisiPredmet($naziv->id, $i)'></i></td>
                                </tr>"; $br++; }
                            }
            $odgovor.=      "</table>
                        </div>";
        }
        return $odgovor;
    }

    // Unos novog predmeta
    public function unesiPredmet($predmet, $razredi) {
        // Unos u tabelu Predmet
        $data_predmet = array( 
            'predmet' => $predmet
        );
        $this->db->insert('predmet', $data_predmet);
        $this->last_predmet_id = $this->db->insert_id();

        // Unos u tabelu Predmeti po razredima
        foreach($razredi as $razred) {
            $data_predmet = array( 
                'predmet_id' => $this->last_predmet_id,
                'razred_id'  => $razred
            );
            $this->db->insert('predmeti_po_razredima', $data_predmet);
        }
    }

    // Brisanje predmeta
    public function obrisiPredmet($predmet_id, $razred) {
        // Brisanje iz tabele Predmeti po razredima
        $this->db->where('predmet_id', $predmet_id);
        $this->db->where('razred_id', $razred);
        $this->db->delete('predmeti_po_razredima');

        // Brisanje iz tabele Predmet

        // Prvo provera da li se u opste predaje u nekom razredu
        $this->db->from('predmeti_po_razredima');
        $this->db->where('predmet_id', $predmet_id);
        $query = $this->db->get();
        $query = $query->result();
        var_dump($query); 
        
        //Ako se predmet ne predaju ni u jednom razredu, brise se iz tabele Predmet
        if(sizeof($query) == 0) {
            // Brisanje iz tabele Predmet
            $this->db->where('id', $predmet_id);
            $this->db->delete('predmet');
        }
    }




   /*-------------- LOGOVANJE ----------------------*/


    // checking for matching username
    public function match_username($username) {
        $this->db->where('username', $username);
        $user = $this->db->get('user');
            if($user->num_rows() > 1) {
                return $user->result(); // result() returns array of objects
            } elseif($user->num_rows() == 1) {
                return $user->row(); // row() returns one object
            } else {
                return false;
            }
    }

    // loging in
    public function login($username, $password) {
        // validate
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $result = $this->db->get('user');
        if($result->num_rows() == 1) {
            return $result->row(); // returnig matched user from table
        } else {
            return false; 
        }
    }




    
       /*---------------  Raspored  -------------*/
    public function AdminScheduleEdit($razred,$odeljenje){
    if(isset($_POST['unesi_promene'])){
            $raspored = [];
            foreach($_POST as $post=>$value){
                if($post == "unesi_promene" or $post == "razred" or $post == "odeljenje"){
        
                }
                else {
                $splitter   = explode("|",$post);
                $dan        = $splitter[0];
                $cas        = $splitter[1];
                $raspored[$dan][$cas] = $value;
                }
                
            }
            foreach($raspored as $dan=>$casovi){
                foreach($casovi as $cas=>$predmet){
                    $Cas = "Cas".$cas;
                    $prof_unos = $razred.".".$odeljenje;
                    $query1 = "UPDATE `raspored` SET `$Cas` = '$predmet' WHERE `raspored`.`odeljenje_id` = $odeljenje and razred_id = $razred and `raspored`.`day` = $dan;";
                    $update = $this->db->query($query1);
                    if($razred >4){
                    $prof_predmet = $this->db->query("select * from profesor where predmet_id = $predmet")->result();
                        foreach($prof_predmet as $prof_pre){
                            $prof_id = $prof_pre->id;
                            $query2 = "UPDATE `prof_raspored` SET `$Cas` = '$prof_unos' WHERE `prof_id` = $prof_id and `day` = $dan;";
                            $result2 = $this->db->query($query2);
                        }
                    }
                }
            }
            $razr  = 1;
            $odelj = 1;
            $razr  = isset($_POST['razred'])?$_POST['razred']:1;
            $odelj = isset($_POST['odeljenje'])?$_POST['odeljenje']:1;
            $raz_ode = [$razr,$odelj];
            return $raz_ode;
            header("location: ".current_url());
        } 
       
    }
    public function AdminScheduleView($razred, $odeljenje){
        $predmeti_razreda = $this->db->query("select * from predmeti_po_razredima where razred_id = $razred")->result();
        $raspored = "";
        for($cas=1;$cas<8;$cas++){
           $raspored .="<tr style='text-align:center;'>
                    <td>$cas</td>";
                for($dan=1;$dan<6;$dan++){
                    $raspored.= "<td>";
                    $predmet_iz_rasporeda = $this->db->query("select * from raspored where day = '$dan' and odeljenje_id = $odeljenje and razred_id = $razred")->result();
                    $Cas = "Cas".$cas;
                    foreach($predmet_iz_rasporeda as $predmet){
                        $selektor = $predmet->$Cas;
                        $ime_predmet = $this->db->query("select * from predmet where id = '$selektor'")->result();
                        if(empty($ime_predmet)){
                            $vrednost   = 0;
                            $predmeti   = "";
                        } else {
                            foreach($ime_predmet as $selektovan_predmet){
                                $vrednost   = $selektovan_predmet->id;
                                $predmeti    = $selektovan_predmet->predmet;
                            }
                        }
                    }
                  $data_za_formu = $dan."|".$cas;  
                  $raspored .= "<select name='$data_za_formu' class='browser-default custom-select custom-select mb-3 rasporedInput' selected>
                       <option value=0></option>
                       <option value='$vrednost' selected>$predmeti</option>";
                       
                    foreach($predmeti_razreda as $predmeti_r){
                            $uslov = $predmeti_r->predmet_id;
                            $predmet_id = $this->db->query("select * from predmet where id = '$uslov'")->result();
                            foreach($predmet_id as $predmet_naziv){
                                $predmet_ime = $predmet_naziv->predmet;
                                $raspored.="<option value='$predmeti_r->predmet_id'>$predmet_ime</option>";
                            }
                        }
                    }
                    $raspored .= "</select>    
                    </td>";
                }
            $raspored .="</tr>";
        return $raspored;
    }
}
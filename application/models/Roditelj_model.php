<?php

    class Roditelj_model extends CI_Model {

        private $roditelj_id;
        private $ucenik_id;
        private $razred_id;
        private $odeljenje_id;

        public function __construct() {
            $this->load->database();
            echo $this->roditelj_id;
        }
 
        public function get_roditelj($sesija) {
            
            $user_id = $sesija['user_id'];
            $this->db->from('roditelj');
            $this->db->where('user_id', $user_id); 
            $query = $this->db->get();
            $query->result();
            $this->ucenik_id   = $query->result_object[0]->ucenik_id;
            $this->roditelj_id = $query->result_object[0]->id;
            return $query;       
        }

        public function get_dete() {

            $this->db->from('view_massimo');
            $this->db->where('id', $this->ucenik_id);  // id_ucenika koji se dobija iz $ucenik_id iz funkcije get_roditelj();
            $query  = $this->db->get();
            $query->result();
            if(sizeof($query->result()) == 0) {
                echo "Trenutno učenik nije dodeljen u odeljenje.<br> Kontaktirajte Vašu školu radi više informacija.<br>";
                echo "<a href='admin/logout'>Odjavi se!</a>";
                exit();
            }
            $this->razred_id = $query->result_object[0]->razred;
            $this->odeljenje_id = $query->result_object[0]->odeljenje;
            return $query->result();
            //na osnovu podataka iz get_roditelj radi filtraciju, daje nam razred, odeljenje i vraca ucenika
        }

        public function get_staresina() {
            // ako je razred <4 onda Ucitelj, u suprotnom >5 onda Profesor
            if($this->razred_id <= 4) {
                $this->db->from('view_massimo_odeljenje-ucitelj');
                $this->db->where('razred', $this->razred_id);                        
                $this->db->where('odeljenje', $this->odeljenje_id);
                $query = $this->db->get();
                return $query->result();
            } elseif ($this->razred_id >= 5) {
                $this->db->from('view_massimo_odeljenje-profesor');
                $this->db->where('razred', $this->razred_id);                        
                $this->db->where('odeljenje', $this->odeljenje_id);
                $query = $this->db->get();
                return $query->result();
            }
        }
        public function showMyRequest()
        {
            // $query = $this->db->query("SELECT dan,  WHERE roditelj_id = '$this->roditelj_id' ORDER BY id ASC");
            // return $query->result();

            $this->db->from('zahtevi');
            $this->db->select('dan, time, approved');
            $this->db->where('roditelj_id', $this->roditelj_id);
            $this->db->order_by('id', 'DESC');
            $this->db->limit(10);
            $query = $this->db->get();
            return $query->result();

        }
        public function headTeacher()
        {

            $query = $this->db->query("SELECT id, first_name, last_name FROM ucitelj WHERE razred_id = '$this->razred_id' and odeljenje_id = '$this->odeljenje_id'");
            return $query->result();

        }

        public function requestAppointmentForTeacher($staresina, $date, $time)
        {

           $query = $this->db->query("INSERT INTO `zahtevi` (`id`, `profesor_id`, `ucitelj_id`, `roditelj_id`, `dan`, `time`, `approved`) VALUES (NULL, NULL, '$staresina', '$this->roditelj_id', '$date', '$time', 'ceka odobrenje')");
            return;
        }


        public function requestAppointmentForProfesor($staresina, $date, $time)
        {

            $this->db->query("INSERT INTO `zahtevi` (`id`, `profesor_id`, `ucitelj_id`, `roditelj_id`, `dan`, `time`, `approved`) VALUES (NULL, '$staresina', NULL, '$this->roditelj_id', '$date', '$time', 'ceka odobrenje')");
            return;
        }

        public function get_otvorena_vrata(){
            echo $this->ucenik_id;
            $this->db->from('view_otvorena_vrata_roditelj');
             $this->db->where('ucenik', $this->ucenik_id);                        
             
             
             $query = $this->db->get();
             return $query->result();
         }
         public function get_otvorena_vrata_ucitelj(){
            echo $this->ucenik_id;
            $this->db->from('view_otvorena_vrata_ucitelj');
             $this->db->where('ucenik_id', $this->ucenik_id);                        
             
             
             $query = $this->db->get();
             return $query->result();
         }
         public function get_otvorena_vrata_zakazivanje(){
            $this->db->from('view_otvorena_vrata_zakazivanje');
            $this->db->where('ucenik', $this->ucenik_id);                        
             
             
             $query = $this->db->get();
             return $query->result();
         }
         public function zakazi_otvorenavrata_profesor(){
            if(isset($_POST['profesor_id'])){
                
                $data = array(
                    'roditelj_id' => $_POST['roditelj_id'],
                    'profesor_id' => $_POST['profesor_id'],
                    'status' => 1
            );
            
            $this->db->insert('otvorena_vrata', $data);
            header("location: ".current_url());
            

            }
        }
        public function zakazi_otvorenavrata_ucitelj(){
            if(isset($_POST['ucitelj_id'])){
                
                $data = array(
                    'roditelj_id' => $_POST['roditelj_id'],
                    'ucitelj_id' => $_POST['ucitelj_id'],
                    'status' => 1
            );
            
            $this->db->insert('otvorena_vrata', $data);
            header("location: ".current_url());

            }
        }

        /*--------------- OPRAVDANJA ------------*/

        // Slanje opravdanja
        public function opravdanje_posalji($razred, $user_id, $staresina_id, $sadrzaj, $slika, $slika_tmp) {

            // Dobijanje roditelja id
            $this->db->from('roditelj');
            $this->db->where('user_id', $user_id);
            $query = $this->db->get();
            $query = $query->result();
            $roditelj_id = $query[0]->id;

            // Provera da li je staresina ucitelj ili profesor
            if($razred <= 4) {
                $this->db->from('ucitelj');
                $this->db->where('id', $staresina_id);
                $query = $this->db->get();
                $query = $query->result();
                $ucitelj_id = $query[0]->id;
                $profesor_id = null;

            }else if($razred >= 5) {
                $this->db->from('profesor');
                $this->db->where('id', $staresina_id);
                $query = $this->db->get();
                $query = $query->result();
                $profesor_id = $query[0]->id;
                $ucitelj_id = null;
            }

            // Upload slike
            $slika = time().$slika;
            if(@move_uploaded_file($slika_tmp, "uploads/".$slika)) {

                // Unos u tabelu Opravdanja
                $data = array(
                    'roditelj_id' => $roditelj_id,
                    'ucitelj_id'  => $ucitelj_id,
                    'profesor_id' => $profesor_id,
                    'sadrzaj'     => $sadrzaj,
                    'slika'       => $slika
                );
                $this->db->insert('opravdanja', $data);
                // success
                echo "Uspesno poslato";
            }else {
                echo "Greska";
            }

                

        }
    }
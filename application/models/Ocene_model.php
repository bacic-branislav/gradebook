<?php
 class Ocene_model extends CI_Model {

    public function __construct() {
        $this->load->database();
       
    }

    public function AverageGrade($id){
        $result = $this->db->query("select * from ocene where ucenik_id = $id")->result();
        $Ocene_za_prosek = [];
        foreach($result as $ocena_predmet){
            $dekodirano  = json_decode($ocena_predmet->ocena);
            $zakljucnaOC = $dekodirano->Zakljucna[0];
            if(empty($zakljucnaOC)){
                $zakljucnaOC = 0;
            }
            array_push($Ocene_za_prosek, $zakljucnaOC);
        }
        if(in_array( 0 , $Ocene_za_prosek)){
            $prosek = " ";
        } else if (in_array( 1 , $Ocene_za_prosek)){
            $prosek = " ";
        } else {
            $prosek = 0;
            foreach($Ocene_za_prosek as $ocena){
                $prosek += $ocena;
            }
            $broj_predmeta = count($Ocene_za_prosek);
            $prosek /= $broj_predmeta;
            $prosek = round($prosek,2);
        }
        return $prosek;
    }
}
<?php

class Schedule_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        
    }

    /**************** Prikaz za Ucitelja/Roditelja ****************/
    public function getSchedule($_razred, $_odeljenje){
        //Svako odelenje ima po 5 entria u bazi podataka za po 5 dana
        $razred    = $_razred; //vuce ovaj podatak iz kontrolera
        $odeljenje = $_odeljenje; //podatak iz kontrolera
        $dan       = 1; //dummy vrednost
        $prikaz=''; //odavde krece konkatinacija prikaza koji pravi zasebne <tr> po svakom loopu 
        for($cas=1;$cas<8;$cas++){ //pravi 7 redova za 7 casova koje postavljamo kao max u nasoj skoli
            $prikaz.= "<tr style='text-align:center;'>
                        <td>$cas</td>";
                for($dan=1;$dan<6;$dan++){ //za svaki red pravimo po 5 kolona za 5 dana
                    //dobavljanje podataka iz baze na osnovu parametara odeljenje i razred. Takodje se proverava po danima posto je u loopu $dan
                    $uc_raspored    = $this->db->query("select * from raspored where odeljenje_id = '$odeljenje' and razred_id = '$razred' and day = '$dan';")->result();
                    //ako je przno onda ne pokazuje nista (tipa kad odelenju nije dodeljen raspored ili profesoru)
                    if(empty($uc_raspored)){
                        $prikaz.= ""; 
                    } else {
                    $cas_za_prikaz      = "Cas".$cas; //Casovi u bazi su nazvani Cas1,Cas2,Cas3... pa se ovde radi konkatinacija
                    $cas_za_prikaz      = $uc_raspored[0]->$cas_za_prikaz; //uzimamo prvi result iz $uc_raspored i biramo polje po $cas, dobija se broj, tipa 11,10 tj ID predmeta
                    $ime_predmeta = $this->db->query("select * from predmet where id = '$cas_za_prikaz'")->result(); //ovde se poredi ID predmeta dobijenog i uzima se njegovo ime iz tabele predmet
                    if(empty($ime_predmeta)){
                        $prikaz.= "<td colspan='1'>";
                        $prikaz.= "/";
                    } else {
                    foreach($ime_predmeta as $predmet){
                        $ime_predmeta = $predmet->predmet;
                        $prikaz.= "<td colspan='1'>";
                        $prikaz.= "$ime_predmeta"; //ispisujemo ime predmeta
                        }
                    }
                }
            }
            $prikaz.= "</tr>";
        }
        return $prikaz; //vracanje vrednosti rasporeda
    }
    public function getScheduleProf($prof_id){
        $prikaz = "";
        for($cas=1;$cas<8;$cas++){
            $prikaz .= "<tr style='text-align:center;'>
            <td>$cas</td>";
            for($dan=1;$dan<6;$dan++){
                $Cas = "Cas".$cas;
                $sql1 = "select $Cas from prof_raspored where prof_id = '$prof_id' and day = $dan";
                $result1 = $this->db->query($sql1)->result();
                //print_r($result1);
                foreach($result1 as $cass=>$razred_odeljenje){
                    foreach($razred_odeljenje as $casss=>$odelj){
                        //echo "<br>".$odelj . "<br>";
                        if($odelj==0){
                            $prikaz.="<td>/</td>";
                        } else {
                            $prikaz.="<td>$odelj</td>";
                        }
                    }
                }
                
            }
            $prikaz.= "</tr>";
            
        }
        return $prikaz;
    }


}
<?php
class Htmltopdf_model extends CI_Model
{
	function fetch()
	{
		$this->db->order_by('id', 'ASC');
		return $this->db->get('ucenik');
	}
	function fetch_single_details($customer_id)
	{
		$this->db->where('id', $customer_id);
		$data = $this->db->get('ucenik');
		$output = '<table width="100%" cellspacing="5" cellpadding="5" style="background-color:rgba(252,248,203,1);">';
		foreach($data->result() as $row)
		{	
			$ocene = $this->db->query("select * from ocene where ucenik_id = $row->id")->result();
			//print_r($ocene);
			$output .= '
			<tr style="width:100%;">
				<td width="100%">
					<p width="60%">Osnovna Skola: ______<u style="border-bottom:1px solid black;"><i>Acina Skolica</i></u>___________ , u: _____________________, datum: _______________</p>
				</td>
				
				
			</tr>
			
			<tr>
				<td width="100%">
					<p style="text-align:center;font-size:18px;">Drugi ciklus osnovnog obrazovanja i vaspitanja</p>
					<p style="text-align:center;font-size:45px;">Svedocanstvo</p>
					<p style="text-align:center;"><b><u style="border-bottom:1px solid black;">____________________________'.$row->first_name.' ' .$row->last_name. '____________________________<u></b></p>
					
					
				</td>
			</tr>
			</table>
			<div style="width:100%;background-color:rgba(252,248,203,1);">
			<table width="90%" cellspacing="3" cellpadding="3" style="background-color:rgba(252,248,203,1);  margin:0 auto;">>
			';
			foreach($ocene as $ocena){
				$predmeti = $this->db->query("select * from predmet where id = $ocena->predmet_id")->result();
				$ocene_dekodirane = json_decode($ocena->ocena);
				$output .= '
					<tr>
						<td colspan="1">
							<p>'. $ocena->predmet_id . '</p>
						</td>';
						foreach($predmeti as $predmet){
							$output .= '<td colspan="1">
								<p>'. $predmet->predmet . '</p>
							</td>';
						}
				$output .= '
						<td colspan="1" style="background-color:rgba(0,0,0,0.3); text-align:center;">
							<p>----------------------------<b><i>(  '.$ocene_dekodirane->Zakljucna[0].'  )</i></b></p>
						</td>
						<td colspan="1">

						</td>
					</tr>';
			}
		}
		$output .= '
		<tr>
			<td colspan="4" style="text-align:center;padding-top:15px;border-top:1px dotted black;">Ucenik-ca je sa _________________ ('.$this->Ocene_model->AverageGrade($row->id).') opstim uspehom zavrsio-la ________________ razred osnovne skole.</td> 
		</tr>
		';
		$output .= '</table></div>
		<div style="text-align:center;background-color:rgba(252,248,203,1);">
			<div style="float:left;width:50%;background-color:rgba(252,248,203,1);">
					<p>Odeljenski staresina: </p>
					<p>________________________________</p>
			</div>
			<div style="float:right;width:50%;background-color:rgba(252,248,203,1);">
					<p>Direktor: </p>
					<p>________________________________</p>
			</div>
		</div>		
		';
		return $output;
	}
}

?>
<?php
class User_model extends CI_Model {

    protected $last_ucenik_id;
    protected $last_user_id;

    public function get_ucitelji() {
        $this->db->from('ucitelj');
        $query = $this->db->get();
        return $query->result(); 
    }

    public function get_profesori() {
        $this->db->from('profesor');
        $query = $this->db->get();
        return $query->result(); 
    }

    public function register_ucenik($deteime, $deteprezime, $razred, $odeljenje, $ucitelj_id, $profesor_id) {

        if($ucitelj_id == 0 && $profesor_id == 0) {
            $ucitelj_name = null;
            $professor_name = null;
        } elseif($ucitelj_id == 0) {
            $professor_name = $profesor_id;
            $ucitelj_name = null;
        } else {
            $professor_name = null;
            $ucitelj_name = $ucitelj_id;
        }
            
        $data_dete = array(
            'first_name' => $this->input->post('child_f_name'),
            'last_name' => $this->input->post('child_l_name'),
            'razred_id' => $this->input->post('razred'),
            'odeljenje_id' => $this->input->post('odeljenje'),
            'ucitelj_id' => $ucitelj_name,
            'profesor_id' => $professor_name
        );
        $this->db->insert('ucenik', $data_dete);
        $this->last_ucenik_id = $this->db->insert_id();
    }

    public function register_user($hash_password, $username, $role) {
        $data_user = array(
            'username' => $username,
            'password' => $hash_password,
            'role' => $role,
            //'loged_in' => 0,
            //'activated' => 1'activated' => 1 //disabled because values are default in db
        );
        $this->db->insert('user', $data_user);
        $this->last_user_id = $this->db->insert_id();
    }

    public function register_roditelj($jmbg, $first_name, $last_name) {
        $data_roditelj = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'jmbg' => $jmbg,
            'user_id' => $this->last_user_id,
            'ucenik_id' => $this->last_ucenik_id
        );

        $this->db->insert('roditelj', $data_roditelj);
    }



    // checking for matching username
    public function match_username($username) {
        $this->db->where('username', $username);
        $user = $this->db->get('user');
            if($user->num_rows() > 1) {
                return $user->result(); // result() returns array of objects
            } elseif($user->num_rows() == 1) {
                return $user->row(); // row() returns one object
            } else {
                return false;
            }
    }

    // loging in
    public function login($username, $password) {
        // validate
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $result = $this->db->get('user');
        if($result->num_rows() == 1) {
            return $result->row(); // returnig matched user from table
        } else {
            return false; 
        }
    }
}
<!-- style for sent time in chat window -->
<style>
	.msgTimeL {
		display: block;
		font-size: 10px;
		color: #fcba03;
	}
</style>

<!-- import chatkit -->
<script src="https://unpkg.com/@pusher/chatkit-client@1/dist/web/chatkit.js"></script>	

<script>
	// Initialise chatkit
	const tokenProvider = new Chatkit.TokenProvider({
		url: "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/05676e97-a4f9-484f-8da9-272b6b5d92ff/token"
	});

	
	// verification of logged in user (uzer, sto bi rekao Bozo.js)
	const chatManager = new Chatkit.ChatManager({
		instanceLocator: "v1:us1:05676e97-a4f9-484f-8da9-272b6b5d92ff",
		userId: "<?=$this->session->userdata['user_id']?>", // is always loggedin user
		tokenProvider: tokenProvider
	});

	// if teacher or professor is logged in, but not parent, go with this code
	<?php if($this->session->userdata['role'] != 'roditelj' ) {?>

		var roditelj;
		
		// Clear chat window
		// document.getElementById("chatWindow").innerHTML = "";

		// Get selected roditelj name from select options tag
		var sel = document.getElementById('izaberiRoditelja');
		var opt = sel.options[sel.selectedIndex];
		roditelj = opt.text;
		// console.log(roditelj);

	<?php } ?>

	// Connect to chatkit
	chatManager
	.connect()
	.then(currentUser => {

		//Push web notifications to browser
		currentUser.enablePushNotifications()
		.then(() => {
			showNotificationsTabClosed: true
			console.log('Push Notifications enabled');
		})
		.catch(error => {
			console.error("Push Notifications error:", error);
		});
		
		// Declare variable to store Room ID of a matched roditelj name (only room where a parent resides)
		let room_Id;

		room_Id = currentUser.rooms[0].id; // if a parrent is logged in, this would get an id of that room

		if(typeof roditelj !== 'undefined') { // if there is no defined variable roditelj, skip this block of code
			// loop through rooms where loggedin teacher or professor is registered
			currentUser.rooms.forEach(foo); // use js forEach method which excepts function
			function foo(item) {
			// when roditelj found, set this rooms id to variable
				if(item.name == roditelj) {
					// console.log(item.name);
					// console.log(item.createdByUserId);
					// console.log(item.id);
					room_Id = item.id; // this overrides room ID if loggedin user is either teacher or professor | if there is a parent in the system, and not registered in a room, code goes back to room_Id = currentUser.rooms[0].id; | fix this!
					// console.log(typeof(room_id));
					// console.log(item)
					// console.log(item.unreadCount)
				}
			}
		}

		// currentUser.isTypingIn({ roomId: room_Id })
		// .then(() => {
		// 	console.log('isTyping method = ON')
		// })
		// .catch(err => {
		// 	console.log(`Error sending typing indicator: ${err}`)
		// });

		//console.log(currentUser.roomSubscriptions);
		currentUser.rooms.forEach(foo);
		function foo(item) {
			//console.log(item);
			//console.log(item.userStore.users);
		}
		
		currentUser.subscribeToRoom({
			// roomId: currentUser.rooms[0].id,
			
			roomId: room_Id, // now loggedin teacher or professor is subscribed to a room of selected roditelj
			hooks: {

				// onUserStartedTyping: user => {
				// 	console.log(`${user.name} started typing`)
				// },
				// onUserStoppedTyping: user => {
				// 	console.log(`${user.name} stopped typing`)
				// },
				
				// When message arrives do this...
				onMessage: message => {
					
					 //onsole.log(message.id)

					currentUser.setReadCursor({
						roomId: room_Id,
						position: message.id
						})
						.then(() => {
							//console.log(message.id)
						})
						.catch(err => {
							console.log(`Error setting cursor: ${err}`)
					})

					// grab chat window div
					var chat_win = document.getElementById("chatWindow");
					// create paragaph element
					var poruka = document.createElement("p");
					// create span element which will hold message sent time 
					var msgTime = document.createElement("span");
					// instantiate Date class, to beautify the look of sent time | param is taken from chatkit
					var time = new Date(message.createdAt);

					if(message.senderId == "<?=$this->session->userdata['user_id']?>") {
						// add two classes, one for all received messages (chatPoruka), other to separate received from sent messages
						poruka.classList.add("poslatePoruke", "chatPoruka");
					} else {
						poruka.classList.add("primljenePoruke", "chatPoruka");	
					}
					// put message content in previously created paragraph
					poruka.appendChild(
						// document.createTextNode(`${message.senderId}: ${
						document.createTextNode(`${
							message.text// this is actual content of a message
						}`)
					);
					// append the time in new line by adding class to span element, setting it to display block
					poruka.appendChild(msgTime);
					msgTime.classList.add("msgTimeL"); // class added for some styling
					// finally, add the text presentation of the time
					msgTime.appendChild(
						document.createTextNode(time.toLocaleString())
					);	
					// append finished paragraph to chat window div
					chat_win.appendChild(poruka);
					
					// Automatic scroll to bottom of chat window
					var scrollWindow = setTimeout(function() {
						chat_win.scrollBy(0,5000);
					}, 1);
				},

				onNewReadCursor: cursor => {
					const otherUserCursor = currentUser.readCursor({
						roomId: room_Id
						//userId:
					})
					document.getElementById("status").style.visibility = "visible";
					document.getElementById("status").innerHTML = "Korisnik je procitao sve poruke.";
					setTimeout(function(){
						document.getElementById("status").style.visibility = "hidden"
					}, 5000);
					console.log(`other user has read up to ${
						otherUserCursor.position
					} in ${
						otherUserCursor.room.name
					}.`)
				},

				onPresenceChanged: (state, user) => {
					if(state.current == "online") {
						// console.log(`Korisnik ${user.name} je upravo u tabu 'Poruke'`);
						var icon = document.getElementById("chatOnline");
						var status = document.getElementById("statusOnline");
						icon.classList.remove('offline');
						icon.classList.add('online');
						status.innerHTML = "online";
					} else {
						// console.log(`Korisnik ${user.name} je van taba 'Poruke'`);
						var icon = document.getElementById("chatOnline");
						var status = document.getElementById("statusOnline");
						icon.classList.remove('online');
						icon.classList.add('offline');
						status.innerHTML = "offline";
					}
  					
				}
			},
			messageLimit: 15 // This shows previous messages in the same window. Default is 20. Max is 100. :)
		});
		
		// Send message
		var form = document.getElementById("message-form");
		form.addEventListener("submit", e => {
			e.preventDefault();
			var input = document.getElementById("exampleTextarea");
			currentUser.sendSimpleMessage({
				text: input.value,
				// roomId: currentUser.rooms[0].id
				roomId: room_Id
			});
			input.value = "";
		});

		//console.log(currentUser.roomSubscriptions);
		//console.log(currentUser.rooms);
	})
	.catch(error => {
		console.error("error:", error);
	});
</script>
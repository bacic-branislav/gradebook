</div><!-- end of Container -->
    <!-- Footer -->
    
    <footer class="page-footer footer font-small unique-color-dark pt-4">
    <hr>
        <!-- Footer Elements -->
            <ul class="list-unstyled list-inline text-center py-2">
                <li class="list-inline-item">
                    <h5 class="mb-1">Footer content</h5>
                </li>
            </ul>
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
            <a href="#"> PiedPiper.com</a>
        </div>
    </footer>
    <!-- <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script> -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/0f1bbfb78d.js"></script>
        <script src="<?php echo base_url().'js/jquery-3.4.1.js'?>"></script> 
        <script src="<?php echo base_url().'js/main.js'?>"></script>
        <script src="<?php echo base_url().'js/animacije.js'?>"></script>
    </body>

    </html>
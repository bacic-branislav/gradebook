<!Doctype html> 
<html lang="en">
    <head>
        <title>Pied Piper school</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/bootstrap.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/style.css'?>">
        <link href="https://fonts.googleapis.com/css?family=Cinzel:400,700&display=swap" rel="stylesheet">
        <!-- <script src="http://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script> -->
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
    <body>

    <!-- Flash messages -->
    <?php if($this->session->flashdata('user_loggedin')): ?>
        <?php echo '<p class="alert-success">'. $this->session->flashdata('user_loggedin') .'</p>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('login_failed')): ?>
        <?php echo '<p class="alert-danger">'. $this->session->flashdata('login_failed') .'</p>'; ?>
    <?php endif; ?>

    <?php if($this->session->flashdata('user_loggedout')): ?>
        <?php echo '<p class="alert-success">'. $this->session->flashdata('user_loggedout') .'</p>'; ?>
    <?php endif; ?>

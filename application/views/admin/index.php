<?php require_once('header.php') ?>

<!-- Gornji menu - svi korisnici -->
<div class="adminHeader">
	<div class="studentsTab headerBox">
		<p>Učenici</p>
		<div class="tabWrapper">
			<span class='broj_ucenika'></span>
			<i class="fas fa-user-graduate ikonica"></i>
		</div>
	</div><!-- end of students tab -->

	<div class="parentsTab headerBox">
		<p>Roditelji</p>
		<div class="tabWrapper">
			<span class='brojRoditelja'></span>
			<i class="fas fa-user-friends ikonica"></i>
		</div>
	</div><!-- end of parents tab -->

	<div class="teachersTab headerBox">
		<p>Učitelji</p>
		<div class="tabWrapper">
			<span class='brojUcitelja'></span>
			<i class="fas fa-book-reader ikonica"></i>
		</div>
	</div><!-- end of teachers tab -->

	<div class="profsTab headerBox">
		<p>Profesori</p>
		<div class="tabWrapper">
			<span class='brojProfesora'></span>
			<i class="fas fa-book-reader ikonica"></i>
		</div>
	</div><!-- end of profesors tab -->

	<div class="adminTab headerBox">
		<p>Administratori</p>
		<div class="tabWrapper">
			<span class='brojAdmina'></span>
			<i class="fas fa-user-cog ikonica"></i>
		</div>
	</div><!-- end of admin tab -->

	<div class="directorTab headerBox">
		<p>Direktor</p>
		<div class="tabWrapper">
			<span class='brojDirektora'></span>
			<i class="fas fa-user-tie ikonica"></i>
		</div>
	</div><!-- end of director tab -->
</div><!-- end of admin header -->


<!-- Main admin wrapper -->
<div class="adminWrapper">
	<div class="adminMain">
		<div class="adminListaKorisnika">
			<!-- LEFT -->
			<div class="left">

				<!-- Tabela za prikaz svih ucenika -->
				<table class="table table-dark table-hover tabelaUcenici">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Učenik</th>
							<th>Roditelj</th>
							<th>Razred</th>
							<th>Odeljenje</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class="tabelaUceniciResponse">
						<!-- PRIKAZ IZ JS-a -->
					</tbody>
				</table><!-- tabela prizaz svih ucenika -->

				<!-- Tabela za prikaz svih roditelja -->
				<table class="table table-dark table-hover tabelaRoditelji">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Roditelj</th>
							<th>JMBG</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class="tabelaRoditeljiResponse">
						<!-- PRIKAZ IZ JS-a -->
					</tbody>
				</table><!-- tabela prizaz svih roditelja -->

				<!-- Tabela za prikaz svih ucitelja -->
				<table class="table table-dark table-hover tabelaUcitelji">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Učitelj</th>
							<th>Razred</th>
							<th>Odeljenje</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class='tabelaUciteljiResponse'>
						<!-- Prikaz iz JS-a -->
					</tbody>
				</table><!-- tabela prizaz svih ucitelja -->

				<!-- Tabela za prikaz svih profesora -->
				<table class="table table-dark table-hover tabelaProfesori">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Profesor</th>
							<th>Razred</th>
							<th>Odeljenje</th>
							<th>Predmet</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class='tabelaProfesoriResponse'>
						<!-- Prikaz iz JS-a -->
					</tbody>
				</table><!-- tabela prizaz svih profesora -->

				<!-- Tabela za prikaz svih admina -->
				<table class="table table-dark table-hover tabelaAdmini">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Administrator</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class='tabelaAdministratoriResponse'>
						<!-- Prikaz iz JS-a -->
					</tbody>
				</table><!-- tabela prizaz svih admina -->

				<!-- Tabela za prikaz svih direktora -->
				<table class="table table-dark table-hover tabelaDirektori">
					<thead>
						<tr class="headline">
							<th></th>
							<th>Direktor</th>
							<th>Opcije</th>
						</tr>
					</thead>
					<tbody class='tabelaDirektoriResponse'>
						<!-- Prikaz iz JS-a -->
					</tbody>
					<!-- <?php $i=1; foreach($directors as $director) : ?>
					<tr>
						<td><?= $i ?></td>
						<td><?= $director->first_name.' '.$director->last_name ?></td>
						<td class="adminOpcije">
							<i class="fas fa-trash-restore" onclick="obrisiDirektora(<?=$director->user_id?>)" title="Obriši"></i>
							<i class="fas fa-user-edit prikaziDetaljeDirektora" onclick="izmeniDirektora(<?=$director->id?>, '<?=$director->first_name?>', '<?=$director->last_name?>')" title="Izmeni"></i>
						</td>
					</tr>
					<?php $i++; endforeach; ?> -->
				</table><!-- tabela prizaz svih direktora -->
			</div> <!-- end of LEFT -->

			<div class="right">
				<!-- Detalji ucenika -->
				<div class="detaljiUcenika">
					<h3>Detalji učenika</h3>
					<form>
						<input type="hidden" id="ucenik_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="ucenik_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="ucenik_prezime" placeholder="Prezime korisnika">
						<div>
							<button type="button" class="btn btn-outline-success" onclick='sacuvaj_ucenika()'>Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiUcenikaOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji ucenika -->

				

				<!-- Detalji roditelja -->
				<div class="detaljiRoditelja">
					<h3>Detalji roditelja</h3>
					<form>
						<input type="hidden" id="roditelj_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="roditelj_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="roditelj_prezime" placeholder="Prezime korisnika">
						<label>JMBG:</label>
						<input type="text" class="form-control" id="roditelj_jmbg" placeholder="JMBG">
						<div>
							<button type="button" class="btn btn-outline-success" onclick="sacuvaj_roditelja()">Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiRoditeljaOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji roditelja -->

				<!-- Detalji ucitelja -->
				<div class="detaljiUcitelja">
					<h3>Detalji učitelja</h3>
					<form>
						<input type="hidden" id="ucitelj_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="ucitelj_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="ucitelj_prezime" placeholder="Prezime korisnika">
						<div>
							<button type="button" class="btn btn-outline-success" onclick="sacuvaj_ucitelja()">Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiUciteljaOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji ucitelja -->

				<!-- Detalji profesora -->
				<div class="detaljiProfesora">
					<h3>Detalji profesora</h3>
					<form>
						<input type="hidden" id="profesor_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="profesor_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="profesor_prezime" placeholder="Prezime korisnika">
						<label>Predmet:</label>
						<select class="form-control" id="profesor_predmet">
							<?php foreach($allpredmeti as $predmet):?>
							<option value='<?=$predmet->id?>'><?=$predmet->predmet?></option>
							<?php endforeach;?>
						</select>
						<div>
							<button type="button" class="btn btn-outline-success" onclick="sacuvaj_profesora()">Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiProfesoraOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji profesora -->

				<!-- Detalji admina -->
				<div class="detaljiAdmina">
					<h3>Detalji Administratora</h3>
					<form>
						<input type="hidden" id="admin_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="admin_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="admin_prezime" placeholder="Prezime korisnika">
						<div>
							<button type="button" class="btn btn-outline-success" onclick="sacuvaj_admina()">Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiAdminaOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji admina -->

				<!-- Detalji adirektora -->
				<div class="detaljiDirektora">
					<h3>Detalji Direktora</h3>
					<form>
						<input type="hidden" id="direktor_id">
						<label>Ime:</label>
						<input type="text" class="form-control" id="direktor_ime" placeholder="Ime korisnika">
						<label>Prezime:</label>
						<input type="text" class="form-control" id="direktor_prezime" placeholder="Prezime korisnika">
						<div>
							<button type="button" class="btn btn-outline-success" onclick="sacuvaj_direktora()">Sačuvaj</button>
							<button type="button" class="btn btn-outline-danger detaljiDirektoraOtkazi">Otkaži</button>
						</div>
					</form><br>
					<div class="editodg"></div>
				</div><!-- end of detalji admina -->
			</div> <!-- end of RIGHT -->
		</div><!-- end of adminListaKorisnika -->
	</div><!-- end of admin main -->



	<!-- Admin sidebar -->
	<div class="adminSidebar">
		<!-- Curently online users -->
		<table class="table table-striped table-hover">
			<tr>
				<th colspan="2" class="headline">Trenutno online</th>
			</tr>
			<tbody>
				<?php if(sizeof($onlineUsers) == 0) { ?>
				<tr>
					<td>Trenutno nema korisnika online</td>
					<td><i class="fas fa-power-off" style="color:red"></i></td>
				</tr>
				<?php } else {  
                        foreach($onlineUsers as $onlineUser): ?>
				<tr>
					<td> <?=$onlineUser->username ?> </td>
					<td> <?=$onlineUser->role ?> </td>
					<!-- <td id="comment"><i class="fas fa-comment"></i></td> -->
				</tr>
				<?php endforeach; }?>
			</tbody>
		</table>
	</div><!-- end of admin sidebar -->
</div><!-- end of admin wrapper -->

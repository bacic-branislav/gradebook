<?php require_once('header.php') ?>

<div class="container">

	<div class="unesiNovoKorisnika">

		<h2>Unesi novog korisnika</h2>

		<label>Status</label>
		<select class="form-control status" id="role" onchange="korisnikRola(this.value)">
			<option value="0">Odaberi korisnika</option>
			<option value="roditelj">Roditelj - učenik</option>
			<option value="ucitelj">Učitelj</option>
			<option value="profesor">Profesor</option>
			<option value="admin">Admin</option>
			<option value="direktor">Direktor</option>
		</select>

		<!-- unesi Roditelja -->
		<div class="unesiRoditelja">

			<div class="left">
				<label>Ime roditelja</label>
				<input type="text" class="form-control" id="f_name">
				<label>Prezime roditelja</label>
				<input type="text" class="form-control" id="l_name">
				<label>JMBG roditelja</label>
				<input type="text" class="form-control" id="jmbg">
			</div><!-- end of left -->

			<div class="right">
				<label>Ime učenika</label>
				<input type="text" class="form-control" id="child_f_name">
				<label>Prezime učenika</label>
				<input type="text" class="form-control" id="child_l_name">
				<div class="razred-odeljenje">
					<div>
						<label>Razred</label>
						<select class="form-control izabranRazred" id="razred">
							<option value="" selected></option>
							<?php for($i=1; $i<=8; $i++):?>
							<option value="<?=$i?>"><?=$i?></option>
							<?php endfor;?>
						</select>
					</div>
					<div style="margin-right:0">
						<label>Odeljenje</label>
						<select class="form-control" id="odeljenje">
							<option value="" selected></option>
							<?php foreach($odeljenja as $response):?>
							<option value="<?=$response->id ?>"><?=$response->odeljenje ?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
			</div><!-- end of right -->
			
			<button type="button" class="btn btn-success" id="unesiRoditelja">Unesi u bazu</button>
			<div class="odgovor"></div>
		</div><!-- end of unesi roditelja -->

		<!-- forma unesi Ucitelja -->
		<div class="unesiUcitelja">

			<label>Ime učitelja</label>
			<input type="text" class="form-control" id="ucitelj_ime">
			<label>Prezime učitelja</label>
			<input type="text" class="form-control" id="ucitelj_prezime">
			<label>JMBG</label>
			<input type="text" class="form-control" id="ucitelj_jmbg"> <br>

			<button type="button" class="btn btn-success" id="unesiUcitelja">Unesi u bazu</button>
			<div class="odgovorUcitelj"></div>
		</div><!-- end of unesi ucitelja -->



		<!-- forma unesi Profesor -->
		<div class="unesiProfesora">

			<label>Ime profesora</label>
			<input type="text" class="form-control" id="profesor_ime">
			<label>Prezime profesora</label>
			<input type="text" class="form-control" id="profesor_prezime">
			<label>JMBG</label>
			<input type="text" class="form-control" id="profesor_jmbg">
			<label>Predmet</label>
			<select class="form-control" id="profesor_predmet">
				<option value="" selected></option>
				<?php foreach($predmeti as $predmet): ?>
				<option value="<?=$predmet->id ?>"><?=$predmet->predmet ?></option>
				<?php endforeach;?>
			</select>

			<button type="button" class="btn btn-success" id="unesiProfesora">Unesi u bazu</button>
			<div class="odgovorProfesor"></div>
		</div><!-- end of unesi Profesora -->


		<!-- forma unesi Admin -->
		<div class="unesiAdmina">

			<label>Ime administratora</label>
			<input type="text" class="form-control" id="admin_ime">
			<label>Prezime administratora</label>
			<input type="text" class="form-control" id="admin_prezime">
			<label>JMBG</label>
			<input type="text" class="form-control" id="admin_jmbg">

			<button type="button" class="btn btn-success" id="unesiAdmina">Unesi u bazu</button>
			<div class="odgovorAdmin"></div>
		</div><!-- end of unesi Admina -->


		<!-- forma unesi Direktora -->
		<div class="unesiDirektora">

			<label>Ime direktora</label>
			<input type="text" class="form-control" id="direktor_ime">
			<label>Prezime direktora</label>
			<input type="text" class="form-control" id="direktor_prezime">
			<label>JMBG</label>
			<input type="text" class="form-control" id="direktor_jmbg">

			<button type="button" class="btn btn-success" id="unesiDirektora">Unesi u bazu</button>
			<div class="odgovorDirektor"></div>
		</div><!-- end of unesi Direktora -->


	</div><!-- end of unesiNovogKorisnika -->

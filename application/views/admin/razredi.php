<?php include_once('header.php')?>

<!-- MENU -->
<div class="izaberiPrikaz">
    <?php echo form_open('admin/razredi'); 
        if(isset($_POST['izabranRazred']) && isset($_POST['izabranoOdeljenje'])) {
            $razred = $_POST['izabranRazred'];
            $odeljenje = $_POST['izabranoOdeljenje'];
        }else {
            $razred = 'sve';
            $odeljenje = 'sve'; }?>

        <!-- Biranje razreda -->
        <div class="menu">
            <div> <!-- Razred -->
                <label>Razred</label>
                <select class="form-control" name="izabranRazred">
                    <option value="sve" <?= (($razred == 'sve')? "selected" : "" )?>> Sve </option>
                    <option value="1" <?= (($razred == 1)? "selected" : "" )?>> 1 </option>
                    <option value="2" <?= (($razred == 2)? "selected" : "" )?>> 2 </option>
                    <option value="3" <?= (($razred == 3)? "selected" : "" )?>> 3 </option>
                    <option value="4" <?= (($razred == 4)? "selected" : "" )?>> 4 </option>
                    <option value="5" <?= (($razred == 5)? "selected" : "" )?>> 5 </option>
                    <option value="6" <?= (($razred == 6)? "selected" : "" )?>> 6 </option>
                    <option value="7" <?= (($razred == 7)? "selected" : "" )?>> 7 </option>
                    <option value="8" <?= (($razred == 8)? "selected" : "" )?>> 8 </option>
                </select>
            </div> 
            <div> <!-- Odeljenje -->
                <label>Odeljenje</label>
                <select class="form-control" name="izabranoOdeljenje">
                    <option value="sve" <?= (($odeljenje == 'sve')? "selected" : "" )?>> Sve </option>
                    <?php foreach($odeljenja as $response): ?>
                    <option value="<?=$response->id?>" <?=(($odeljenje == $response->odeljenje)? "selected" : "" )?>> <?=$response->odeljenje?> </option>
                    <?php endforeach;?>
                </select>
            </div>
            <button type="submit" class="btn btn-outline-info">Prikaži</button>
            </form>
        </div><!-- end of menu -->
        
        <div class="dodaj">
            <!-- Dodaj ucitelja -->
            <div class="dodajStaresinu dodajUcitelja">
                <label>Izaberi učitelja:</label><br>
                <select class="form-control" id="izaberiUcitelja" <?=(sizeof($nerasporedjeniUcitelji)==0)?'disabled':'' ?>>
                    <?php foreach($nerasporedjeniUcitelji as $ucitelj): ?>
                    <option value="<?=$ucitelj->id?>"> <?=$ucitelj->first_name.' '.$ucitelj->last_name?> </option>
                    <?php endforeach;?>
                </select>
                <button type="button" class="btn btn-outline-success addUcitelj" <?=(sizeof($nerasporedjeniUcitelji)==0)?'disabled':'' ?>>Dodaj učitelja</button>
            </div>

            <!-- Dodaj profesora -->
            <div class="dodajStaresinu dodajProfesora">
                <label>Izaberi profesora:</label><br>
                <select class="form-control" id="izaberiProfesora" <?=(sizeof($nerasporedjeniProfesori)==0)?'disabled':'' ?>>
                    <?php foreach($nerasporedjeniProfesori as $profesor): ?>
                    <option value="<?=$profesor->id?>"> <?=$profesor->first_name.' '.$profesor->last_name?> </option>
                    <?php endforeach;?>
                </select>
                <button type="button" class="btn btn-outline-success addProfesor" <?=(sizeof($nerasporedjeniProfesori)==0)?'disabled':'' ?>>Dodaj profesora</button>
            </div>

            <!-- Dodaj ucenika -->
            <div class="dodajStaresinu dodajUcenika">
                <label>Izaberi učenika:</label><br>
                <select class="form-control" id="izaberiUcenika" <?=(sizeof($nerasporedjeniUcenici)==0)?'disabled':'' ?>>
                    <?php foreach($nerasporedjeniUcenici as $ucenik): ?>
                    <option value="<?=$ucenik->id?>"> <?=$ucenik->first_name.' '.$ucenik->last_name?> </option>
                    <?php endforeach;?>
                </select>
                <button type="button" class="btn btn-outline-success addUcenik" <?=(sizeof($nerasporedjeniUcenici)==0)?'disabled':'' ?>>Dodaj učenika</button>
            </div>
        </div><!-- end of dodaj -->


        <div class="text"></div>
    
</div><!-- end of menu -->



<!-- RAZREDI -->
<div class="predmetiWrapper">
	<div class="predmeti razredi">

        
        <?php if($razred=='sve' && $odeljenje=='sve') {?>
            <!-- SVI RAZREDI -->
            <?= $razredi?>

        <?php }else if($odeljenje=='sve') {?>
            <!-- SVA ODELJENJA JEDNOG RAZREDA --> 
            <?= $razredi?>

        <?php }else {?>
            <!-- SAMO JEDNO ODELJENJE -->  
            <div class="razred odeljenja odeljenjaDetaljno">
                <table class='table table-dark table-hover'>
                    <thead>
                    <tr class='head'>
                        <th colspan="4">Razred <?=$_POST['izabranRazred']?> / Odeljenje <?=$_POST['izabranoOdeljenje']?></th>
                    </tr>
                    </thead>
                    <?php if(sizeof($staresine) == 0) {?>
                        <tr class='staresina'>
                            <td>Starešina:</td>
                            <td colspan='2'><span class='text-danger'>Nije dodeljen</span></td>
                            <td><i class='fas fa-user-plus iconDodaj' onclick='dodajStaresinu(<?=$razred ?>, <?=$odeljenje ?>)' title='Dodaj starešinu'></i></td>
                        </tr>
                    <?php }else { foreach($staresine as $response):?>
                        <tr class='staresina'>
                            <td><?=($response->razred <=4)? 'Učitelj :':'Profesor :' ?></td>
                            <td colspan='2'><?=$response->first_name.' '.$response->last_name?></td>
                            <td><i class='fas fa-user-minus iconObrisi' onclick='ukloniStaresinu(<?=$response->id?>, <?=$response->razred?>, <?=$response->odeljenje?>)' title='Ukloni starešinu'></i></td>
                        </tr>
                    <?php endforeach; }?>
                        <thead>
                        <tr class='headline'>
                            <th></th>
                            <th>Učenik :</th>
                            <th>Roditelj :</th>
                            <th></th>
                        </tr>
                        </thead>
                    <tbody>
                        <tr class='head'>
                            <td>Dodaj učenika</td>
                            <td></td>
                            <td></td> 
                            <?php if(sizeof($staresine) == 0){ ?>
                                <td><i class="fas fa-user-lock" title="Nema starešine"></i></td>
                            <?php } else {?>
                            <td><i class='fas fa-user-plus iconDodaj' onclick='dodajUcenika(<?=$staresine[0]->razred?>,<?=$staresine[0]->odeljenje?>)' title='Dodaj učenika'></i></td>
                            <?php }?>


                            <!-- <td><i class='fas fa-user-plus iconDodaj' onclick='dodajUcenika(<?=$staresine[0]->razred?>,<?=$staresine[0]->odeljenje?>)' title='Dodaj učenika'></i></td> -->
                    <?php if(sizeof($razredi)==0) {?>
                        <tr>
                            <td>0</td>
                            <td colspan='3'><span class='text-danger'>Nema učenika u ovom odeljenju</span></td>
                        </tr>
                    <?php } $i=1; ?>
                    <?php foreach ($razredi as $response): ?>
                        <tr>
                            <td><?=$i?></td>
                            <td> <?=$response->ucenik_ime.' '.$response->ucenik_prezime?> </td>
                            <td> <?=$response->roditelj_ime.' '.$response->roditelj_prezime?> </td>
                            <td><i class='fas fa-user-minus iconObrisi' onclick='ukloniUcenika(<?=$response->ucenik_id ?>)' title='Ukoni učenika'></i></td>
                        </tr>
                    <?php $i++; endforeach; ?>
                    </tbody>
                </table>
            </div> <!-- end of razred odeljenje -->
        <?php }?>
	</div> <!-- end of razredi -->


	<div class="sidebar">
        <!-- Nerasporedjeno osoblje -->
        <div class="unesiPredmet nerasporedjenoOsoblje">
            <h5>Neraspoređeno osoblje</h5>

            <!-- Ucitelji -->
            <table class='table table-sm table-hover table-borderless'>
                <thead ><tr class='headline'> 
                    <th colspan='2'>Učitelji</th>
                </tr></thead>
                <?php if(sizeof($nerasporedjeniUcitelji)==0) {?>
                    <tr>
                        <td colspan='2' ><span class='text-success'>Nema neraspoređenih</span></td>
                    </tr>
                <?php } $i=1; foreach($nerasporedjeniUcitelji as $neUc): ?>
                <tr>
                    <td><?=$i?></td>
                    <td><span class='text-danger'><?=$neUc->first_name.' '.$neUc->last_name ?></span></td>
                </tr>
                <?php $i++; endforeach;?>
            </table>

             <!-- Profesori -->
             <table class='table table-sm table-hover table-borderless'>
                <thead><tr class='headline'> 
                    <th colspan='2'>Profesori</th>
                </tr></thead>
                <?php if(sizeof($nerasporedjeniProfesori)==0) {?>
                    <tr>
                        <td colspan='2'><span class='text-success'>Nema neraspoređenih</span></td>
                    </tr>
                <?php } $i=1; foreach($nerasporedjeniProfesori as $nePr): ?>
                <tr>
                    <td><?=$i?></td>
                    <td><span class='text-danger'><?=$nePr->first_name.' '.$nePr->last_name ?></span></td>
                </tr>
                <?php $i++; endforeach;?>
            </table>

            <!-- Ucenici -->
             <table class='table table-sm table-hover table-borderless'>
                <thead><tr class='headline'> 
                    <th colspan='2'>Učenici</th>
                </tr></thead>
                <?php if(sizeof($nerasporedjeniUcenici)==0) {?>
                    <tr>
                        <td colspan='2'><span class='text-success'>Nema neraspoređenih</span></td>
                    </tr>
                <?php } $i=1; foreach($nerasporedjeniUcenici as $neUc): ?>
                <tr>
                    <td><?=$i?></td>
                    <td><span class='text-danger'><?=$neUc->first_name.' '.$neUc->last_name ?></span></td>
                </tr>
                <?php $i++; endforeach;?>
            </table>
            
        </div>
		
		
		 
	</div> <!-- end of sidebar -->
</div> <!-- end of predmetiWrapper -->
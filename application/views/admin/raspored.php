<?php require_once('header.php');
if(!(isset($_POST['unesi_promene']))){
	$razred     = isset($_POST['razred']) ? $_POST['razred']:1;
	$odeljenje  = isset($_POST['odeljenje']) ? $_POST['odeljenje']:1;
}
if(isset($_POST['OdaberiRazred']) or isset($_POST['unesi_promene'])){
    $razred     = $_POST['razred'];
    $odeljenje  = $_POST['odeljenje'];
}
?>
<script>
function refreshPage(){
    window.location.reload();
} 
</script>
<!-- Raspored casova -->

<div class="studentClasses">
<form action="" method="post">
	<div class="parentsHeader profHeader">
		<!-- razred -->
		<div class="form-group" id="razred">
			<label for="exampleSelect1">Razred</label>
			<select class="form-control" id="exampleSelect1" name="razred">
				<option value="<?=$razred?>" selected><?=$razred?></option>
					<?php
					for($i=1;$i<=8;$i++){
						echo "<option value='$i'>$i</option>";
					}
					?>
			</select>
		</div><!-- razred -->
		<div class="form-group">
			<label class="control-label">Odeljenje</label>
			
			<select name="odeljenje" class="odeljenje form-control">
                <option value="<?=$odeljenje?>" selected><?=$odeljenje?></option>
            <?php
            for($i=1;$i<=3;$i++){
                echo "<option value='$i'>$i</option>";
            }
            ?>
        </select>
		</div><!-- odeljenje -->
		<div class="form-group razredni">
			<label class="control-label">Učitelj / Razredni</label>
			<input class="form-control" type="text" value="Nikola Mitic" readonly>
		</div><!-- razredni --> <br>
		<button type="submit" class="btn btn-outline-info prikazi" name="OdaberiRazred">Prikaži</button>
	</div><!-- end of Admin header -->
</form>
	<table class='table table-hover table-dark table-bordered'>
		<tr style='text-align:center;'>
			<td>Čas</td>
			<th>Ponedeljak</th>
			<th>Utorak</th>
			<th>Sreda</th>
			<th>Cetvrtak</th>
			<th>Petak</th>
		</tr>
		<!-- <tr>
			<td>Čas</td>
			<td colspan='5'>Predmet</td>
		</tr> -->
<form action="" method="post">		
<?php
echo $raspored;
?>
</table><br>
<input type="hidden" name="razred"      value="<?=$razred?>">
<input type="hidden" name="odeljenje"   value="<?=$odeljenje?>">
<button type="submit" class="btn btn-success" name="unesi_promene" style="margin:0 auto; width:150px;" onClick="refreshPage();"> Snimi</button>
</form>
	</table>
</div><!-- end of Ocene ucenika -->


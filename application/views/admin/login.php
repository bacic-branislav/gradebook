<div class="background">
    <div class="loginForm">

        <?php echo validation_errors(); ?>

        <?php echo form_open('admin/login', 'class="form-signin"'); ?>
            <img src="<?php echo base_url().'img/logo.png'?>">
            <label>Korisničko ime</label>
            <input type="text" class="form-control name" name="username" value="<?php echo set_value('username'); ?>" placeholder="Unesite korisničko ime">
            <label>Lozinka</label>
            <input type="password" class="form-control pass" name="password" value= "<?php echo set_value('password'); ?>" placeholder="Unesite lozinku"><br>
            <button type="submit" class="btn btn-success">Prijavi se</button>
        <?php echo form_close(); ?>

    </div>
</div>

<?php require_once('header.php') ?>

<div class="predmetiWrapper">
	<div class="predmeti">
		<?=$razredi?>   
	</div> <!-- end of predmeti -->
	<div class="predmetiOdg"></div>

	<div class="sidebar">

	<div class="unesiPredmet">
		<form action="predmeti" method="POST">
			<label>Unesi novi predmet</label>
			<input type="text" name="nazivPredmeta" class="form-control" placeholder="Naziv predmeta"> <br>

			<label>Predaje se u razredima: <br> (Izaberi više)</label>
			<select multiple="multiple" name="uRazredima[]" class="form-control">
				<?php for($i=1; $i<=8; $i++):?>
				<option value="<?=$i?>"><?=$i?></option>
				<?php endfor;?>
			</select>
			<button type="submit" class="btn btn-success">Unesi u bazu</button>
		</form>
		<?php if(isset($porukaPredmet)) echo $porukaPredmet?>
	</div> <!-- end of unesiPredmet -->
		
		
		 
	</div> <!-- end of sidebar -->
</div> <!-- end of predmetiWrapper -->



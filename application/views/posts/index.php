<?php include_once('header.php')?>
<div class="container-fluid" style="width:100%">

	<?php // User status check 
    $role = $this->session->userdata['role'];
    if($role == 'ucitelj' || $role == 'profesor'): ?>
	<div class="btn-group" role="group" aria-label="Basic example">
		<a href="<?=base_url().'posts'?>" class="btn btn-secondary active">Sva obaveštenja</a>
		<a href="<?=base_url().'posts/create'?>" class="btn btn-secondary">Napravi novo</a>
	</div><br><br>
	<?php endif;?>

	<?php if(sizeof($posts) == 0) {
        echo "Trenutno nema postavljenih obaveštenja";
        return;
    }?>
	<?php foreach($posts as $post): ?>
	<div class="onePost">
		<div class="header">
			<h3> <?=$post['title'] ?></h3>
			<!-- Post created by -->
			<small class="post-date"><?= $post['username']." - ".$post['role'] ?> /
				<i><?= $post['created_at'] ?></i></small>
		</div>
		<p class="sadrzaj">
			<?= word_limiter($post['body'], 35) // to limit (truncate) the text we use CI's text helper and word_limiter() function  ?>
		</p>
		<div class="bottom">
			<p><a class="btn btn-primary btn-sm" href="<?= site_url('posts/'.$post['slug']); ?>">Pročitaj celu vest</a>
			</p>
		</div>
	</div>
	<?php endforeach; ?>
	<div class="pagination-links pagination">
		<?php echo $this->pagination->create_links(); ?>
	</div>

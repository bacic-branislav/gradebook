<?php include_once('header.php')?>

<?php $role = $this->session->userdata['role']; // User status check for nav-menu ?>

<div class="container-fluid" style="width:100%">
	<div class="onePostView">
		<h2 class="title-post"><?= $post['title'] ?></h2>
		<div class="post-body">
			<?= $post['body'] ?> <br><br>
			<small class="post-date"><?= $post['username']." - ".$post['role'] ?> / <?= $post['created_at'] ?></small>
		</div>

        <!-- User rights check -->
        <?php echo form_open('/posts/delete/' . $post['id']);
        // Only post owner can edit own post
        $logedUser = $this->session->userdata['user_id'];
        $postOwner = $post['user_id']; 
        if($logedUser == $postOwner){ ?>
            <!-- go back -->
            <a href="<?=base_url().'posts'?>" class="btn btn-primary">Nazad</a>
            <!-- edit -->
            <a class="btn btn-info mr-2" href="<?=base_url()?>posts/edit/<?=$post['slug']; ?>">Uredi</a>
            <!-- delete -->
            <input type="submit" value="Obriši" class="btn btn-danger">

        <?php } else if($role == 'admin'){ ?>
            <!-- go back -->
            <a href="<?=base_url().'posts'?>" class="btn btn-primary">Nazad</a>
            <!-- only delete -->
            <input type="submit" value="Obriši" class="btn btn-danger">

        <?php } else{ ?>
            <!-- No rights -->
            <a href="<?=base_url().'posts'?>" class="btn btn-primary">Nazad</a>
        <?php } ?>
		</form>
	</div> <!-- end of onePostView -->

</div>

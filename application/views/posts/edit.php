<?php include_once('header.php')?>
<div class="container-fluid" style="width:100%">

<div class="btn-group" role="group" aria-label="Basic example">
	<a href="<?=base_url().'posts'?>" class="btn btn-secondary active">Nazad</a>
</div><br><br>
<!-- before actual form we need to call form validation method -->


<!-- we will use CI's form hellper so instead of <form> as an opening tag we will use CI's method -->
<?= form_open('posts/update') ?>
<div class="form-group">
	<input type="hidden" name="id" value="<?php echo $post['id'] ?>">
	<label>Naslov</label>
	<!-- Added set_value('name of the input') to get the form to repopulate this input -->
	<input type="text" class="form-control" name="title" value="<?php echo $post['title']; ?>"
		placeholder="Unesi naslov obavestenja">
</div>
<div class="form-group">
	<label>Tekst obavestenja</label>
	<!-- <textarea class="form-control" name="body" placeholder="Unesi tekst obavestenja"></textarea> -->
	<!-- going with method instead of textarea tag because for the tag value can not be set, so it is impossibile to repopulate -->
	<?=form_textarea(array('name' => 'body', 'value' => $post['body'], 'rows' => '5', 'class' => 'form-control', 'placeholder' => 'Unesi tekst obavestenja'))?>
</div>
<button type="submit" class="btn btn-default">Submit</button>
<?= validation_errors() ?>
</form>

<?php include_once('header.php')?>

<?php
$role = $this->session->userdata['role'];
if($role == 'roditelj' || $role == 'admin') {
    echo "Nemate pravo da pisete nove postove<br>";
    echo "<a href='".base_url().'posts'."'>Nazad</a>";
    return;
}
?>


<!-- Navigation -->
<div class="btn-group" role="group" aria-label="Basic example">
	<a href="<?=base_url().'posts'?>" class="btn btn-secondary">Sva obavestenja</a>
	<a href="<?=base_url().'posts/create'?>" class="btn btn-secondary active">Napravi novo</a>
</div> <br><br>

<div class="container-fluid" style="width:100%">
	<h2><?= $title ?></h2>
	<div class="onePost" style="padding:10px 50px; text-align:left">
		<?= form_open('posts/create') ?>
		<div class="form-group">
			<label>Naslov</label>
			<input type="text" class="form-control" name="title" value="<?php echo set_value('title'); ?>" placeholder="Unesi naslov obaveštenja">
		</div>
		<div class="form-group">
			<label>Tekst obavestenja</label>
			<?=form_textarea(array('id' => 'editor1', 'name' => 'body', 'value' => set_value('body'), 'rows' => '5', 'class' => 'form-control', 'placeholder' => 'Unesi tekst obaveštenja'))?>
		</div>
		<button type="submit" class="btn btn-success">Snimi</button>
        </form>
        <?= validation_errors() ?>
	</div>

</div>


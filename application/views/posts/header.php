<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<!-- logo -->
	<img src="<?php echo base_url().'img/logoWhite.png'?>" height="75" class="d-inline-block align-top">
    <div class="banner">
		<p><i class="fas fa-envelope"></i>piedpiper@gmail.com</p>
		<p class="phnumber"><i class="fas fa-phone-alt"></i>011/278-916</p>
		<p><i class="fas fa-map-marker-alt"></i>Sillicon Valley</p>
	</div>
	<div class="collapse navbar-collapse" id="navbarColor02">
		<ul class="navbar-nav mr-auto">
		</ul>
		<!-- Prijavljen korisnik -->
		<div class="form-inline my-2 my-lg-0">
			<ul class="nav nav-pills">
				<li class="nav-item">
					<a href="" class="nav-link disabled"> <?= (isset($this->session->userdata['role']))?  ucwords($this->session->userdata['role']) : 'nema role' ?> </a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
						aria-haspopup="true" aria-expanded="false">
						<?= (isset($this->session->userdata['user_id']))? $this->session->userdata['username'] : 'nema sesije' ?></a>
					<div class="dropdown-menu" x-placement="bottom-start"
						style="position: absolute; width:50px; text-align:center; will-change: transform; top: 0px; left: -17px; transform: translate3d(0px, 50px, 0px);">
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?= base_url().'admin/logout'?>">Odjavi se</a>
					</div>
				</li>
			</ul>
		</div><!-- end of Prijavljen korisnik -->
	</div>
</nav><!-- end of Navigation -->

<?php $role = $this->session->userdata['role']; ?>

<!-- Main content wrapper -->
<div class="container-fluid"> 

<!-- Menu tab-bar -->
<div class="navigacija">
	<ul class="nav nav-tabs">
		<?php // Drugaciji menu za administratora
		if($role == 'admin'){ ?>
			<li class="nav-item"> <!-- Svi korisnici -->
				<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>"><i class="fas fa-users tab_icon"></i> Svi korisnici</a>
			</li> 
			<li class="nav-item"> <!-- Raspored casova -->
				<a href="<?= base_url().$this->session->userdata['role'].'/raspored'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/raspored')? 'active' : ''?>"><i class="fas fa-calendar-alt tab_icon"></i> Raspored</a>
			</li>
			<li class="nav-item"> <!-- Unos novog korisnika -->
				<a href="<?= base_url().$this->session->userdata['role'].'/register'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/register')? 'active' : ''?>"><i class="fas fa-user-plus tab_icon"></i> Unesi novog korisnika</a>
			</li>
			<li class="nav-item"> <!-- Razredi -->
				<a href="<?= base_url().$this->session->userdata['role'].'/razredi'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/razredi')? 'active' : ''?>"><i class="fas fa-graduation-cap tab_icon"></i> Razredi-Odeljenja</a>
			</li>
			<li class="nav-item"> <!-- Predmeti -->
				<a href="<?= base_url().$this->session->userdata['role'].'/predmeti'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/predmeti')? 'active' : ''?>"><i class="fas fa-book tab_icon"></i>Predmeti</a>
			</li>
			<li class="nav-item"> <!-- Poruke -->
				<a href="<?= base_url().$this->session->userdata['role'].'/excel'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/excel')? 'active' : ''?>"><i class="fas fa-file-excel tab_icon"></i>Excel fajlovi</a>
			</li>
			<li class="nav-item"> <!-- Obavestenja -->
				<a href="<?= base_url().'posts'?>" class="nav-link <?=(current_url() == base_url().'posts')? 'active' : '' ?>"><i class="fas fa-comment-alt tab_icon"></i> Obaveštenja</a>
			</li>
		<!-- Menu za Direktora -->
		<?php } elseif($role == 'direktor') { ?>
			<li class="nav-item"> <!-- Statistike po odeljenju -->
			<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>">Statistike odeljenja</a>
			</li>
			<li class="nav-item"> <!-- Statistike na nivou skole -->
				<a href="<?= base_url().$this->session->userdata['role'].'/statistikeSkole'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/statistikeSkole')? 'active' : ''?>">Statistike škole</a>
			</li>
		<!-- Menu za Ucitelja / Profesora -->
		<?php } elseif($role == 'ucitelj' || $role == 'profesor') {?>	
			<li class="nav-item"> <!--Sve ocene ucenika -->
				<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>"> <i class="fas fa-poll-h tab_icon"></i>Ocene</a>
			</li>
			<li class="nav-item"> <!-- Raspored casova -->
				<a href="<?= base_url().$this->session->userdata['role'].'/raspored'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/raspored')? 'active' : ''?>"> <i class="fas fa-calendar-alt tab_icon"></i>Raspored</a>
			</li>
			<li class="nav-item"> <!--Poruke -->
				<a href="<?= base_url().$this->session->userdata['role'].'/poruke'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/poruke')? 'active' : ''?>"> <i class="fas fa-comment tab_icon"></i>Poruke</a>
			</li>
			<li class="nav-item"> <!--Otvorena Vrata -->
				<a href="<?= base_url().$this->session->userdata['role'].'/otvorenaVrata'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/otvorenaVrata')? 'active' : ''?>"> <i class="fas fa-address-card tab_icon"></i>Otvorena vrata</a>
			</li>
			<li class="nav-item"> <!--Obavestenja -->
				<a href="<?= base_url().'posts'?>" class="nav-link <?=(current_url() == base_url().'posts')? 'active' : '' ?>"> <i class="fas fa-comment-alt tab_icon"></i>Obaveštenja</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url().$this->session->userdata['role'].'/opravdanja'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/opravdanja')? 'active' : ''?>">Opravdanja</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url().$this->session->userdata['role'].'/izostanci'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/izostanci')? 'active' : ''?>">Izostanci i Upis</a>
			</li>
		<?php } else { ?>
			<li class="nav-item"> <!--Sve ocene ucenika -->
				<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>"> <i class="fas fa-poll-h tab_icon"></i>Ocene</a>
			</li>
			<li class="nav-item"> <!-- Raspored casova -->
				<a href="<?= base_url().$this->session->userdata['role'].'/raspored'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/raspored')? 'active' : ''?>"> <i class="fas fa-calendar-alt tab_icon"></i>Raspored</a>
			</li>
			<li class="nav-item"> <!--Poruke -->
				<a href="<?= base_url().$this->session->userdata['role'].'/poruke'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/poruke')? 'active' : ''?>"> <i class="fas fa-comment tab_icon"></i>Poruke</a>
			</li>
			<li class="nav-item"> <!--Otvorena Vrata -->
				<a href="<?= base_url().$this->session->userdata['role'].'/otvorenaVrata'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/otvorenaVrata')? 'active' : ''?>"> <i class="fas fa-address-card tab_icon"></i>Otvorena vrata</a>
			</li>
			<li class="nav-item"> <!--Obavestenja -->
				<a href="<?= base_url().'posts'?>" class="nav-link <?=(current_url() == base_url().'posts')? 'active' : '' ?>"> <i class="fas fa-comment-alt tab_icon"></i>Obaveštenja</a>
			</li>
			<li class="nav-item">
				<a href="<?= base_url().$this->session->userdata['role'].'/opravdanja'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/opravdanja')? 'active' : ''?>">Opravdanja</a>
			</li>
		<?php } ?>
	</ul>
</div><!-- end of Menu tab-bar -->
<div class="container">
    <h2><?= $title ?></h2>

    <!-- before actual form we need to call form validation method -->
    <?= validation_errors() ?>

    <!-- we will use CI's form hellper so instead of <form> as an opening tag we will use CI's method -->
    <?= form_open('users/register') ?>
    
        <div class="form-group">
        <label>Rola</label>
            <?php
                $options = array(
                    'korisnik' => 'Odaberi korisnika',
                    'ucitelj' => 'Ucitelj',
                    'profesor' => 'Profesor',
                    'roditelj' => 'Roditelj',
                    'admin' => 'Admin',
                    'direktor' => 'Direktor'
                );
                echo form_dropdown('role', $options, 'korisnik', 'class="form-control"'); // params order: name, array of options, selected
            ?>
        </div>
        <hr>
        <div class="form-group">
            <label>Ime ucenika</label>
            <!-- Added set_value('name of the input') to get the form to repopulate this input -->
            <input type="text" class="form-control" name="child_f_name" value="<?php echo set_value('child_f_name'); ?>" placeholder="Ime deteta">
        </div>
        <div class="form-group">
            <label>Prezime</label>
            <!-- Added set_value('name of the input') to get the form to repopulate this input -->
            <input type="text" class="form-control" name="child_l_name" value="<?php echo set_value('child_l_name'); ?>" placeholder="Prezime deteta">
        </div>
        <div class="form-group">
        <label>Razred ucenika</label>
            <?php
                $options = array(
                    '1' => 'Prvi razred',
                    '2' => 'Drugi razred',
                    '3' => 'Treci razred',
                    '4' => 'Cetvrti razred',
                    '5' => 'Peti razred',
                    '6' => 'Sesti razred',
                    '7' => 'Sedmi razred',
                    '8' => 'Osmi razred',
                );
                echo form_dropdown('razred', $options, '1', 'class="form-control"'); // params order: name, array of options, selected
            ?>
        </div>
        <div class="form-group">
        <label>Odeljenje</label>
            <?php
                $options = array(
                    '1' => 'A',
                    '2' => 'B',
                    '3' => 'C'
                );
                echo form_dropdown('odeljenje', $options, '1', 'class="form-control"'); // params order: name, array of options, selected
            ?>
        </div>
        <div class="form-group">
        <label>Ucitelj</label>
        <select class="form-control" name="ucitelj">
            <option value=0 selected>Izaberi</option>
            <?php foreach($ucitelji as $ucitelj): ?>
            <option value=" <?=$ucitelj->id ?> "> <?=$ucitelj->first_name." ".$ucitelj->last_name ?> </option>
            <?php endforeach; ?>
        </select>
        </div>
        <div class="form-group">
        <label>Profesor</label>
        <select class="form-control" name="profesor">
            <option value=0 selected>Izaberi</option>
            <?php foreach($profesori as $profesor): ?>
            <option value=" <?=$profesor->id ?> "> <?=$profesor->first_name." ".$profesor->last_name ?> </option>
            <?php endforeach; ?>
        </select>
        </div>
        <br> <br>
        <div class="form-group">
            <label>Ime roditelja</label>
            <!-- Added set_value('name of the input') to get the form to repopulate this input -->
            <input type="text" class="form-control" name="f_name" value="<?php echo set_value('f_name'); ?>" placeholder="Ime roditelja">
        </div>
        <div class="form-group">
            <label>Prezime roditelja</label>
            <!-- Added set_value('name of the input') to get the form to repopulate this input -->
            <input type="text" class="form-control" name="l_name" value="<?php echo set_value('l_name'); ?>" placeholder="Prezime roditelja">
        </div>
        <div class="form-group">
            <label>JMBG roditelja</label>
            <!-- Added set_value('name of the input') to get the form to repopulate this input -->
            <input type="text" class="form-control" name="jmbg" value="<?php echo set_value('jmbg'); ?>" placeholder="JMBG">
        </div>
        <button type="submit" class="btn btn-primary">Snimi</button>
    </form>
    </div>
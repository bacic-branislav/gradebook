
    <?php require_once('header.php') ?>
    <table>
        <h4>Zahtevi za Otvorena vrata</h4>
        <thead>
            <tr>
                <th>Roditelj</th>
                <th>Vreme</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <form method="post" action="">
                <fieldset disabled="disabled">
                    <?php foreach($requests as $request) : ?>
                        <tr>
                            <form method="post">
                                <td><?php echo $request->ime .' '.$request->prezime; ?></td>
                                <td><?php echo $request->vreme; ?></td>
                                <td><?php echo $request->odobrenje; ?></td>
                                <td><input type="submit" name="mub" value="prihvati"></td>
                                <td><input type="submit" name="hub" value="odbij"></td>
                                <td><input type="hidden" name="id" value="<?php echo $request->id; ?>" readonly></td>
                            </form>
                        </tr>

                    <?php endforeach; ?>
                </fieldset> 
            </form>
        </tbody>        
    </table>

    <table>
        <h4>Prihvaceni zahtevi za Otvorena vrata</h4>
        <thead>
            <tr>
                <th>Roditelj</th>
                <th>Vreme</th>
            </tr>
        </thead>
        <tbody>
            <form method="post" action="">
                <fieldset disabled="disabled">
                    <?php foreach($approved as $approve) : ?>
                        <tr>
                            <form method="post">
                                <td><?php echo $approve->ime .' '.$approve->prezime ; ?></td>
                                <td><?php echo $approve->vreme; ?></td>
                            </form>
                        </tr>

                    <?php endforeach; ?>
                </fieldset> 
            </form>
        </tbody>        
    </table>
<div>    
    <div class="overlay" id="loading">
        <div class="overlay__inner">
            <div class="overlay__content"><span class="spinner"></span></div>
        </div>
    </div>
</div>
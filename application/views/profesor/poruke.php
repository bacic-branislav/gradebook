<?php require_once('header.php') ?>


<!-- Primljene poruke -->
<div class="card border-dark mb-3" style="max-width: 30rem;">
	<div class="card-header">
		<div class="form-group">
			<label for="izaberiRoditelja">Izaberi roditelja:</label> <br>
			<select class="form-control izaberiRoditelja" id="izaberiRoditelja" onchange="localStorage.setItem('name', this.value); document.location.reload(true);" style="min-width:200px">
			<script>
				var local_storage = localStorage.getItem('name');
				//console.log(local_storage);
			</script>
				<?php if(sizeof($roditelji) > 0) {
					foreach($roditelji as $roditelj) { 
						$name = $roditelj->roditelj_ime . "." . $roditelj->roditelj_prezime; ?>
						<script>
							var x = document.getElementById("izaberiRoditelja");
							var option = document.createElement("option");
							option.text = "<?= $name ?>";
							x.add(option);
							if(local_storage == "<?= $name ?>") {
								option.setAttributeNode(document.createAttribute("selected"));
							}
						</script>
					<?php } ?>
				<?php } ?><!-- dodaj else... mozda -->
			</select>
			<i class="fas fa-power-off" id="chatOnline"></i> <span id='statusOnline'></span>
		</div>
			<!-- Chat window -->
			<div class="chatWindow"  id="chatWindow">

				<!-- Primljene poruke -->

			</div>
			<label for="exampleSelect1"><span id="status" class="badge badge-primary badge-pill"></span></label>
			<form id="message-form">
			<div class="form-group">
				<label>Unesite poruku:</label>
				<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
			</div>
			<button type="submit" class="btn btn-outline-success">Pošalji poruku</button>
			</form>
		</div><!-- end of card body -->
	</div><!-- end of card header -->
</div><!-- end of Poruke -->

<?php $this->load->view("chat/chat") ?>
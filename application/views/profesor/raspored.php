<?php require_once('header.php') ?>

<!-- Raspored casova -->
<div class="studentClasses ">
	<table class="table table-hover" style="display: inline-block;">
		<thead>
			<tr style="background-color: #1a1a1a">
				<th scope="col">#</th>
				<th scope="col">Ponedeljak</th>
				<th scope="col">Utorak</th>
				<th scope="col">Sreda</th>
				<th scope="col">Četvrtak</th>
				<th scope="col">Petak</th>
			</tr>
		</thead>
		<tbody>
			<?=$raspored?>
			
		</tbody>
	</table>
</div><!-- end of Raspored casova -->

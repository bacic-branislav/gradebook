
<?php require_once('header.php') ?>

<!-- Profesor header -->
<div class="parentsHeader profHeader">
	<div class="form-group" id="razred">
		<?php echo form_open('profesor'); ?>
		<?php if(isset($_POST['izabranRazred']) && isset($_POST['izabranoOdeljenje'])) {
			$razred = $_POST['izabranRazred'];
			$odeljenje = $_POST['izabranoOdeljenje'];
		}else {
			$razred = 5;
			$odeljenje = '1';
		}?>
		<label>Razred</label>
		<select class="form-control" name="izabranRazred">
			<option value="5" <?= (($razred == 5)? "selected" : "" )?>> 5 </option>
			<option value="6" <?= (($razred == 6)? "selected" : "" )?>> 6 </option>
			<option value="7" <?= (($razred == 7)? "selected" : "" )?>> 7 </option>
			<option value="8" <?= (($razred == 8)? "selected" : "" )?>> 8 </option>
		</select>
	</div><!-- razred -->
	<div class="form-group">
		<label>Odeljenje</label>
		<select class="form-control" name="izabranoOdeljenje">
			<option value="1" <?= (($odeljenje == '1')? "selected" : "" )?>> 1 </option>
			<option value="2" <?= (($odeljenje == '2')? "selected" : "" )?>> 2 </option>
			<option value="3" <?= (($odeljenje == '3')? "selected" : "" )?>> 3 </option>
		</select>
	</div><!-- odeljenje -->
	<div class="form-group razredni razredniProf" style="margin-left:50px;">
		<label>Razredni starešina</label>
		<input class="form-control" type="text" value="<?=$razredni[0]->first_name." ".$razredni[0]->last_name ?>"
			readonly>
	</div> <br>
	<button type="submit" class="btn btn-outline-info prikazi" id="primeniPromenu">Prikaži</button>
	</form>
</div><!-- end of Profesor header -->



<!-- Ocene ucenika -->
<div class="studentGredes ">
	<table class="table table-hover table-bordered" style="text-align:center">
		<thead>
			<tr class="headline">
				<th colspan="2" scope="col">Učenik</th>
				<th colspan='2' scope='col'> <?= $predmet[0]->predmet ?> </th>
				<th scope="col">Opcije</th>
				<!-- Prikaz PDF svedocanstava samo ako je to njegov razred -->
				<?= ($razred == $teacher->razred_id && $odeljenje == $teacher->odeljenje_id)? '<th>Svedočanstva</th>':'' ?>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; foreach($ucenici as $ucenik) {
                    $i++;
                    //print_r($ucenik);
                    $kontroler_ucenik = $ucenik->id;
                    $ime_prezime = $ucenik->last_name. " " . $ucenik->first_name;
                    $kontroler_ocena = [];
                    //pravim loop za sve ucenike u ovom odelenju. $i sluzi za redni broj u dnevniku. $kontroler_unosa da proveri id ucenika
                    foreach($sve_ocene as $ocene){
                        if($ocene->ucenik_id == $kontroler_ucenik){
                            array_push($kontroler_ocena,$ocene);
                        }
                    } //posto originalni loop vadi iz tabele sve ocene, ovo proverava da li je ucenik id isti kao ucenik id iz ocena i samo njih pakuje u $kontroler_ocena
                ?>
			<tr>
				<form action='' method="post">
					<input type='hidden' name="ucenik" value="<?php echo $kontroler_ucenik; ?>">

					<!-- Ponovno slanje POST razred i odeljenja -->
					<input type='hidden' name="izabranRazred"
						value="<?= (isset($_POST['izabranRazred'])? $_POST['izabranRazred'] : "5" ) ?>">
					<input type='hidden' name="izabranoOdeljenje"
						value="<?= (isset($_POST['izabranoOdeljenje'])? $_POST['izabranoOdeljenje'] : "1" ) ?>">

					<td scope="row"> <?=$i ?> </td> <!-- redni broj -->
					<td>
						<?= $ime_prezime ?> <br> <a href="profesor/poruke"><i class="fas fa-comment"></i></a> <br> <i class="kontaktirajRoditelja prof">Kontaktiraj roditelja</i> 
					</td>
					<?php foreach($kontroler_ocena as $kontroler) {
                        //uzimam iz promenjive $kontroler_ocena, prolazim ih kroz loop posto ih sad ima po 4 (postaviti da je max)
                        $kontrolerOC = json_decode($kontroler->ocena); //dekodira ocene iz svakog kontrolera
                        $zakljucnaOC = $kontrolerOC->Zakljucna[0]; //uzima zakljucnu ocenu koja se upisuje kao vrednost
                        //loop koji pravi onoliko kolona koliko predmeta ima
                        echo "<td>";
                        
                        for($polje=0; $polje<4; $polje++) {
                            //pravim loop za 4 polja unosa
                                $uneta_ocena = $kontrolerOC->Ocene[$polje]; //uzimam vrednosti iz $kontroleraOC koji je sad array po indexu $polje [0,1,2,3]
                                $ime_unosa = $kontroler->predmet_id . "|" . $polje; //koristi se sa ispis $_POST metoda u formi kao ID predmeta + polje koje se unosi
                                $zakljucna_unos = $kontroler->predmet_id . "|" . 4;
                            echo " <input type='number' class='form-control' min='1' max='5' value='$uneta_ocena' name='$ime_unosa'> ";
                        }
                        echo "</td>";
                        echo "<td>
                                <input type='number' class='zakljuceno form-control' min='1' max='5' value='$zakljucnaOC' name='$zakljucna_unos'> <!-- Zakljuceno -->
                            </td>";
                        }
                        ?>
					<td class="prosekOcena">
						<button type="submit" class="btn btn-outline-success" name="snimi_ocenu">Snimi
							ocenu</button>
					</td>
					<!-- Prikaz PDF svedocanstava samo ako je to njegov razred -->
					<?php if($razred == $teacher->razred_id && $odeljenje == $teacher->odeljenje_id) {
                            echo '<td style="text-align:center">';
                                echo '<a href="'.base_url().'htmltopdf/pdfdetails/'.$ucenik->id.'" target="blank"><i class="far fa-file" style="font-size:50px; color:gray"></i></a>';
                            echo '</td>';
                        } ?>
				</form>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div><!-- end of Ocene ucenika -->

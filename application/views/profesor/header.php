<?php foreach($teachers as $teacher){} ?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<!-- logo -->
	<img src="<?php echo base_url().'img/logoWhite.png'?>" height="75" class="d-inline-block align-top">
	<div class="banner" style="width:55%;">
		<p><i class="fas fa-envelope"></i>piedpiper@gmail.com</p>
		<p class="phnumber"><i class="fas fa-phone-alt"></i>011/278-916</p>
		<p><i class="fas fa-map-marker-alt"></i>Sillicon Valley</p>
	</div>
	<div class="collapse navbar-collapse" id="navbarColor02">
		<ul class="navbar-nav mr-auto">
		</ul>
		<!-- Prijavljen korisnik -->
		<div class="form-inline my-2 my-lg-0">
			<ul class="nav nav-pills">
				<li class="nav-item">
				<?php //var_dump($this->session->userdata()); exit();?>
					<a href="" class="nav-link disabled"> <?= (isset($this->session->userdata['role']))? ucwords($this->session->userdata['role']).' - '.$predmet[0]->predmet : 'nema role' ?> </a>
				</li>
				<li class="nav-item">
					<a href="" class="nav-link disabled">Starešina: <?=$teacher->razred_id."/".$teacher->odeljenje_id ?> </a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
						aria-haspopup="true" aria-expanded="false">
						<?= (isset($this->session->userdata['user_id']))? $this->session->userdata['username'] : 'nema sesije' ?></a>
					<div class="dropdown-menu" x-placement="bottom-start"
						style="position: absolute; width:50px; text-align:center; will-change: transform; top: 0px; left: -17px; transform: translate3d(0px, 50px, 0px);">
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?= base_url().'admin/logout'?>">Odjavi se</a>
					</div>
				</li>
			</ul>
		</div><!-- end of Prijavljen korisnik -->
	</div>
</nav><!-- end of Navigation -->

<!-- Main content wrapper -->
<div class="container-fluid"> 

<!-- Menu tab-bar -->
<div class="navigacija">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>"><i class="fas fa-poll-h tab_icon"></i>Ocene</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/raspored'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/raspored')? 'active' : ''?>"><i class="fas fa-calendar-alt tab_icon"></i>Raspored</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/poruke'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/poruke')? 'active' : ''?>"><i class="fas fa-comment tab_icon"></i>Poruke</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/otvorenaVrata'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/otvorenaVrata')? 'active' : ''?>" ><i class="fas fa-address-card tab_icon"></i>Otvorena vrata</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().'posts'?>" class="nav-link <?=(current_url() == base_url().'posts')? 'active' : '' ?>"> <i class="fas fa-comment-alt tab_icon"></i>Obaveštenja</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/opravdanja'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/opravdanja')? 'active' : ''?>"><span style="color:green;"><i class="fas fa-scroll"></i></span> Opravdanja</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/izostanci'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/izostanci')? 'active' : ''?>"><span style="color:green;border:2px dashed green;border-radius:40%;"> B</span> Izostanci i Upis</a>
		</li>
	</ul>
</div><!-- end of Menu tab-bar -->
<?php require_once('header.php') ?>


	<!-- Teacher header -->
	<div class="parentsHeader teacherHeader">
		<div class="form-group">
			<label class="control-label">Razred</label>
			<input class="odeljenje form-control" type="text" value="<?= $teacher->razred_id ?>" readonly>
		</div><!-- razred -->
		<div class="form-group">
			<label class="control-label">Odeljenje</label>
			<input class="odeljenje form-control" type="text" value="<?= $teacher->odeljenje_id ?>" readonly>
		</div><!-- odeljenje -->
	</div><!-- end of Teacher header -->

	

	<!-- Ocene ucenika tabela -->
	<div class="studentGredes">
		<table class="table table-hover table-bordered" style="text-align:center;">
			<thead>
				<tr class="headline">
					<th colspan="2" scope="col">Učenik</th>
					<?php foreach($svi_predmeti as $predmet_id=>$predmet): ?>
					<th colspan='2' scope='col'><?=$predmet->predmet ?></th>
					<?php endforeach;?>
					<th scope="col">Opcije</th>
					<th scope="col">Svedočanstva</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$i=0;
					foreach($ucenici as $ucenik){
						$i++;
						$kontroler_ucenik = $ucenik->id;
						$ime_prezime = $ucenik->last_name. " " . $ucenik->first_name;
						$kontroler_ocena = [];
					//pravim loop za sve ucenike u ovom odelenju. $i sluzi za redni broj u dnevniku. $kontroler_unosa da proveri id ucenika
							foreach($sve_ocene as $ocene){
								if($ocene->ucenik_id == $kontroler_ucenik){
									array_push($kontroler_ocena,$ocene);
								}
							} //posto originalni loop vadi iz tabele sve ocene, ovo proverava da li je ucenik id isti kao ucenik id iz ocena i samo njih pakuje u $kontroler_ocena
					?>
				<tr>
					<form action='' method="post">
						<input type='hidden' name="ucenik" value="<?php echo $kontroler_ucenik; ?>">
						<td scope="row"><?=$i ?></td> 
						<td>
							<?= $ime_prezime ?> <br> <a href="ucitelj/poruke"><i class="fas fa-comment"></i></a> <br> <i class="kontaktirajRoditelja">Kontaktiraj roditelja</i> 
						</td> 
						<?php 
                        foreach($kontroler_ocena as $kontroler){
                            //uzimam iz promenjive $kontroler_ocena, prolazim ih kroz loop posto ih sad ima po 4 (postaviti da je max)
                            $kontrolerOC = json_decode($kontroler->ocena); //dekodira ocene iz svakog kontrolera
                            $zakljucnaOC = $kontrolerOC->Zakljucna[0]; //uzima zakljucnu ocenu koja se upisuje kao vrednost
                            //loop koji pravi onoliko kolona koliko predmeta ima
                                echo "<td style='width:80px'>";
                                
                                for($polje=0;$polje<4;$polje++){
                                    //pravim loop za 4 polja unosa
                                        $uneta_ocena = $kontrolerOC->Ocene[$polje]; //uzimam vrednosti iz $kontroleraOC koji je sad array po indexu $polje [0,1,2,3]
                                        $ime_unosa = $kontroler->predmet_id . "|" . $polje; //koristi se sa ispis $_POST metoda u formi kao ID predmeta + polje koje se unosi
                                        $zakljucna_unos = $kontroler->predmet_id . "|" . 4;
                                    echo " <input type='number' class='form-control' min='1' max='5' value='$uneta_ocena' name='$ime_unosa'> ";
                                }
                                echo "</td>";
                                echo "<td>
                                        <input type='number' class='zakljuceno form-control' min='1' max='5' value='$zakljucnaOC' name='$zakljucna_unos' style='height:210px'> <!-- Zakljuceno -->
                                    </td>";
                                } ?>
						<td class="prosekOcena">
							<button type="submit" class="btn btn-outline-success" name="snimi_ocenu">Snimi
								ocenu</button> <br>
							<label>Prosek</label> <br>
							<input type='number' class='form-control' value='<?php echo $this->Ocene_model->AverageGrade($kontroler_ucenik); ?>'>
						</td>
						<td style="text-align:center">
							<?php
                                echo '<a href="'.base_url().'htmltopdf/pdfdetails/'.$ucenik->id.'" target="blank"><i class="far fa-file" style="font-size:50px"></i></a>';
                                ?>
						</td>
					</form>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div><!-- end of Ocene -->

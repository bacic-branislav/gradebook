<div>
    <?php require_once('header.php') ?>
    <table>
        <h4>Zahtevi za Otvorena vrata</h4>
        <thead>
            <tr>
                <th>Roditelj</th>
                <th>Vreme</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <form method="post" action="">
                <fieldset disabled="disabled">
                    <?php foreach($zahtevi as $zahtev) : ?>
                        <tr>
                            <form method="post">
                                <td><?php echo $zahtev->ime .' '.$zahtev->prezime; ?></td>
                                <td><?php echo $zahtev->vreme; ?></td>
                                <td><?php echo $zahtev->odobrenje; ?></td>
                                <td><input type="submit" name="sub" value="prihvati"></td>
                                <td><input type="submit" name="bub" value="odbij"></td>
                                <td><input type="hidden" name="id" value="<?php echo $zahtev->id; ?>" readonly></td>
                            </form>
                        </tr>

                    <?php endforeach; ?>
                </fieldset> 
            </form>
        </tbody>        
    </table>

    <table>
        <h4>Prihvaceni zahtevi za Otvorena vrata</h4>
        <thead>
            <tr>
                <th>Roditelj</th>
                <th>Vreme</th>
            </tr>
        </thead>
        <tbody>
            <form method="post" action="">
                <fieldset disabled="disabled">
                    <?php foreach($prihvaceni as $prihvacen) : ?>
                        <tr>
                            <form method="post">
                                <td><?php echo $prihvacen->ime .' '.$prihvacen->prezime ; ?></td>
                                <td><?php echo $prihvacen->vreme; ?></td>
                            </form>
                        </tr>

                    <?php endforeach; ?>
                </fieldset> 
            </form>
        </tbody>        
    </table>
    <div class="overlay" id="loading">
        <div class="overlay__inner">
            <div class="overlay__content"><span class="spinner"></span></div>
        </div>
    </div>
</div>
<?php require_once('header.php') ?>


<div class="opravdanjeWrapper">
    <div class="opravdanje">
        <label>Nepročitana opravdanja: <span class="badge badge-success"><?=(sizeof($opravdanja)==0)?'0':sizeof($opravdanja) ?></span> </label><br>
        <select class="form-control" id="svaOpravdanja" <?=(sizeof($opravdanja)==0)?'disabled':''?>>
            <?php foreach($opravdanja as $opravdanje): ?>
            <option value="<?=$opravdanje->id?>"><?=$opravdanje->roditelj_ime.' '.$opravdanje->roditelj_prezime?></option>
            <?php endforeach; ?>
        </select>
        <button type="button" class="btn btn-outline-info" id="opravdanjaPrikazi" <?=(sizeof($opravdanja)==0)?'disabled':''?>>Prikaži</button>

        <div class="primnjenaOpravdanja">
            <label>Sva pročitana opravdanja: <span class="badge badge-success"><?=(sizeof($svaOpravdanja)==0)?'0':sizeof($svaOpravdanja) ?></span> </label>

            <div class="svaOpravdanja">
                <table class="table table-hover">
                <thead>
                    <th>Roditelj</th>
                    <th>Učenik</th>
                    <th>Opcije</th>
                </thead>
                <tbody>
                    <?php foreach($svaOpravdanja as $response): ?>
                    <tr>
                        <td><?=$response->roditelj_ime.' '.$response->roditelj_prezime ?></td>
                        <td><?=$response->ucenik_ime.' '.$response->ucenik_prezime ?></td>
                        <td class="opcije">
                            <!-- brisanje -->
                            <button type="button" class="btn btn-outline-danger" onclick="obrisiOpravdanje(<?=$response->id?>)" ><i class="far fa-trash-alt"></i></button>
                            <!-- prikaz -->
                            <button type="button" class="btn btn-outline-success" onclick="prikaziOpravdanje(<?=$response->id?>)"><i class="fas fa-arrow-right"></i></button>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
                </table>
            </div>
            <div class="istorijaOdg"></div>
        </div>
    </div> <!-- end of opravdanje primljeno -->

    <div class="opravdanjePrikaz">
        <h3>Opravdanje</h3>
        <label>Od roditelja</label>
        <input class="form-control" type="text" id="odRoditelja" readonly>
        <label>Za učenika</label>
        <input class="form-control" type="text" id="zaUcenika" readonly>
        <label>Sadržaj poruke:</label>
        <textarea class="form-control" rows="3" id="sadrzaj" readonly></textarea>
        <label>Dokumentacija:</label> <br>
        <img src="" id="slika"><br>
        <i>Datum: <br> <span id="vreme"></span></i> <br><br>
        <button type="button" class="btn btn-outline-success" id="procitano" <?=(sizeof($opravdanja)==0)?'disabled':''?>>Pročitano</button>
        <div class="opravdanjeOdg"></div>
    </div> <!-- end of opravdanje prikaz -->
</div> <!-- end of opravdanje wrapper -->


<?php // var_dump($svaOpravdanja); ?>



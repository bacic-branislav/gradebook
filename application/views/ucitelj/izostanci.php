<?php require_once('header.php') ?>
<?php
foreach($teachers as $teacher){}
date_default_timezone_set('Europe/Belgrade');
$time = date('h:i:s d-m-Y');

/** Ovaj deo se otvara ukoliko se odabere opcija za unos izostanaka **/

if(isset($_POST['izostanci']) or isset($_POST['odabraliSmo']) or isset($_POST['ukloniUpisanog'])){
   $izostanci = isset($_POST['izostanci'])?($_POST['izostanci']):"0,0";
   $izostanci = isset($_POST['izostanci2'])?($_POST['izostanci2']):$izostanci;
    ?>
    <div style="width:30%; float:right;">
        <h2>Odaberi Ucenike: <?=$izostanci?></h2>
        <table class='table table-hover table-dark table-bordered'>
            <tr style='text-align:center;'>
                <td>Upisi Izostanke</td>
                
            </tr>
            <form action="" method="post" style="width:50%;">
                <tr>   
                    <td>
                        <select multiple="multiple" name="absentees[]" placeholder="Nesto" style="width:100%;text-align:center;">
                        <?php
                        foreach($ucenici as $ucenik){
                            $vrednost = $ucenik->id;
                            if($absent == 0){
                                $ime_prezime = $ucenik->first_name . " " . $ucenik->last_name;
                                echo "<option value=$vrednost>$ime_prezime</option>";
                            }
                            else if(in_array($vrednost, $absent)){} 
                            else {
                            $ime_prezime = $ucenik->first_name . " " . $ucenik->last_name;
                            echo "<option value=$vrednost>$ime_prezime</option>";
                            }
                        }
                        ?>
                        </select>
                    </td>
                </tr>
                <tr style="text-align:center;">
                    <td><input type="submit" name="odabraliSmo" class='btn btn-outline-success' value="Unesi"></td>
                </tr>
                <input type="hidden" name="izostanci" value="<?=$izostanci?>">
                </form>
                <tr style="text-align:center;">
                    <td>Vec/Greskom Upisano</td>
                        
                </tr>
                <form action="" method = "post">
                <tr> 
                    <td>
                        <select multiple="multiple" name="remove_absentees[]" placeholder="Nesto" style="width:100%;text-align:center;">
                        <?php
                        foreach($ucenici as $ucenik){
                            $vrednost = $ucenik->id;
                            $ime_prezime = $ucenik->first_name . " " . $ucenik->last_name;
                            if($absent == 0){}
                            else if(in_array($vrednost, $absent)){
                                echo "<option value=$vrednost>$ime_prezime</option>";
                            }
                        }?>
                        </select>
                    </td>
                </tr>
                <tr  style="text-align:center;">        
                            <input type="hidden" name="izostanci2" value="<?=$izostanci?>">
                        <td><input type="submit" name="ukloniUpisanog" class='btn btn-outline-success' value="Ukloni"></td>
                </tr>
                </form>        
        </table>
    </div>
<?php
}

/** Ovaj deo se otvara ukoliko je odabrana opcija za Unos podataka za Cas **/

if(isset($_POST['upis_casova']) or isset($_POST['upis_submit'])){
    ?>
    <div style="width:30%; float:right;">
        <h2>Upisi Cas: <?=isset($_POST['upis_casova'])?($_POST['upis_casova']):""?></h2>
        <table class='table table-hover table-dark table-bordered'>
            <tr style='text-align:center;'>
                <td>Upisi Cas</td>
            </tr>
            <form action="" method="post" style="width:50%;">
                <tr>   
                    <td>
                        <input type="text" name="unesi_podatke" style="width:100%;min-height:50px;" value="<?=$upis_casa?>">
                    </td>
                </tr>
                <tr style="text-align:center;">
                        <input type="hidden" name="upis_casova" value="<?=$_POST['upis_casova']?>">
                        <input type="hidden" name="ucitelj_id" value="<?=$teacher->id?>">
                        <input type="hidden" name="vreme_unosa" value="<?=$time?>">
                    <td><input type="submit" name="upis_submit" class='btn btn-outline-success'></td>
                </tr>    
            </form>
        </table>
    </div>
<?php
}
?>

<!-- Deo za prikaz tabele -->

<div style="max-width:60%;float:left;">
    <h2>Upis casova i izostanaka </h2><br>
    <table class='table table-hover table-dark table-bordered'>
            <tr style='text-align:center;background-color: #1a1a1a'>
                <td>Čas</td>
                <th colspan="2">Ponedeljak</th>
                <th colspan="2">Utorak</th>
                <th colspan="2">Sreda</th>
                <th  colspan="2">Cetvrtak</th>
                <th colspan="2">Petak</th>
            </tr>
        <form action="" method="post">
        <?php
            for($i=1;$i<=7;$i++){
        echo "<tr style='text-align:center;' rowspan='2'>";
            echo "<td rowspan='2'>$i</td>";
            for($dan=1;$dan<=5;$dan++){
                $vrednost = $dan. "," . $i;
                echo "<td colspan='1' rowspan='1'><button type='submit' value=$vrednost placeholder='odaberi' class='btn btn-outline-success' name='upis_casova'>Upis</button></td>"; 
                echo "<td colspan='1' rowspan='1'><button type='submit' value=$vrednost placeholder='odaberi' class='btn btn-outline-success' name='izostanci'>Izostanci</button></td>";
                
                }
        echo "</tr>";
        echo "<tr>";
        $Cas = "Cas".$i;
            for($day=1;$day<=5;$day++){
                $result = $this->db->query("SELECT * FROM `raspored` inner join predmet on raspored.$Cas = predmet.id WHERE razred_id = $teacher->razred_id and odeljenje_id = $teacher->odeljenje_id and day = $day")->result();
                if(empty($result)){
                   echo "<td colspan='2' rowspan='1' style='text-align:center;'> </td>";
                } else {
                foreach($result as $cas){
                    echo "<td colspan='2' rowspan='1' style='text-align:center;'>$cas->predmet</td>";
                    }
                }
            }
            
        echo "</tr>";
            }
        ?>
        </form>
    </table>
</div>

<div stlye="clear:both;"></div>
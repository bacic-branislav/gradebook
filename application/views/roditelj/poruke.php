<?php require_once('header.php') ?>


<!-- Primljene poruke -->
<div class="card border-dark mb-3">
	<div class="card-header">
		<div class="form-group">
		<label><?=$staresina[0]->first_name.' '.$staresina[0]->last_name?><i class="fas fa-power-off iconRoditelj" id="chatOnline"></i> <span id='statusOnline'></span></label>
			
		</div>
		<div class="card-body">
			<!-- Chat window -->
			<div class="chatWindow" id="chatWindow">

				<!-- Primljene poruke -->

			</div>
			<label><span id="status" class="badge badge-primary badge-pill"></span></label>
			<form id="message-form">
			<div class="form-group">
				<label>Unesite poruku:</label>
				<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
			</div>
			<button type="submit" class="btn btn-outline-success">Pošalji poruku</button>
			</form>
		</div><!-- end of Card body -->
	</div><!-- end of Card header -->
</div><!-- end of Poruke -->

<?php $this->load->view("chat/chat") ?>

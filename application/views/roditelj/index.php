<?php require_once('header.php') ?>


<!-- Parent header -->
<div class="parentsHeader">
	<div class="form-group">
		<label for="exampleSelect1">Učenik:</label>
		<input class="ucenik form-control" type="text"
			value="<?= $ucenici[0]->first_name . " " . $ucenici[0]->last_name ?>" readonly>
	</div><!-- ucenik -->
	<div class="form-group">
		<label class="control-label">Razred</label>
		<input class="odeljenje form-control" type="text" value="<?= $ucenici[0]->razred ?>" readonly>
	</div><!-- razred -->
	<div class="form-group">
		<label class="control-label">Odeljenje</label>
		<input class="odeljenje form-control" type="text" value="<?= $ucenici[0]->odeljenje ?>" readonly>
	</div><!-- odeljenje -->
	<div class="form-group razredni">
		<?php ((($ucenici[0]->razred) > 4)? $label = 'Razredni starešina': $label = 'Učitelj') ?>
		<label class="control-label"> <?= $label ?> </label>
		<input class="form-control" type="text" value="<?= $staresina[0]->first_name." ".$staresina[0]->last_name ?>"
			readonly>
	</div><!-- razredni -->
</div><!-- end of Parent header -->

<?php// var_dump($staresina)?>

<!-- Ocene ucenika -->
<div class="studentGredes ">
	<table class="table table-hover" style="text-align:center">
		<thead>
			<tr class="headline">
				<th colspan="2">Predmeti</th>
				<th>Ocene</th>
				<th>Zaključeno</th>
			</tr>
		</thead>
		<tbody>
			<?php $br=1; foreach($ucenici as $ucenik): 
                    $dekodirane_ocene = json_decode($ucenik->ocena);
                    $zakljucna_ocena  = $dekodirane_ocene->Zakljucna[0];
                ?>
			<tr>
				<td><?=$br?></td>
				<td style="text-align:left"> <?=$ucenik->predmet?> </td>
				<td>
					<?php for($i=0;$i<4;$i++){
                            $ocene = $dekodirane_ocene->Ocene[$i];
                            echo "<input type='number' class='form-control roditeljOcene' readonly value='$ocene'> ";
                        } ?>
				</td>
				<td>
					<input type='number' class='zakljucenoRoditelj form-control' style="height:48px"
						value='<?=$zakljucna_ocena ?>' readonly><!-- Zakljuceno -->
				</td>
			</tr>
			<?php $br++; endforeach; ?>
			<tr class="table-dark prosek">
				<td></td>
				<td></td>
				<td></td>
				<td>Prosek: <span><?php echo $this->Ocene_model->AverageGrade($ucenik->id); ?></span></td>
			</tr>
		</tbody>
	</table>
</div><!-- end of Ocene ucenika -->

<?php require_once('header.php') ?>


<div class="slobodan_termin">
	<form method="post" action="">

		<?php foreach ($ucitelj as $uca) : ?>

			<input type="hidden" name="head" readonly="" value="<?php echo $uca->id; ?>">
			<p>Razredni staresina: <?php echo $uca->first_name .' ' .$uca->last_name; ?></p>

		<?php endforeach; ?>	
		<h5>Izaberi Termin</h5>
		<label>Izaberi dan</label>
		<input type="date" name=dan value="<?php echo date("Y-m-d");?>">

		<label>Izaberi termin</label>
		<select name="vreme">
			<option name="termin1">9.30 - 10.30</option>
			<option name="termin2">11.30 - 12.30</option>
			<option name="termin3">13.30 - 14.30</option>
			<option name="termin4">15.30 - 16.30</option>
		</select>
		<input type="hidden" name="zh">
		<input type="submit" name="zahtev" value="Posalji">
	</form>

    <table>
        <h4>Vasi zahtevi za Otvorena vrata</h4>
        <thead>
            <tr>
                <th>Dan</th>
                <th>Vreme</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($requests as $zahtev) : ?>
        		<tr>
        			<td><?php echo $zahtev->dan;?></td>
        			<td><?php echo $zahtev->time;?></td>
        			<td><?php echo $zahtev->approved;?></td>
        		</tr>
        	<?php endforeach; ?>	 			
        </tbody>
    </table>    


</div>
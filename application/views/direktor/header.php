<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<!-- logo -->
	<img src="<?php echo base_url().'img/logoWhite.png'?>" height="75" class="d-inline-block align-top">
	<div class="banner">
		<p><i class="fas fa-envelope"></i>piedpiper@gmail.com</p>
		<p class="phnumber"><i class="fas fa-phone-alt"></i>011/278-916</p>
		<p><i class="fas fa-map-marker-alt"></i>Sillicon Valley</p>
	</div>
	<div class="collapse navbar-collapse" id="navbarColor02">
		<ul class="navbar-nav mr-auto">
		</ul>
		<!-- Prijavljen korisnik -->
		<div class="form-inline my-2 my-lg-0">
			<ul class="nav nav-pills">
				<li class="nav-item">
					<a href="" class="nav-link disabled"> <?= (isset($this->session->userdata['role']))? $this->session->userdata['role'] : 'nema role' ?> </a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
						aria-haspopup="true" aria-expanded="false">
						<?= (isset($this->session->userdata['user_id']))? $this->session->userdata['username'] : 'nema sesije' ?></a>
					<div class="dropdown-menu" x-placement="bottom-start"
						style="position: absolute; width:50px; text-align:center; will-change: transform; top: 0px; left: -17px; transform: translate3d(0px, 50px, 0px);">
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?= base_url().'admin/logout'?>">Odjavi se</a>
					</div>
				</li>
			</ul>
		</div><!-- end of Prijavljen korisnik -->
	</div>
</nav><!-- end of Navigation -->

<!-- Main content wrapper -->
<div class="container"> 

<!-- Menu tab-bar -->
<div class="navigacija">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role']?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'])? 'active' : ''?>">Statistike odeljenja</a>
		</li>
		<li class="nav-item">
			<a href="<?= base_url().$this->session->userdata['role'].'/statistikeSkole'?>" class="nav-link <?=(current_url() ==  base_url().$this->session->userdata['role'].'/statistikeSkole')? 'active' : ''?>">Statistike škole</a>
		</li>
	</ul>
</div><!-- end of Menu tab-bar -->
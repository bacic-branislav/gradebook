<?php require_once('header.php')?>

<!-- <?php foreach($sviPredmeti as $predmet) {
    echo $predmet->predmet."<br>";
}?> -->
<!-- Direktor header -->
<div class="parentsHeader profHeader">
	<div class="form-group" id="razred">
		<?php echo form_open('direktor/statistikeSkole'); ?>
		<?php if(isset($_POST['izabranRazred'])) {
			$razred = $_POST['izabranRazred'];
			
		}else {
			$razred = 1;
			$odeljenje = '1';
		}?>
		<label>Razred</label>
		<select class="form-control" name="izabranRazred">
			<option value="1" <?= (($razred == 1)? "selected" : "" )?>> 1 </option>
			<option value="2" <?= (($razred == 2)? "selected" : "" )?>> 2 </option>
			<option value="3" <?= (($razred == 3)? "selected" : "" )?>> 3 </option>
			<option value="4" <?= (($razred == 4)? "selected" : "" )?>> 4 </option>
			<option value="5" <?= (($razred == 5)? "selected" : "" )?>> 5 </option>
			<option value="6" <?= (($razred == 6)? "selected" : "" )?>> 6 </option>
			<option value="7" <?= (($razred == 7)? "selected" : "" )?>> 7 </option>
			<option value="8" <?= (($razred == 8)? "selected" : "" )?>> 8 </option>
		</select>
	</div><!-- end of razred -->
	
    <button type="submit" class="btn btn-outline-info prikazi" id="primeniPromenu">Prikaži</button>
	</form>
</div><!-- end of Direktor header -->
<!-- Obavezan unos biblioteka -->
<script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="//www.amcharts.com/lib/4/themes/material.js"></script>

<!-- Ovo je element koji ce prikazivati grafikon -->
<div id="chartdiv" style="width: 900px; height 800px;"></div>

<script>

// Primeni teme animacija i osnovne boje
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_material);

// Chart instanca i poziv ka prikazu tipa sveca (XYChart)
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Kreiraj X osu i dodeli joj kategorije
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "odeljenje";

// Kreiraj Y osu i dodeli joj vrednosti
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Uspesnost (ocena)";

// Prikazi svece u 3D obliku
var series = chart.series.push(new am4charts.ColumnSeries3D());

// Dodaj vrednosti i kategorije
series.dataFields.valueY = "uspeh";
series.dataFields.categoryX = "odeljenje";

// Ucitaj podatke
chart.dataSource.url = "<?php echo base_url();?>odeljenja_razredi.json";

// // Jednobojni prikaz grafikona
// series.columns.template.fill = am4core.color("#104547");

// Visebojni prikaz (nijanse) | ako zelis jednobojni, ovaj zakomentarisi 
series.heatRules.push({
 "target": series.columns.template,
 "property": "fill",
 "min": am4core.color("#cfe1ff"),
 "max": am4core.color("#7c2be0"),
 "dataField": "valueY"
});

// Razni ispisi na grafikonu
series.name = "Uspesnost odeljenja";
series.columns.template.tooltipText = "Odeljenje: {categoryX}\nUspesnost: {valueY}";

// Zadatak_2 - uspesnost odeljenja po razredima na nivou skole
/** 
* dobavi sve zakljucene ocene jednog odeljenja i izracunaj njihov prosek
* napravi selektor razreda i postojecih odeljenja
* odeljenje Prvo 1 je prvo odeljenje prvog razreda itd...
*/ 
// chart.data = [{
//   "odeljenje": "Prvo 1",
//   "uspeh": 5
// }, {
//   "odeljenje": "Prvo 2",
//   "uspeh": 3
// }, {
//   "odeljenje": "Prvo 3",
//   "uspeh": 4
// }];

// Legenda
chart.legend = new am4charts.Legend();
</script>
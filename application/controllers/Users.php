<?php
class Users extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('User_model');
    }

    // register user
    // will display the register page loading the view and will also submit
    public function register() {
        $data['title'] = "Unesi novog korisnika";

        // dobavljanje svih ucitelja - profesora
        $data['ucitelji'] = $this->User_model->get_ucitelji();
        $data['profesori'] = $this->User_model->get_profesori();

        //setting validation message ine serbian
        $this->form_validation->set_message('required', '<span style="color: red;">Polje <i>%s</i> mora biti popunjeno!');

        //setting validation rules for parent
        $this->form_validation->set_rules('f_name', 'Ime', 'required');
        $this->form_validation->set_rules('l_name', 'Prezime', 'required');
        $this->form_validation->set_rules('jmbg', 'JMBG', 'required');

        // setting validation rules for child
        $this->form_validation->set_rules('child_f_name', 'Ime', 'required');
        $this->form_validation->set_rules('child_l_name', 'Prezime', 'required');
        $this->form_validation->set_rules('razred', 'Razred', 'required'); // Validation?
        $this->form_validation->set_rules('odeljenje', 'Odeljenje', 'required'); // Validation?

        if($this->form_validation->run() === false) {
            $this->load->view('templates/header');
            $this->load->view('users/register', $data);
            $this->load->view('templates/footer');
        } else {
            // rola Roditelj

            // ucenik
            if($this->input->post('role') == 'roditelj') {
                
                $deteime = $this->input->post('child_f_name');
                $deteprezime = $this->input->post('child_l_name');
                $razred = $this->input->post('rezred');
                $odeljenje = $this->input->post('odeljenje');
                $ucitelj_id = $this->input->post('ucitelj');
                $profesor_id = $this->input->post('profesor');
    
                $this->User_model->register_ucenik($deteime, $deteprezime, $razred, $odeljenje, $ucitelj_id, $profesor_id);
    
                // user
                $hash_password = password_hash($this->input->post('jmbg'), PASSWORD_DEFAULT); // hashing jmbg to use as password
                $username = $this->input->post('f_name') . "." . $this->input->post('l_name'); // formating username from name, concat dot and concat last name of a registered person
                $role = $this->input->post('role');

                $this->User_model->register_user($hash_password, $username, $role);

                // roditelj
                $jmbg = $this->input->post('jmbg');
                $first_name = $this->input->post('f_name');
                $last_name = $this->input->post('l_name');

                $this->User_model->register_roditelj($jmbg, $first_name, $last_name);
               
                // set flash message in seesion | first param choose name, second is a message
                $this->session->set_flashdata('user_registered', 'Korisnik je registrovan i moze se logovati');
                redirect('users/register');
            
            } else if($this->input->post('role') == 'ucitelj'){
               
            } else if($this->input->post('role') == 'profesor'){

            } else if($this->input->post('role') == 'admin'){

            } else if($this->input->post('role') == 'direktor'){

            }
 

            
        }
    }

    // login user
    public function login() {
        $data['title'] = "Uloguj se";

        //setting validation rules for parent/child form
        $this->form_validation->set_message('required', '<span style="color: red;">Polje <i>%s</i> mora biti popunjeno!');
        $this->form_validation->set_rules('username', 'Korisnicko ime', 'required');
        $this->form_validation->set_rules('password', 'Sifra', 'required');

        if($this->form_validation->run() === false) {
            $this->load->view('templates/header');
            $this->load->view('users/login', $data);
            $this->load->view('templates/footer');
        } else {
            // get username from form
            $username = $this->input->post('username');

            // get the password from form
            $password = $this->input->post('password');

            //check if username exits
            $user = $this->User_model->match_username($username);

            // check if $user is array. if it is, model is returning an array with result() method, which means that database has two or more exact names
            // in case professor's or teacher's child goes to same school where one works, password will be the same for both db entries, but if exact name represents different persons then we have to check that each password matches the input and then proceed with password_verify()
            if(is_array($user)) {
                foreach($user as $hash) { //$hash is an object inside $user array
                    if(password_verify($password, $hash->password)) { // REMINDER! If parent == teacher or proffessor. one should choose how to log, as a parrent, teacher or proffessor
                        //if($hash->role == "profesor") { // Za Mileta Kitica... Jer je isti covek, sa dve uloge
                            $hashed_password = $hash->password;
                            break;
                        //}
                    }
                }
            } else {
                // if model returns an object, it is because there is only one matched name in db. we than take its password field and set it to a variable
                $hashed_password = $user->password;
            }
            
            if(password_verify($password, $hashed_password)) {
                $user = $this->User_model->login($username, $hashed_password);
                if($user) {
                    echo $user->role;
                    //create session
                    $user_data = array(
                        'user_id' => $user->id,
                        'username' => $username,
                        'role' => $user->role,
                        'logged_in' => true

                    );
                    $this->session->set_userdata($user_data);

                    $this->session->set_flashdata('user_loggedin', 'Uspesno ste ulogovani');
                    redirect('profesor/index');
                }
            } else {
                $this->session->set_flashdata('login_failed', 'Doslo je do greske. Pokusajte ponovo!');
                redirect('users/login');
            }
        }
    }

    // Logout user
    public function logout() {
        // unset user data
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('logged_in');

        // set message
        $this->session->set_flashdata('user_loggedout', 'Uspesno ste se izlogovali');
        redirect('users/login');
    }
    // if exists... whatever
    public function check_username_exists($username) {
        $this->form_validation->set_message('check_username_exits', 'That username is taken. Try another.');
        if($this->User_model->check_username_exists($username)) {
            return true;
        } else {
            return false;
        }
    }
}
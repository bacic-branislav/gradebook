<?php
    class Roditelj extends CI_Controller {
       
        private $sesija;

        function __construct() { 
            parent::__construct();

            // PROVERE TEST
            if(!$this->session->userdata('user_id')) {
                die('Morate biti prijavljeni<br> <a href="admin/login">Nazad</a>');
            }else if($this->session->userdata('role') != 'roditelj') {
                die("Samo roditelji imaju pristup ovoj stranici<br> Vi ste: {$this->session->userdata['role']} <br><a href='admin/login'>Nazad</a>");
            }
            
            $this->load->model('Roditelj_model');
            $this->sesija = $this->session->userdata();
        }

        public function index() {

            $this->Roditelj_model->get_roditelj($this->sesija);
            $data['ucenici'] = $this->Roditelj_model->get_dete();
            $data['staresina'] = $this->Roditelj_model->get_staresina();
            
            $this->load->view("templates/header.php");
            $this->load->view("roditelj/index", $data);
            $this->load->view("templates/footer.php");
        }

        public function raspored() {
            $this->Roditelj_model->get_roditelj($this->sesija);
            $data['ucenici'] = $this->Roditelj_model->get_dete();
            foreach($data['ucenici'] as $dete){
                $_razred        = $dete->razred;
                $_odeljenje     = $dete->odeljenje;
            }
            $data["Schedule"]   = $this->Schedule_model->getSchedule($_razred,$_odeljenje);
            $this->load->view("templates/header.php");
            $this->load->view("roditelj/raspored", $data);
            $this->load->view("templates/footer.php");
        }

        public function poruke() {
            $data['poruke']    = $this->Roditelj_model->get_roditelj($this->sesija);
            $data['ucenici']   = $this->Roditelj_model->get_dete();
            $data['staresina'] = $this->Roditelj_model->get_staresina();
            
            $this->load->view("templates/header.php");
            $this->load->view("roditelj/poruke", $data);
            $this->load->view("templates/footer.php");
        }

        public function otvorenaVrata() {
            $data['poruke'] = $this->Roditelj_model->get_roditelj($this->sesija);
            $data['ucenici'] = $this->Roditelj_model->get_dete();
            $data['otvorena_vrata'] = $this->Roditelj_model->get_otvorena_vrata();
            $data['otvorena_vrata_ucitelj'] = $this->Roditelj_model->get_otvorena_vrata_ucitelj();
            $data['staresina'] = $this->Roditelj_model->get_staresina();
            $data['ucitelj'] = $this->Roditelj_model->headTeacher();
            $data['requests'] = $this->Roditelj_model->showMyRequest();      
    
            $razredni = $data['staresina'][0]->razred;
    
            if($razredni <= 4){
                if (isset($_POST['zh'])) {
                    $rawdate = htmlentities($_POST['dan']);
                    $date = date('d-m-Y', strtotime($rawdate));
                    $time = $_POST['vreme'];
                    $staresina = $data['staresina'][0]->id;
    
                    $this->Roditelj_model->requestAppointmentForTeacher($staresina, $date, $time);
    
                    
                    header('Location: '. current_url());
                    die;         
    
    
                }
            }
            if($razredni >4){
                if(isset($_POST['zh'])){
                    $rawdate = htmlentities($_POST['dan']);
                    $date = date('d-m-Y', strtotime($rawdate));
                    $time = $_POST['vreme'];
                    $staresina = $data['staresina'][0]->id;
    
                    $this->Roditelj_model->requestAppointmentForProfesor($staresina, $date, $time);
    
                    
                    header('Location: '. current_url());
                    die; 
                }    
            }    
    
            
    
    
            $this->load->view("templates/header.php");
            $this->load->view("roditelj/otvorenaVrata", $data);
            $this->load->view("templates/footer.php");
            
            // $this->load->view("templates/header.php");
            // $this->load->view("roditelj/otvorenaVrata", $data);
            // $this->load->view("templates/footer.php");
        }

        // OPRAVDANJA
        public function opravdanja() {
            // Kada roditelj salje opravdanje
            if(isset($_POST['user_id']) && isset($_POST['staresina_id']) && isset($_POST['opravdanje_sadrzaj']) && isset($_FILES['opravdanje_file']['name'])) {

                $user_id      = $_POST['user_id'];
                $staresina_id = $_POST['staresina_id'];
                $razred       = $_POST['razred'];
                $sadrzaj      = $_POST['opravdanje_sadrzaj'];
                $slika        = $_FILES['opravdanje_file']['name'];
                $slika_tmp    = $_FILES['opravdanje_file']['tmp_name'];;
                // Provere
                if(empty($staresina_id) || empty($sadrzaj) || empty($slika)) {
                    echo "Morate popuniti sva polja";
                    return;
                }else if(!getimagesize($_FILES['opravdanje_file']['tmp_name'])) {
                    echo "Fajl mora biti slika";
                    return;
                }else if($_FILES['opravdanje_file']['size']>150000) {
                    echo "Slika mora biti manja od 100kb";
                    return;
                }

                $data['opravdanja'] = $this->Roditelj_model->opravdanje_posalji($razred, $user_id, $staresina_id, $sadrzaj, $slika, $slika_tmp);
                return;
            }


            $this->Roditelj_model->get_roditelj($this->sesija);
            $data['ucenici'] = $this->Roditelj_model->get_dete();
            $data['staresina'] = $this->Roditelj_model->get_staresina();
            $this->load->view("templates/header.php");
            $this->load->view("roditelj/opravdanja", $data);
            $this->load->view("templates/footer.php");
        }
    }
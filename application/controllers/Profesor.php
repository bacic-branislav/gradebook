<?php
    class Profesor extends CI_Controller {

        private $sesija;
        
        function __construct(){
            parent::__construct();

             // PROVERE TEST
             if(!$this->session->userdata('user_id')) {
                die('Morate biti prijavljeni<br> <a href="admin/login">Nazad</a>');
            }else if($this->session->userdata('role') != 'profesor') {
                die("Samo profesori imaju pristup ovoj stranici<br> Vi ste: {$this->session->userdata['role']} <br><a href='admin/login'>Nazad</a>");
            }

            $this->load->model('Profesor_model');
            $this->sesija = $this->session->userdata();
        }    
        
        public function index() {
            if(isset($_POST['izabranRazred']) && isset($_POST['izabranoOdeljenje'])) {
                $razred = $_POST['izabranRazred'];
                $odeljenje = $_POST['izabranoOdeljenje'];
            }else {
                $razred = 5;
                $odeljenje = '1';
            }

            $data["teachers"]  = $this->Profesor_model->teacher_data($this->sesija);
            $data['razredni']  = $this->Profesor_model->razredni($razred, $odeljenje);
            $data["ucenici"]   = $this->Profesor_model->SpisakUcenika($razred, $odeljenje);
            $data['predmet']   = $this->Profesor_model->getPredmet();
            $data["sve_ocene"] = $this->Profesor_model->getOcene();
            
            $this->load->view("templates/header.php");
            $this->load->view("profesor/index", $data);
            $this->load->view("templates/footer.php");         
        }

        public function raspored() {
            
            $data["teachers"] = $this->Profesor_model->teacher_data($this->sesija);
            foreach($data['teachers'] as $teacher){
                $prof_id = $teacher->id;
            }
            $data['predmet']  = $this->Profesor_model->getPredmet();
            $data['raspored'] = $this->Schedule_model->getScheduleProf($prof_id);

            $this->load->view("templates/header.php");
            $this->load->view("profesor/raspored", $data);
            $this->load->view("templates/footer.php"); 
        }

        public function poruke() {
            
            $data["teachers"] = $this->Profesor_model->teacher_data($this->sesija);
            $data['predmet'] = $this->Profesor_model->getPredmet();
            $data['roditelji'] = $this->Profesor_model->chatRoditelji($this->sesija);
            
            $this->load->view("templates/header.php");
            $this->load->view("profesor/poruke", $data);
            $this->load->view("templates/footer.php"); 
        }

        public function otvorenaVrata() {
            
            $data['teachers'] = $this->Profesor_model->teacher_data($this->sesija);
            $data['predmet'] = $this->Profesor_model->getPredmet();
            $data['otvorena_vrata'] = $this->Profesor_model->otvorena_vrata();
            $data['requests'] = $this->Profesor_model->showAppointments();
            $data['approved'] = $this->Profesor_model->showAcceptedAppointments();
            $this->Profesor_model->otvorena();
            $this->Profesor_model->prihvati();

            if (isset($_POST['mub'])) {
            $id = $_POST['id'];

            $this->Profesor_model->acceptAppointment($id);
            header('Location: '. current_url());

            }
            if (isset($_POST['hub'])) {
                $id = $_POST['id'];

                $this->Profesor_model->refuseAppointment($id);
                header('Location: '. current_url());
            }

            $this->load->view("templates/header.php");
            $this->load->view("profesor/otvorenaVrata", $data);
            $this->load->view("templates/footer.php"); 
        }

        public function opravdanja() {

            $data["teachers"]      = $this->Profesor_model->teacher_data($this->sesija);
            $data['predmet']       = $this->Profesor_model->getPredmet();
            $data['opravdanja']    = $this->Profesor_model->getOpravdanja();
            $data['svaOpravdanja'] = $this->Profesor_model->svaOpravdanja();
            $this->Profesor_model->prihvati();
            $this->Profesor_model->otvorena();

            // Prikaz izabranog opravdanja
            if(isset($_POST['id'])) {
                $id = $_POST['id'];
                echo JSON_encode($this->Profesor_model->getSadrzajOpravdanja($id));
                return;
            }
            // Postavljanje opravdanja u procitano
            if(isset($_POST['procitano'])) {
                $id = $_POST['procitano'];
                $this->Profesor_model->procitanoOpravdanje($id);
                return;
            }
            // Brisanje opravdanja
            if(isset($_POST['brisanje_id'])) {
                $id = $_POST['brisanje_id'];
                $this->Profesor_model->obrisiOpravdanje($id);
                return;
            }
            // Prikaz primljenog opravdanja
            if(isset($_POST['prikazi_id'])) {
                $id = $_POST['prikazi_id'];
                echo JSON_encode($this->Profesor_model->getSadrzajOpravdanja($id));
                return;
            }

            $this->load->view("templates/header.php");
            $this->load->view("profesor/opravdanja", $data);
            $this->load->view("templates/footer.php");
        }

        /** Deo za Upis casova i Izostanaka **/
        public function izostanci() {
            $data["teachers"]       = $this->Profesor_model->teacher_data($this->sesija);
            $data["upis_casa"]      = $this->Profesor_model->UpisCasa();
            $data["absent"]         = $this->Profesor_model->Absentees();
            $data['predmet']        = $this->Profesor_model->getPredmet();
            $data['ucenici']        = $this->Profesor_model->StudentsForAbsenteesm();
            
            $this->load->view("templates/header.php");
            $this->load->view("profesor/izostanci", $data);
            $this->load->view("templates/footer.php");
        }

       






        

            
        
            
    }

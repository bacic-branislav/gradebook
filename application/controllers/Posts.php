<?php
class Posts extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Post_model');
    }   

    public function index($offset = 0) {

        // pagination
        $config['base_url'] = base_url() . "posts/index/";
        $config['total_rows'] = $this->db->count_all('posts');
        $config['per_page'] = 3;
        $config['uri_segment'] = 3; // shows which segment of URI contains the page number
        $config['attributes'] = array('class' => 'pagination-link');

        $this->pagination->initialize($config, $config['per_page'], $offset);

        // variables that we want to pass into the view
        //$data["title"] = "Obaveštenja";

        // first we create active model and call our method to get all the posts from our database
        $data["posts"] = $this->Post_model->get_posts(false, $config['per_page'], $offset); // first param false sets the slug from Post_model's method get_posts (cant be ignored)

        // loading our views
        $this->load->view("templates/header.php");
        $this->load->view("posts/index", $data);
        $this->load->view("templates/footer.php");
    }

    // for viewing separate whole posts, since all post page will have truncated posts view
    public function view($slug = null) {
        $data['post'] = $this->Post_model->get_posts($slug);

        if(empty($data['post'])) {
            show_404();
        }
        $data['title'] = $data['post']['title'];

        $this->load->view("templates/header.php");
        $this->load->view("posts/view", $data);
        $this->load->view("templates/footer.php");
    }

    //method to create new post
    public function create() {
        $data['title'] = "Postavi novo obaveštenje";

        //form validation rules with custom message in serbian
        $this->form_validation->set_message('required', '<span style="color: whitesmoke;">%s mora biti unešen!');
        $this->form_validation->set_rules('title', 'Naslov', 'required');
        $this->form_validation->set_rules('body', 'Tekst obaveštenja', 'required');

        if($this->form_validation->run() === false) {
            $this->load->view("templates/header.php");
            $this->load->view("posts/create", $data);
            $this->load->view("templates/footer.php");
        } else {
            $this->Post_model->set_post();
            redirect('posts');
        }
    }

    public function delete($id) {
        $this->Post_model->delete_post($id);
        redirect('posts');
    }
    
    public function edit($slug) {
        $data['post'] = $this->Post_model->get_posts($slug);

        // Check user for validation
        $logedUser = $this->session->userdata['user_id'];
        $postOwner = $this->Post_model->get_posts($slug)['user_id'];
        if($logedUser != $postOwner) {
            redirect('posts');
        }

        if(empty($data['post'])) {
            show_404();
        }
        $data['title'] = 'Uredi obavestenje';

        $this->load->view("templates/header.php");
        $this->load->view("posts/edit", $data);
        $this->load->view("templates/footer.php");
    }
    public function update() {
        $this->Post_model->update_post();
        redirect('posts');
    }
} 
<?php 
    class Direktor extends CI_Controller {

        private $sesija;

        function __construct() {
            parent::__construct();

            // PROVERE
            if(!$this->session->userdata('user_id')) {
                die('Morate biti prijavljeni<br> <a href="admin/login">Nazad</a>');
            }else if($this->session->userdata('role') != 'direktor') {
                die("Samo direktor ima pristup ovoj stranici<br> Vi ste: {$this->session->userdata['role']} <br><a href='admin/login'>Nazad</a>");
            }

            $this->load->model('Direktor_model');
            $this->sesija = $this->session->userdata();
        }  

        public function index() {
            // Provera da li je izabran razred/odeljenje
            if(isset($_POST['izabranRazred']) && isset($_POST['izabranoOdeljenje'])) {
                $razred    = $_POST['izabranRazred'];
                $odeljenje = $_POST['izabranoOdeljenje'];
            } else {
                $razred    = 1;
                $odeljenje = 1;
            }


            $data['predmeti'] = $this->Direktor_model->getPredmeti($razred);
            $this->Direktor_model->zakljucne_ocene($razred,$odeljenje);
            // Ucitava prikaz za View
            $this->load->view("templates/header.php");
            $this->load->view("direktor/index", $data);
            $this->load->view("templates/footer.php");
        }

        public function statistikeSkole() {
             // Provera da li je izabran razred/odeljenje
             if(isset($_POST['izabranRazred']) ) {
                $razred    = $_POST['izabranRazred'];
                
            } else {
                $razred    = 5;
               
            }



            $data['sviPredmeti'] = $this->Direktor_model->getAllPredmeti();
			$this->Direktor_model->uspeh_odeljenja($razred);
            // Ucitava prikaz za View
            $this->load->view("templates/header.php");
            $this->load->view("direktor/statistikeSkole", $data);
            $this->load->view("templates/footer.php");
        }


    }
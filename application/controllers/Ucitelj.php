<?php
    class Ucitelj extends CI_Controller {

        protected $sesija;

        function __construct(){
            parent::__construct();

            // PROVERE TEST
            if(!$this->session->userdata('user_id')) {
                die('Morate biti prijavljeni<br> <a href="admin/login">Nazad</a>');
            }else if($this->session->userdata('role') != 'ucitelj') {
                die("Samo ucitelji imaju pristup ovoj stranici<br> Vi ste: {$this->session->userdata['role']} <br><a href='admin/login'>Nazad</a>");
            }

            
            $this->load->model('Ucitelj_model');
            $this->sesija = $this->session->userdata();
        } 
        public function index() {

            $data["teachers"]     = $this->Ucitelj_model->teacher_data($this->sesija);
            $data["ucenici"]      = $this->Ucitelj_model->SpisakUcenika();
            $data["sve_ocene"]    = $this->Ucitelj_model->getOcene();
            //$data['predmeti']   = $this->Ucitelj_model->Predmeti();
            $data['svi_predmeti'] = $this->Ucitelj_model->getPredmeti(); 


            $this->load->view("templates/header.php");
            $this->load->view("ucitelj/index", $data);
            $this->load->view("templates/footer.php");
        }
       
        public function poruke() {
            $data["teachers"]   = $this->Ucitelj_model->teacher_data($this->sesija);
            $data['onlineUsers'] = $this->Ucitelj_model->getOnlineUsers();
            $data['roditelji'] = $this->Ucitelj_model->chatRoditelji($this->sesija);

            $this->load->view("templates/header.php");
            $this->load->view("ucitelj/poruke", $data);
            $this->load->view("templates/footer.php");
        }

        public function raspored() {
            $data["teachers"]   = $this->Ucitelj_model->teacher_data($this->sesija);
            foreach($data['teachers'] as $teacher){
                $_razred        = $teacher->razred_id;
                $_odeljenje     = $teacher->odeljenje_id;
            }
            $data["Schedule"]   = $this->Schedule_model->getSchedule($_razred,$_odeljenje);
            $this->load->view("templates/header.php");
            $this->load->view("ucitelj/raspored", $data);
            $this->load->view("templates/footer.php");
        }

        public function otvorenaVrata() {
            $data['teachers'] = $this->Ucitelj_model->teacher_data($this->sesija);
            $data['zahtevi'] = $this->Ucitelj_model->showAppointments();
            $data['prihvaceni'] = $this->Ucitelj_model->showAcceptedAppointments();
            $this->load->view("ucitelj/otvorenaVrata", $data);
    
           
    
            
            if (isset($_POST['sub'])) {
                $id = $_POST['id'];
    
                $this->Ucitelj_model->acceptAppointment($id);
                header('Location: '. current_url());
    
            }
            if (isset($_POST['bub'])) {
                $id = $_POST['id'];
    
                $this->Ucitelj_model->refuseAppointment($id);
                header('Location: '. current_url());
            }
    
    
    
    
            $this->load->view("templates/header.php");
            $this->load->view("templates/footer.php");
        }

        public function opravdanja() {

            $data["teachers"]       = $this->Ucitelj_model->teacher_data($this->sesija);
            $data['otvorena_vrata'] = $this->Ucitelj_model->otvorena_vrata();
            $data['opravdanja']     = $this->Ucitelj_model->getOpravdanja();
            $data['svaOpravdanja']  = $this->Ucitelj_model->svaOpravdanja();
            $this->Ucitelj_model->prihvati();
            $this->Ucitelj_model->otvorena();

            // Prikaz izabranog opravdanja
            if(isset($_POST['id'])) {
                $id = $_POST['id'];
                echo JSON_encode($this->Ucitelj_model->getSadrzajOpravdanja($id));
                return;
            }
            // Postavljanje opravdanja u procitano
            if(isset($_POST['procitano'])) {
                $id = $_POST['procitano'];
                $this->Ucitelj_model->procitanoOpravdanje($id);
                return;
            }
            // Brisanje opravdanja
            if(isset($_POST['brisanje_id'])) {
                $id = $_POST['brisanje_id'];
                $this->Ucitelj_model->obrisiOpravdanje($id);
                return;
            }
            // Prikaz primljenog opravdanja
            if(isset($_POST['prikazi_id'])) {
                $id = $_POST['prikazi_id'];
                echo JSON_encode($this->Ucitelj_model->getSadrzajOpravdanja($id));
                return;
            }

            $this->load->view("templates/header.php");
            $this->load->view("ucitelj/opravdanja", $data);
            $this->load->view("templates/footer.php");
        }
        public function izostanci() {
            $data["teachers"]       = $this->Ucitelj_model->teacher_data($this->sesija);
            $data["upis_casa"]      = $this->Ucitelj_model->UpisCasa();
            $data["ucenici"]        = $this->Ucitelj_model->SpisakUcenika();
            $data["absent"]         = $this->Ucitelj_model->Absentees();
            
            $this->load->view("templates/header.php");
            $this->load->view("ucitelj/izostanci", $data);
            $this->load->view("templates/footer.php");
        }
    }
<?php
class Admin extends CI_Controller {

    private $sesija;

    function __construct() {
        parent::__construct();

        // PROVERE LOGIN
        $loginURL = base_url().'admin/login';
        $logoutURL = base_url().'admin/logout';

        if(current_url() != $loginURL && current_url() != $logoutURL) {
            if(!$this->session->userdata('user_id')) {
                die("Morate biti prijavljeni<br> <a href='http://localhost/gradebook/admin/login'>Nazad</a>");
            }else if($this->session->userdata('role') != 'admin') {
                die("Samo profesori imaju pristup ovoj stranici<br> Vi ste: {$this->session->userdata['role']} <br><a href='http://localhost/gradebook/admin/login'>Nazad</a>");
            }
        }
        $this->load->model('Admin_model');
        $this->sesija = $this->session->userdata();
    } 

    public function validiraj_text($param) {
        if(!preg_match("/^[a-zA-Z ]+$/", $param)) {
            echo "Nije ispravan unos!";
            exit();
        } else 
            return trim($param);
    }

    public function validiraj_broj($param) {
        if(!preg_match("/^[0-9]+$/", $param)) {
            echo "Dozvoljeni su samo brojevi!";
            exit();
        } else 
            return $param;
    }

    public function index() {
        /*------------------------ PRIKAZ SVIH KORISNIKA ---------------------*/
        // Prikaz svih ucenika
        if(isset($_GET['ucenici'])) {
            $ucenici = $this->Admin_model->getAllStudents();
            echo json_encode($ucenici);
            exit();
        }
        // Prikaz svih roditelja
        if(isset($_GET['roditelji'])) {
            $roditelji = $this->Admin_model->getAllParents();
            echo JSON_encode($roditelji);
            exit();
        }
        // Prikaz svih ucitelja
        if(isset($_GET['ucitelji'])) {
            $ucitelji = $this->Admin_model->getAllTeachers();
            echo JSON_encode($ucitelji);
            exit();
        }
        // Prikaz svih profesora
        if(isset($_GET['profesori'])) {
            $profesori = $this->Admin_model->getAllProfesors();
            echo JSON_encode($profesori);
            exit();
        }
        // Prikaz svih administratora
        if(isset($_GET['admini'])) {
            $admini = $this->Admin_model->getAllAdmins();
            echo JSON_encode($admini);
            exit();
        }
        // Prikaz svih direktora
        if(isset($_GET['direktori'])) {
            $direktori = $this->Admin_model->getAllDirectors();
            echo JSON_encode($direktori);
            exit();
        }
        /*------------------------- EDITOVANJE POSTOJECIH KORISNIKA ---------------*/

        // EDIT ucenik
        if(isset($_POST['ucenik_id']) && isset($_POST['ucenik_ime']) && isset($_POST['ucenik_prezime'])) {
            $id      = $_POST['ucenik_id'];
            $ime     = $this->validiraj_text($_POST['ucenik_ime']);
            $prezime = $this->validiraj_text($_POST['ucenik_prezime']);

            $this->Admin_model->updateUcenik($id, $ime, $prezime);
            // success
            echo 'Uspešno izmenjeno';
            exit();

        // EDIT roditelja    
        }else if(isset($_POST['roditelj_id']) && isset($_POST['roditelj_jmbg'])) {
            $id      = $_POST['roditelj_id'];
            $ime     = $this->validiraj_text($_POST['roditelj_ime']);
            $prezime = $this->validiraj_text($_POST['roditelj_prezime']);
            $jmbg    = $this->validiraj_broj($_POST['roditelj_jmbg']);

            $this->Admin_model->updateRoditelj($id, $ime, $prezime, $jmbg);
            // success
            echo 'Uspešno izmenjeno';
            exit();

        // EDIT ucitelja    
        }else if(isset($_POST['ucitelj_id']) && isset($_POST['ucitelj_ime']) && isset($_POST['ucitelj_prezime'])) {
            $id      = $_POST['ucitelj_id'];
            $ime     = $this->validiraj_text($_POST['ucitelj_ime']);
            $prezime = $this->validiraj_text($_POST['ucitelj_prezime']);

            $this->Admin_model->updateUcitelj($id, $ime, $prezime);
            // success
            echo 'Uspešno izmenjeno';
            exit();

        // EDIT profesora
        }else if(isset($_POST['profesor_id']) && isset($_POST['predmet'])) {
            $id      = $_POST['profesor_id'];
            $ime     = $this->validiraj_text($_POST['profesor_ime']);
            $prezime = $this->validiraj_text($_POST['profesor_prezime']);
            $predmet = $_POST['predmet'];

            // PROVERE OVDE
            $this->Admin_model->updateProfesor($id, $ime, $prezime, $predmet);
            // success
            echo 'Uspešno izmenjeno';
            exit();

        // EDIT administratora
        }elseif(isset($_POST['admin_id']) && isset($_POST['admin_ime']) && isset($_POST['admin_prezime'])) {
            $id      = $_POST['admin_id'];
            $ime     = $this->validiraj_text($_POST['admin_ime']);
            $prezime = $this->validiraj_text($_POST['admin_prezime']);

            $this->Admin_model->updateAdmin($id, $ime, $prezime);
            // success
            echo 'Uspešno izmenjeno';
            exit();

        // EDIT direktora    
        }else if(isset($_POST['direktor_id']) && isset($_POST['direktor_ime']) && isset($_POST['direktor_prezime'])) {
            $id      = $_POST['direktor_id'];
            $ime     = $this->validiraj_text($_POST['direktor_ime']);
            $prezime = $this->validiraj_text($_POST['direktor_prezime']);

            $this->Admin_model->updateDirektor($id, $ime, $prezime);
            // success
            echo 'Uspešno izmenjeno';
            exit();
        }
        /* --------------------------- DELETE USERS --------------------------*/

        // BRISANJE ucenika i roditelja iz baze
        if(isset($_POST['obrisiUcenika']) && isset($_POST['obrisiRoditelja'])){
            $ucenik_id   = $_POST['obrisiUcenika'];
            $roditelj_id = $_POST['obrisiRoditelja'];
            $this->Admin_model->deleteUcenikRoditelj($ucenik_id, $roditelj_id);
            // success
            echo 'Uspešno obrisano';
            exit();

        // BRISANJE ucitelja    
        }else if(isset($_POST['obrisiUcitelja']) && isset($_POST['user'])) {
            $ucitelj_id = $_POST['obrisiUcitelja'];
            $user_id    = $_POST['user'];
            $this->Admin_model->deleteUcitelj($ucitelj_id, $user_id);
            // success
            echo 'Uspešno obrisano';
            exit();

        // Brisanje profesora    
        }else if(isset($_POST['obrisiProfesora'])) {
            $user_id = $_POST['obrisiProfesora'];
            $this->Admin_model->obrisiProfesor($user_id);
            // success
            echo 'Uspešno obrisano';
            exit();

        // Brisanje admina    
        }else if(isset($_POST['obrisiAdmina'])) {
            $user_id = $_POST['obrisiAdmina'];
            $this->Admin_model->obrisiAdmin($user_id);
            // success
            echo 'Uspešno obrisano';
            exit();

        // Brisanje direktora
        }else if(isset($_POST['obrisiDirektora'])){
            $user_id = $_POST['obrisiDirektora'];
            $this->Admin_model->obrisiDirektor($user_id);
            // success
            echo 'Uspešno obrisano';
            exit();
        }
        
        // Salje ove podateke u view:
        $data['onlineUsers'] = $this->Admin_model->getOnlineUsers();
        $data['classes']     = $this->Admin_model->getallClasses();
        $data['allpredmeti'] = $this->Admin_model->get_predmeti();

        // Ucitava prikaz za View
        $this->load->view("templates/header.php");
        $this->load->view("admin/index", $data);
        $this->load->view("templates/footer.php");
    }

    public function raspored() {
        $data= [];
        $razred     = isset($_POST['razred']) ? $_POST['razred']:1;
        $odeljenje  = isset($_POST['odeljenje']) ? $_POST['odeljenje']:1;
        if(isset($_POST['OdaberiRazred']) or isset($_POST['unesi_promene'])){
            $razred     = $_POST['razred'];
            $odeljenje  = $_POST['odeljenje'];
        }
        $data['edit_raspored'] = $this->Admin_model->AdminScheduleEdit($razred,$odeljenje); //ovaj mora prvi da bude pozvan kako bi pravilno bilo refreshovano

        $data['raspored']   = $this->Admin_model->AdminScheduleView($razred,$odeljenje);
        
        // Ucitava prikaz za View
        $this->load->view("templates/header.php");
        $this->load->view("admin/raspored", $data);
        $this->load->view("templates/footer.php");
    }

    /*------------------------ EXCEL IMPORT/EXPORT --------------------*/

    public function excel() {
        $data= [];
        // Ucitava prikaz za View
        $this->load->view("templates/header.php");
        $this->load->view("admin/excel", $data);
        $this->load->view("templates/footer.php");
    }

    /*------------------ REGISTROVANJE NOVIH KORISNIKA ---------------*/

    // Register new user
    public function register() {
        // Pozivanje chatkit-a
        $this->load->view("../assets/vendor/autoload");
        $chatkit = new Chatkit\Chatkit([
            'instance_locator' => 'v1:us1:05676e97-a4f9-484f-8da9-272b6b5d92ff',
            'key' => 'b6151ccd-c342-4dfe-9ee1-bb26026848a5:8qqaT7KJwoLM0akjpFg3mA+dI9wH0weTHA9GooYEvds='
        ]);
        // Dobijanje podataka za select opciju
        $data['odeljenja'] = $this->Admin_model->getOdeljenja();
        $data['predmeti']  = $this->Admin_model->get_predmeti();

        // REGISTER roditelj i ucenik
        if(isset($_POST['role']) && $_POST['role'] == 'roditelj') {
            $rola      = $_POST['role'];
            $r_ime     = $this->validiraj_text($_POST['f_name']);
            $r_prezime = $this->validiraj_text($_POST['l_name']);
            $jmbg      = $this->validiraj_broj($_POST['jmbg']);
            $u_ime     = $this->validiraj_text($_POST['child_f_name']);
            $u_prezime = $this->validiraj_text($_POST['child_l_name']);
            $razred    = $_POST['razred'];
            $odeljenje = $_POST['odeljenje'];
            
            // Ako nema staresine dodeljenog za taj razred
            $staresina  = $this->Admin_model->proveriStaresinu($razred, $odeljenje);
            if($staresina[0]->ucitelj_id == null && $staresina[0]->profesor_id == null) {
                echo "<i>Molimo Vas, izaberite drugo odeljenje<br>Ovo deljenje trenutno nema dodeljenog starešinu</i>";
                exit();
            }
            // Unos korisnika u User tabelu
            $username = $r_ime .".". $r_prezime; // concat dot and concat last name of a registered person
            $hash_password = password_hash($jmbg, PASSWORD_DEFAULT); // hashing jmbg to use as password
            $user_id = $this->Admin_model->register_user($hash_password, $username, $rola);

            // Unos ucenika
            $this->Admin_model->register_ucenik($u_ime, $u_prezime, $razred, $odeljenje);
            // Zatim unos default ocena za sve predmete izabranog razreda
            $odg = $this->Admin_model->unesi_ocene($razred);

            
            // Dobijanje staresine za izabrani razred - potrebno za chat
            $staresina_id = $this->Admin_model->getStaresinaID($razred, $odeljenje);
            $staresina_id = $staresina_id[0]->user_id;

            // Unos Roditelja u tabelu
            $this->Admin_model->register_roditelj($jmbg, $r_ime, $r_prezime);

            
            // Kreiranje korisnika za chatkit
            $chatkit->createUser([
                'id'   => "$user_id", 
                'name' =>  $username
              ]);
            // Kreiranje sobe za chat za tog usera
            $chatkit->createRoom([
                'id'         => "$user_id",
                'creator_id' => "$user_id",
                'name'       =>  $username,
                'private'    =>  true
              ]);
            // Dodavanje ucitelja/profesora u sobu
            $chatkit->addUsersToRoom([
                'user_ids' => [$staresina_id], // ucitelj/profesor
                'room_id'  => "$user_id"
              ]);
              
            // Success
            echo "<i style='color:green'>Dodato u bazu.<br> Roditelj $username se sada može ulogovati!</i>";
            exit();
        
        // REGISTER ucitelj
        }else if(isset($_POST['role']) && $_POST['role'] == 'ucitelj') {
            $rola      = $_POST['role'];
            $ime       = $this->validiraj_text($_POST['ucitelj_ime']);
            $prezime   = $this->validiraj_text($_POST['ucitelj_prezime']);
            $jmbg      = $this->validiraj_broj($_POST['ucitelj_jmbg']);

            // Unos korisnika u User tabelu
            $hash_password = password_hash($jmbg, PASSWORD_DEFAULT);
            $username = $ime . "." . $prezime; // Spajam sa tackom izmedju imena i prezimena
            $user_id = $this->Admin_model->register_user($hash_password, $username, $rola);
            
            // Unos ucitelja u tabelu Ucitelji
            $this->Admin_model->register_ucitelj($ime, $prezime/*, $razred, $odeljenje*/);

            // Kreiranje korisnika za chatkit
            $chatkit->createUser([
                'id'   => "$user_id", 
                'name' =>  $username
              ]);

            // Success
            echo "<i style='color:green'>Dodato u bazu<br> Korisnik mora sacekati da mu se dodeli odeljenje!</i>";
            exit();

        // REGISTER profesor
        }else if(isset($_POST['role']) && $_POST['role'] == 'profesor') {
            $rola      = $_POST['role'];
            $ime       = $this->validiraj_text($_POST['profesor_ime']);
            $prezime   = $this->validiraj_text($_POST['profesor_prezime']);
            $jmbg      = $this->validiraj_broj($_POST['profesor_jmbg']);
            $predmet   = $_POST['profesor_predmet'];

            if($predmet == '') {
                echo "Morate izabrati predmet";
                exit();
            }
            // Unos korisnika u User tabelu
            $hash_password = password_hash($jmbg, PASSWORD_DEFAULT);
            $username = $ime . "." . $prezime; // Spajam sa tackom izmedju imena i prezimena
            $user_id = $this->Admin_model->register_user($hash_password, $username, $rola);

            // Unos korisnika u tabelu Profesori
            $this->Admin_model->register_profesor($ime, $prezime, $predmet);

            // Kreiranje korisnika za chatkit
            $chatkit->createUser([
                'id'   => "$user_id", 
                'name' =>  $username
              ]);

            // Success
            echo "<i style='color:green'>Dodato u bazu<br> Korisnik mora sacekati da mu se dodeli odeljenje!</i>";
            exit();
            
        // REGISTER administrator  
        }else if(isset($_POST['role']) && $_POST['role'] == 'admin') {
            $rola    = $_POST['role'];
            $ime     = $this->validiraj_text($_POST['admin_ime']);
            $prezime = $this->validiraj_text($_POST['admin_prezime']);
            $jmbg    = $this->validiraj_broj($_POST['admin_jmbg']);
            
            // Unos korisnika u tabelu User
            $hash_password = password_hash($jmbg, PASSWORD_DEFAULT);
            $username = $ime . "." . $prezime; // Spajam sa tackom izmedju imena i prezimena
            $this->Admin_model->register_user($hash_password, $username, $rola);

            // Unos korisnika u tabelu Direktor
            $this->Admin_model->register_admin($ime, $prezime);
            // Success
            echo "<i style='color:green'>Dodato u bazu. Administrator se sada može ulogovati!</i>";
            exit();

        // REGISTER direktor    
        }else if(isset($_POST['role']) && $_POST['role'] == 'direktor') {
            $rola    = $_POST['role'];
            $ime     = $this->validiraj_text($_POST['direktor_ime']);
            $prezime = $this->validiraj_text($_POST['direktor_prezime']);
            $jmbg    = $this->validiraj_broj($_POST['direktor_jmbg']);
            
            // Unos korisnika u tabelu User
            $hash_password = password_hash($jmbg, PASSWORD_DEFAULT);
            $username = $ime . "." . $prezime; // Spajam sa tackom izmedju imena i prezimena
            $this->Admin_model->register_user($hash_password, $username, $rola);
            
            // Unos korisnika u tabelu Direktor
            $this->Admin_model->register_direktor($ime, $prezime);
            // Success
            echo "<i style='color:green'>Dodato u bazu. Direktor se sada može ulogovati!</i>";
            exit();
        }
        // Ucitavanje stranice
        $this->load->view('templates/header');
        $this->load->view('admin/register', $data);
        $this->load->view('templates/footer');
    }

    /*------------------------ RAZREDI I ODELJENJA ---------------------*/

    // RAZREDI
    public function razredi() {
        // Dobijanje svih odeljenja iz baze za select opciju
        $data['odeljenja'] = $this->Admin_model->getOdeljenja();

        // Prikaz samo za izabranog razreda i odeljenje
        if(isset($_POST['izabranRazred']) && isset($_POST['izabranoOdeljenje'])) {
            $razred    = $_POST['izabranRazred'];
            $odeljenje = $_POST['izabranoOdeljenje'];
        }else {
            $razred    = 'sve';
            $odeljenje = 'sve';
        }
        // Uklanjanje staresine iz odeljenja
        if(isset($_POST['ukloniStaresinu'])) {
            $id        = $_POST['ukloniStaresinu'];
            $razred    = $_POST['razred'];
            $odeljenje = $_POST['odeljenje'];
            $this->Admin_model->ukloniStaresinu($id, $razred, $odeljenje);
            exit();
        }
        // Uklanjanje ucenika iz odeljenja
        if(isset($_POST['ukloniUcenika'])) {
            $id = $_POST['ukloniUcenika'];
            $this->Admin_model->ukloniUcenika($id);
            exit();
        }
        // DODAVANJE ucitelja u odeljenje
        if(isset($_POST['dodajUcitelja'])) {
            $id        = $_POST['dodajUcitelja'];
            $razred    = $_POST['razred'];
            $odeljenje = $_POST['odeljenje'];
            $this->Admin_model->dodeliUcitelja($id, $razred, $odeljenje);
            exit();
        }
        // DODAVANJE profesora u odeljenje
        if(isset($_POST['dodajProfesora'])) {
            $id        = $_POST['dodajProfesora'];
            $razred    = $_POST['razred'];
            $odeljenje = $_POST['odeljenje'];
            $this->Admin_model->dodeliProfesora($id, $razred, $odeljenje);
            exit();
        }
        // DODAVANJE ucenika u odeljenje
        if(isset($_POST['dodajUcenika'])) {
            $id        = $_POST['dodajUcenika'];
            $razred    = $_POST['razred'];
            $odeljenje = $_POST['odeljenje'];
            $this->Admin_model->dodeliUcenika($id, $razred, $odeljenje);
            $this->Admin_model->dodeliOcene($id, $razred);
            exit();
        }


        $data['razredi']   = $this->Admin_model->getRazredi($razred, $odeljenje);
        $data['staresine'] = $this->Admin_model->getStaresina($razred, $odeljenje);
        $data['nerasporedjeniUcitelji']  = $this->Admin_model->nerasporedjeniUcitelji();
        $data['nerasporedjeniProfesori'] = $this->Admin_model->nerasporedjeniProfesori();
        $data['nerasporedjeniUcenici']   = $this->Admin_model->nerasporedjeniUcenici();

        $this->load->view('templates/header');
        $this->load->view('admin/razredi', $data);
        $this->load->view('templates/footer');
    }

    /*---------------------- PREDMETI PO RAZREDIMA -------------------*/

    // PREDMETI
    public function predmeti() {
        // dodavanje predmeta 
        if(isset($_POST['nazivPredmeta']) && isset($_POST['uRazredima'])) {
            $predmet = $this->validiraj_text($_POST['nazivPredmeta']);
            $razredi = $_POST['uRazredima'];

            // Unos predmeta u bazu
            $this->Admin_model->unesiPredmet($predmet, $razredi); 
            // Success
            $data['porukaPredmet'] = "<i style='color:green'>Predmet je dodat u bazu</i>";           
        }

        // brisanje predmeta
        if(isset($_POST['predmet_id']) && isset($_POST['razred'])) {
            $predmet_id = $_POST['predmet_id'];
            $razred     = $_POST['razred'];

            $this->Admin_model->obrisiPredmet($predmet_id, $razred);
            // Success
            echo "<i style='color:green'>Predmet je obrisan</i>";
            exit();
        } 

        // Prikaz svih predmeta po razredima
        $data['razredi'] = $this->Admin_model->predmetiPoRazredima();
        $this->load->view("templates/header.php");
        $this->load->view("admin/predmeti", $data);
        $this->load->view("templates/footer.php");
    }

    /*------------------------- USER LOGIN ---------------------*/

    // LOGOVANJE
    public function login() {
        //setting validation rules for parent/child form
        $this->form_validation->set_message('required', '<span style="color: wheat;">Polje <i>%s</i> mora biti popunjeno!</span>');
        $this->form_validation->set_rules('username', 'Korisnicko ime', 'required|regex_match[/^[a-zA-Z]+\.{1}[a-zA-Z]+$/]', array('regex_match' => '<span style="color: wheat;">Korisničko ime nije odgovarajuće.</span>'));
        $this->form_validation->set_rules('password', 'Sifra', 'required|numeric', array('numeric' => '<span style="color: wheat;">Ukucali ste šifru korisnika Ruže Đesić. Pokušajte ponovo sa vašom šifrom!</span>'));

        if($this->form_validation->run() === false) {
            $this->load->view('templates/header');
            $this->load->view('admin/login');
        } else {
            // get username from form
            $username = $this->input->post('username');
            $username = strip_tags($username);
            // get the password from form
            $password = $this->input->post('password');

            if(!is_numeric($password)) {
                redirect('admin/login');
            }
            //check if username exits
            $user = $this->Admin_model->match_username($username);

            // check if $user is array. if it is, model is returning an array with result() method, which means that database has two or more exact names
            // in case professor's or teacher's child goes to same school where one works, password will be the same for both db entries, but if exact name represents different persons then we have to check that each password matches the input and then proceed with password_verify()
            if(is_array($user)) {
                foreach($user as $hash) { //$hash is an object inside $user array
                    if(password_verify($password, $hash->password)) { // REMINDER! If parent == teacher or proffessor. one should choose how to log, as a parrent, teacher or proffessor
                        //if($hash->role == "profesor") { // Za Mileta Kitica... Jer je isti covek, sa dve uloge
                            $hashed_password = $hash->password;
                            break;
                        //}
                    }
                }
            } else {
                // if model returns an object, it is because there is only one matched name in db. we than take its password field and set it to a variable
                $hashed_password = $user->password;
            }
            
            if(password_verify($password, $hashed_password)) {
                $user = $this->Admin_model->login($username, $hashed_password);
                if($user) {
                    echo $user->role;
                    //create session
                    $user_data = array(
                        'user_id' => $user->id,
                        'username' => $username,
                        'role' => $user->role,
                        'logged_in' => 1
                    );
                    $this->session->set_userdata($user_data);

                    // Update status-online to 1
                    $this->db->set('loged_in', '1');
                    $this->db->where('id', $this->session->userdata['user_id']);
                    $this->db->update('user');
                    // Set success message
                    //$this->session->set_flashdata('user_loggedin', 'Dobrodošli, '.$this->session->userdata['username']);
                    // Redirection 
                    redirect("$user->role");
                }
            } else {
                $this->session->set_flashdata('login_failed', 'Doslo je do greske. Pokusajte ponovo!');
                redirect('admin/login');
            }
        }
    }

    /*---------------------------- LOGOUT -----------------------*/

    // Logout user
    public function logout() {

        // Update status-online to 0
        $this->db->set('loged_in', '0');
        $this->db->where('id', $this->session->userdata['user_id']);
        $this->db->update('user');
        // unset user data
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('logged_in');
        // set success message
        $this->session->set_flashdata('user_loggedout', 'Uspesno ste se izlogovali');
        // Redirection
        redirect('admin/login');
    }
    // if exists... whatever
    public function check_username_exists($username) {
        $this->form_validation->set_message('check_username_exits', 'That username is taken. Try another.');
        if($this->Admin_model->check_username_exists($username)) {
            return true;
        } else {
            return false;
        }
    }
}
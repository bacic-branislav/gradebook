<?php
class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Home_model');
    }    

    public function view($page = "home") {
        if(!file_exists(APPPATH . "views/pages/$page.php")) { // APPPATH is CI constant representing root of our app
            show_404(); // CI function to show an error
        }
        // variables that we want to pass into the view
        $data["title"] = ucfirst($page);
        $data['school_title'] = "Acina Školica";
        // $data['copyright'] = "pusherteam.com";

        // loading our views
        $this->load->view("templates/header.php");
        $this->load->view("pages/$page", $data);
        //$this->load->view("templates/footer.php");
    } 

    // login user
    public function login() {
        $data['title'] = "Uloguj se";

        //setting validation rules for parent/child form
        $this->form_validation->set_message('required', '<span style="color: red;">Polje <i>%s</i> mora biti popunjeno!');
        $this->form_validation->set_rules('username', 'Korisnicko ime', 'required');
        $this->form_validation->set_rules('password', 'Sifra', 'required');

        if($this->form_validation->run() === false) {
            $this->load->view('templates/header');
            $this->load->view('admin/login', $data);
            $this->load->view('templates/footer');
        } else {
            // get username from form
            $username = $this->input->post('username');

            // get the password from form
            $password = $this->input->post('password');

            //check if username exits
            $user = $this->Admin_model->match_username($username);

            // check if $user is array. if it is, model is returning an array with result() method, which means that database has two or more exact names
            // in case professor's or teacher's child goes to same school where one works, password will be the same for both db entries, but if exact name represents different persons then we have to check that each password matches the input and then proceed with password_verify()
            if(is_array($user)) {
                foreach($user as $hash) { //$hash is an object inside $user array
                    if(password_verify($password, $hash->password)) { // REMINDER! If parent == teacher or proffessor. one should choose how to log, as a parrent, teacher or proffessor
                        //if($hash->role == "profesor") { // Za Mileta Kitica... Jer je isti covek, sa dve uloge
                            $hashed_password = $hash->password;
                            break;
                        //}
                    }
                }
            } else {
                // if model returns an object, it is because there is only one matched name in db. we than take its password field and set it to a variable
                $hashed_password = $user->password;
            }
            
            if(password_verify($password, $hashed_password)) {
                $user = $this->Admin_model->login($username, $hashed_password);
                if($user) {
                    echo $user->role;
                    //create session
                    $user_data = array(
                        'user_id' => $user->id,
                        'username' => $username,
                        'role' => $user->role,
                        'logged_in' => true

                    );
                    $this->session->set_userdata($user_data);

                    $this->session->set_flashdata('user_loggedin', 'Uspesno ste ulogovani');
                    redirect("$user->role/index");
                }
            } else {
                $this->session->set_flashdata('login_failed', 'Doslo je do greske. Pokusajte ponovo!');
                redirect('admin/login');
            }
        }
    }
}